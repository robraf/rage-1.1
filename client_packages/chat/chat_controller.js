let browser = mp.browsers.new("package://CEF/chat/index.html");
browser.markAsChat();

var player = mp.players.local;
var lastKeyPressedTime = Date.now();
var lastGlobalMessage = { message: "", time: Date.now() - 30 * 1000 };
var lastLocalMessage = { message: "", time: Date.now() - 5 * 1000 };

global.isPlayerTyping = false;
global.isAnyBrowserOpen = true;

const GLOBAL_CHAT_INTERVAL = 30;

mp.events.add("render", () => {
    const controls = mp.game.controls;

    if (isPlayerTyping) {
        controls.disableControlAction(0, 200, true);
    }

    if (Date.now() - lastKeyPressedTime < 200) {
        controls.disableControlAction(0, 200, true);
    }
});

//odbieram od klienta kiedy T lub Enter wciśnięty
mp.events.add("changeChatState", function (value) {
    if (value) {
        if (isAnyBrowserOpen === true) return;
        if (isPlayerTyping === true) return;
        isPlayerTyping = true;
        mp.events.callRemote("callServer::playerUseChat", true);
    } else {
        if (isAnyBrowserOpen === true) return;
        if (isPlayerTyping === false) return;
        isPlayerTyping = false;
        lastKeyPressedTime = Date.now();
        mp.events.callRemote("callServer::playerUseChat", false);
    }
});

function messageToChat(message) {
    if (browser != null) {
        var messageToSend = "`" + message + "`";
        var playerId = -1;
        var playerNick = "";
        var playerData = player.getVariable("playerData");
        if (playerData) {
            playerId = parseInt(playerData.id);
            playerNick = playerData.nick;
        }
        browser.execute(`drawMessage(${messageToSend}, ` + playerId + `, "` + playerNick + `")`);
    }
}
mp.events.add("call::sendMessageToChat", messageToChat)
mp.events.add("callClient::sendMessageToChat", messageToChat)

mp.events.add("call::canPlayerUseChat", (value) => {
    browser.execute(`setPlayerChat("${value}")`)
});

mp.events.add("playerCommand", (command) => {
    const args = command.split(/[ ]+/);
    const commandName = args.splice(0, 1)[0];
    var message = "";
    for (var i = 0; i < args.length; i++) {
        message = message + args[i] + " ";
    }
    if (commandName === "g" && message != "" && message != undefined && message != null) { //czat globalny
        var playerData = player.getVariable("playerData");
        if (playerData !== null && playerData !== undefined) {
            if (playerData.admin != "ADMIN") {
                var currentTime = Date.now();
                var diffrent = parseInt((currentTime - lastGlobalMessage.time) / 1000);
                if (diffrent >= GLOBAL_CHAT_INTERVAL) {
                    if (message === lastGlobalMessage.message && diffrent <= 60) {
                        mp.events.call("call::addNotify", "Spam", "Nie powtarzaj wiadomości!", "red", null);
                    } else {
                        mp.events.callRemote("callServer::playerSendMessage", "global", message);
                        lastGlobalMessage.time = currentTime;
                        lastGlobalMessage.message = message;
                    }
                } else {
                    mp.events.call("call::addNotify", "Czat globalny", "Wiadomości można wysyłać co " + GLOBAL_CHAT_INTERVAL + "s. Pozostało " + (GLOBAL_CHAT_INTERVAL - diffrent) + "s.", "red", null);
                }
            } else {
                mp.events.callRemote("callServer::playerSendMessage", "global", message);
            }
        }

    } else if (commandName === "a" && message != "" && message != undefined && message != null) { //czat adminowski
        mp.events.callRemote("callServer::playerSendMessage", "admin", message);
    }
});


function chatMessage(message) {
    var playerData = player.getVariable("playerData");
    if (playerData != null && playerData != undefined) {
        if (playerData.admin != "ADMIN") {
            var currentTime = Date.now();
            var diffrent = parseInt((currentTime - lastLocalMessage.time) / 1000);
            if (message === lastLocalMessage.message && diffrent <= 5) {
                mp.events.call("call::addNotify", "Spam", "Nie powtarzaj wiadomości!", "red", null);
            } else {
                mp.events.callRemote("callServer::playerSendMessage", "local", message);
                lastLocalMessage.time = currentTime;
                lastLocalMessage.message = message;
            }
        } else {
            mp.events.callRemote("callServer::playerSendMessage", "local", message);
        }
    }
};
mp.events.add("playerChat", chatMessage);