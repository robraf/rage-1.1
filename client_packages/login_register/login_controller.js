let browserLogin = null;
const player = mp.players.local;

//pobranie dowolnego eventu z przegladarki
mp.events.add("callFromHTML::onLoginPanelBrowserAction", function (event, args) {
    args = JSON.parse(args);
    switch (event) {
        case "login_click":
            var rememberPassword = args[2];
            if (rememberPassword != null) {
                if (rememberPassword) {
                    mp.storage.data.loginData = { login: args[0], password: args[1], remember: true };
                    mp.storage.flush();
                } else {
                    mp.storage.data.loginData = { login: "", password: "", remember: false };
                    mp.storage.flush();
                }
            }
            mp.events.callRemote("callServer::onLoginPanelBrowserAction", "loginPlayer", args[0], args[1]);
            break;
        case "register_click":
            mp.events.callRemote("callServer::onLoginPanelBrowserAction", "registerPlayer", args[0], args[1]);
            break;
        default:
            break;
    }
});

//odpowiedź z serwera
mp.events.add("callClient::onLoginPanelBrowserAction", function (status, userId, isCharacterCreated) {
    switch (status) {
        case "login_taken": //przy rejestracji zajęty login
            var list = [];
            list.push({ "status": "login_taken" });
            list = "`" + JSON.stringify(list) + "`";
            browserLogin.execute(`answerFromServer(${list})`);
            break;
        case "registration_success": //pomyślnie zarejestrowano, zwraca userId
            var list = [];
            list.push({ "status": "registration_success", "id": userId });
            list = "`" + JSON.stringify(list) + "`";
            browserLogin.execute(`answerFromServer(${list})`);
            break;
        case "incorrect_login": //przy logowaniu taki login nie istnieje
            var list = [];
            list.push({ "status": "incorrect_login" });
            list = "`" + JSON.stringify(list) + "`";
            browserLogin.execute(`answerFromServer(${list})`);
            break;
        case "bad_password": //podano błedne haslo przy logowaniu
            var list = [];
            list.push({ "status": "bad_password" });
            list = "`" + JSON.stringify(list) + "`";
            browserLogin.execute(`answerFromServer(${list})`);
            break;
        case "success": //podano dobry login i hasło, zalogowano
            if(isCharacterCreated==1){
                mp.events.callRemote("callServer::onSpawnAction", "get_list");
            }
            else{
                mp.events.callRemote("callServer::onPlayerInCreator", "player_in_creator");
                setTimeout(function () {
                    mp.events.call("call::onSpawnSelectAction", "stop_camera_anim", null);
                    mp.events.callRemote("callServer::onPlayerInCreator", "get_character");
                    //mp.events.call("call::playerCreatorBrowser", "set_player_in_creator");
                   // mp.events.call("call::addNotify", "Postać", "Stwórz swoją postać!", "green", null);
                }, 1 * 1000)
            }
            var list = [];
            list.push({ "status": "success" });
            list = "`" + JSON.stringify(list) + "`";
            browserLogin.execute(`answerFromServer(${list})`);
           // mp.events.call("call::addNotify", "Logowanie", "Pomyślnie zalogowano do konta", "green", null);
            setTimeout(function () {
                destroyBrowser();
            }, 1.5 * 1000)
            break;
        case "logged_in": //ktoś jest zalogowany na to konto
            var list = [];
            list.push({ "status": "logged_in" });
            list = "`" + JSON.stringify(list) + "`";
            browserLogin.execute(`answerFromServer(${list})`);
            break;
        case "banned": //konto jest zbanowane
            var list = [];
            list.push({ "status": "banned" });
            list = "`" + JSON.stringify(list) + "`";
            browserLogin.execute(`answerFromServer(${list})`);
            break;
        default:
            break;
    }
});

function createBrowser(isServerReady) {
    player.setVisible(false, false);
    player.freezePosition(true);
    if (browserLogin == null) {
        browserLogin = mp.browsers.new("package://CEF/login-panel/index.html");
        var serverReadyValue = 0;
        if(isServerReady){
            serverReadyValue = 1;
        }
        browserLogin.execute(`setServerActive(`+serverReadyValue+`)`);
        //sprawdzenie storage
        var loginData = mp.storage.data.loginData;
        if (loginData) {
            if (loginData.login != null & loginData.password != null) {
                if (loginData.login != "" && loginData.password != "") {
                    var list = [];
                    list.push({ "login": loginData.login, "password": loginData.password });
                    list = "`" + JSON.stringify(list) + "`";
                    browserLogin.execute(`setInitialFields(${list})`);
                }
            }
        }
        
        mp.gui.cursor.show(true, true);
    }
}

function setServerReady(isServerReady){
    if(browserLogin){
        var serverReadyValue = 0;
        if(isServerReady){
            serverReadyValue = 1;
        }
        browserLogin.execute(`setServerActive(`+serverReadyValue+`)`);
    }
}
mp.events.add("callClient::serverIsReady", setServerReady);


mp.events.add("callClient::createLoginPanelBrowser", function (isServerReady) {
    createBrowser(isServerReady);
});

function destroyBrowser() {
    if (browserLogin) {
        browserLogin.destroy();
        browserLogin = null;
    }
}