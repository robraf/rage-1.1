var player = mp.players.local;
var lastCameraIndex = -1;
var showCamera = true; //zmieni się na false w momencie gdy użytkownik się zaloguje
var sceneryCamera, camera1;
let cameraPoints = [
    { startX: -1922, startY: -1223, startZ: -25, endX: -1922, endY: -1223, endZ: 156, pointX: -1203, pointY: -944, pointZ: 13, duration: 60 }, //molo dellperro
    { startX: 1100, startY: 1518, startZ: 318, endX: 692, endY: 1708, endZ: 451, pointX: 396, pointY: -4, pointZ: 200, duration: 60 }, //napis vinewood
    { startX: 340, startY: 1196, startZ: 342, endX: 1000, endY: 994, endZ: 342, pointX: 743, pointY: 1250, pointZ: 360, duration: 60 },
    { startX: -735, startY: 5591, startZ: 44, endX: -276, endY: 5581, endZ: 238, pointX: 11, pointY: 5583, pointZ: 466, duration: 60 }]; //chilliad


mp.events.add("playerReady", () => {
    isAnyBrowserOpen = true;
    changeCamera();
});

function changeCamera() {
    mp.game.graphics.transitionToBlurred(100);
    var drawRandomCamera = Math.floor(Math.random() * cameraPoints.length);

    while (drawRandomCamera == lastCameraIndex) {  //sprawdza ostatnią kamere zeby sie nie powtarzały
        drawRandomCamera = Math.floor(Math.random() * cameraPoints.length);
    }
    lastCameraIndex = drawRandomCamera;
    player.position = new mp.Vector3(cameraPoints[drawRandomCamera].startX, cameraPoints[drawRandomCamera].startY, cameraPoints[drawRandomCamera].startZ);
    sceneryCamera = mp.cameras.new("loginCamera", new mp.Vector3(cameraPoints[drawRandomCamera].startX, cameraPoints[drawRandomCamera].startY, cameraPoints[drawRandomCamera].startZ), new mp.Vector3(0, 0, 0), 60);
    sceneryCamera.pointAtCoord(cameraPoints[drawRandomCamera].pointX, cameraPoints[drawRandomCamera].pointY, cameraPoints[drawRandomCamera].pointZ);
    sceneryCamera.setActive(true);
    sceneryCamera.shake("JOLT_SHAKE", 5);
    mp.game.cam.renderScriptCams(true, false, 0, true, false);
    setTimeout(function () {
        if (showCamera) {
            camera1 = mp.cameras.new('loginCameraMove1', new mp.Vector3(cameraPoints[drawRandomCamera].endX, cameraPoints[drawRandomCamera].endY, cameraPoints[drawRandomCamera].endZ), new mp.Vector3(0, 0, 0), 60);
            camera1.pointAtCoord(cameraPoints[drawRandomCamera].pointX, cameraPoints[drawRandomCamera].pointY, cameraPoints[drawRandomCamera].pointZ);
            camera1.setActiveWithInterp(sceneryCamera.handle, cameraPoints[drawRandomCamera].duration * 1000, 0, 0);
        }
    }, 0.5 * 1000)

    setTimeout(function () {
        if (showCamera) {
            camera1.shake("JOLT_SHAKE", 5);
            camera1.destroy();
            camera1 = null;
            sceneryCamera.destroy();
            sceneryCamera = null;
            changeCamera();
        }
    }, (cameraPoints[drawRandomCamera].duration + 0.5) * 1000)
}

mp.events.add("call::onSpawnSelectAction", (event, camera) => {
    switch (event) {
        case "mouse_hover":
            showCamera = false;
            if (camera1 != null) {
                camera1.destroy();
                camera1 = null;
            }

            if (sceneryCamera != null) {
                sceneryCamera.destroy();
                sceneryCamera = null;
            }
            player.position = new mp.Vector3(camera.camX, camera.camY, camera.camZ);
            player.dimension = 0;
            //player.setVisible(false, false);
            //player.freezePosition(true);
            sceneryCamera = mp.cameras.new(camera.text, new mp.Vector3(camera.camX, camera.camY, camera.camZ), new mp.Vector3(0, 0, 0), 40);
            sceneryCamera.pointAtCoord(camera.x, camera.y, camera.z + 10);
            sceneryCamera.setActive(true);
            sceneryCamera.shake("JOLT_SHAKE", 3);
            mp.game.cam.renderScriptCams(true, false, 0, true, false);
            break;
        case "stop_camera_anim":
            showCamera = false;
            if (camera1 != null) {
                camera1.destroy();
                camera1 = null;
            }

            if (sceneryCamera != null) {
                sceneryCamera.destroy();
                sceneryCamera = null;
            }
            break;
        case "start_camera_anim":
            lastCameraIndex = -1;
            showCamera = true; //zmieni się na false w momencie gdy użytkownik się zaloguje
            sceneryCamera = null
            camera1 = null
            showCamera = true;
            changeCamera();
            break;
        case "spawn_click":
            showCamera = false;

            var cameraToPlayer = mp.cameras.new('cameraToPlayer', new mp.Vector3(camera.x, camera.y, camera.z), new mp.Vector3(0, 0, 0), 60);
            cameraToPlayer.setActiveWithInterp(sceneryCamera.handle, 1.5 * 1000, 0, 0);
            mp.events.call("call::removeSpawnBrowser");
            setTimeout(function () {
                mp.game.graphics.transitionFromBlurred(0);
                //player.position = new mp.Vector3(camera.x, camera.y, camera.z);
                player.setHeading(camera.rotation);
                if (camera1 != null) {
                    camera1.destroy();
                    camera1 = null;
                }

                if (sceneryCamera != null) {
                    sceneryCamera.destroy();
                    sceneryCamera = null;
                }

                mp.game.cam.renderScriptCams(false, false, 0, true, false);
                player.setVisible(true, true);
                player.freezePosition(false);
                mp.game.ui.displayRadar(true);
                mp.game.ui.displayHud(true); 
                isAnyBrowserOpen = false; //JEŚLI NA FALSE to po wciśnięciu klawisza 'T' aktywuje się czat, właczać na TRUE w przypadku jakiś pól tekstowych
                mp.events.call("call::canPlayerUseChat", true);
                mp.events.callRemote("callServer::onSpawnAction", "player_spawned", new mp.Vector3(camera.x, camera.y, camera.z));
                mp.events.call("call::countActiveTotalTime");
            }, 2 * 1000)
            break;
        default:
            break;
    }
});