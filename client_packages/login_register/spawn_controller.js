let browserSpawn = null;
var localSpawnList = null;

function createBrowser(spawnList) {
    if (browserSpawn == null) {
        browserSpawn = mp.browsers.new("package://CEF/spawns/index.html");
        list = "`" + JSON.stringify(spawnList) + "`";
        browserSpawn.execute(`getSpawnList(${list})`);
        mp.gui.cursor.show(true, true);
    }
}

mp.events.add("callClient::returnSpawnList", function (spawnList) {
    localSpawnList = spawnList;
    setTimeout(function () {
        createBrowser(spawnList);
    }, 1 * 50)
});


function destroyBrowser() {
    if (browserSpawn) {
        browserSpawn.destroy();
        browserSpawn = null;
        mp.gui.cursor.show(false, false);
    }
}
mp.events.add("call::removeSpawnBrowser", destroyBrowser);

mp.events.add("callFromHTML::onSpawnSelectAction", function (event, args) {
    var idOnHover = JSON.parse(args)[0];
    var cameraObj = null;
    for (var i = 0; i < localSpawnList.length; i++) {
        var camera = localSpawnList[i];
        if (camera.id == idOnHover) {
            cameraObj = camera;
            break;
        }
    }
    
    switch (event) {
        case "mouse_hover":
            setTimeout(function () {
                mp.events.call("call::onSpawnSelectAction", "mouse_hover", cameraObj);
            }, 100)

            break;
        case "spawn_click":
            mp.events.call("call::onSpawnSelectAction", "spawn_click", cameraObj);
            break;
        default:
            break;
    }
});

//on Player Die
mp.events.add("playerDeath", (reason, killer) => {
    mp.game.graphics.startScreenEffect("DeathFailNeutralIn", 5000, false);
    mp.events.call("call::addNotify", "Śmierć", "Za chwilę zostaniesz przeniesiony do najbliższego punktu spawnu.", "red", null);
    setTimeout(function () {
        if(localSpawnList!==null){
            var playerPos = mp.players.local.position;
            var theClosestSpawn = new mp.Vector3(0,0,0);
            var theClosestDistance = Number.MAX_SAFE_INTEGER;
            for (var i = 0; i < localSpawnList.length; i++) {
                if(localSpawnList[i].id!=="last_pos"){
                    var distance = mp.game.gameplay.getDistanceBetweenCoords(localSpawnList[i].x, localSpawnList[i].y, localSpawnList[i].z, playerPos.x, playerPos.y, playerPos.z, true);
                    if(distance<=theClosestDistance){
                        theClosestSpawn = new mp.Vector3(localSpawnList[i].x, localSpawnList[i].y, localSpawnList[i].z);
                        theClosestDistance=distance;
                    }
                } 
            }
            mp.events.callRemote("callServer::onSpawnAction", "player_died", theClosestSpawn);
        }
    }, 5*1000)
});

//mp.game.pathfind.calculateTravelDistanceBetweenPoints(x1, y1, z1, x2, y2, z2);