let player = mp.players.local;

var POMIAR_SPEED = 150;

var checkAcceleration = false;
var timeStart = 0;
var timeEnd = 0;
var text = "";
var maxSpeed = 0;

mp.events.add("playerCommand", (command) => {
    const args = command.split(/[ ]+/);
    const commandName = args.splice(0, 1)[0];
    
    if (commandName === "handling") {
        if (player.vehicle) {
            player.vehicle.setHandling(args[0], parseFloat(args[1]));
            mp.events.callRemote("callServer::sendMessageToPlayer", "Ustawiam handling '" + args[0] + "' na wartość: " + args[1], "orange");
        } else {
            mp.events.callRemote("callServer::sendMessageToPlayer", "Nie siedzisz w pojeździe", "red");
        }
    } else if (commandName === "aktualny") {
        if (player.vehicle) {
            mp.events.callRemote("callServer::sendMessageToPlayer", "Aktualny handling: " + player.vehicle.getHandling(args[0]), "orange");
        }
    }else if (commandName === "myj") {
        if(player.vehicle){
            player.vehicle.setDirtLevel(args[0]);
        }
    } 
    else if (commandName === "pomiar") {
        if (player.vehicle && player.vehicle.getSpeed() * 3.6 == 0) {
            POMIAR_SPEED = parseInt(args[0]);
            mp.events.callRemote("callServer::sendMessageToPlayer", "Rusz, aby rozpocząć pomiar do "+args[0]+" km/h", "orange");
            text = "Rusz, aby rozpocząć pomiar"
            checkAcceleration = true;
            timeStart = 0;
            timeEnd = 0;
        }
    }
    else if (commandName === "pos") {
        if (!player.vehicle)
            mp.events.callRemote("callServer::sendMessageToPlayer", "pozcyja " + player.position.x + ", " + player.position.y + ", " + player.position.z + ", heading " + player.getHeading(), "orange");
        else
            mp.events.callRemote("callServer::sendMessageToPlayer", "pozcyja " + player.vehicle.position.x + ", " + player.vehicle.position.y + ", " + player.vehicle.position.z + ", heading " + player.vehicle.getHeading(), "orange");
    } else if (commandName === "maxspeed") {
        if (player.vehicle) {
            player.vehicle.setMaxSpeed(parseInt(args[0]) / 3.6);
            mp.events.callRemote("callServer::sendMessageToPlayer", "Ustawiono VMAX'a na " + args[0], "orange");
        }
    } else if (commandName === "drzwi") {
        if (player.vehicle){
            player.vehicle.setDoorBroken(parseInt(args[0]), false);
        }
    }else if (commandName === "sprawdzdrzwi"){
        if(player.vehicle){
            mp.events.callRemote("callServer::sendMessageToPlayer", "Sprawdzam drzwi: "+player.vehicle.isDoorDamaged(parseInt(args[0])), "orange");
        }
    }
});

mp.events.add('render', () => {
    if (player.vehicle) {
        //engine value set

       
    
        var currentSpeed = player.vehicle.getSpeed() * 3.6;
        if (checkAcceleration) {
            if (timeStart == 0 && currentSpeed > 0.7)
                timeStart = Date.now();

            if (timeStart > 0) {
                text = parseFloat((Date.now() - timeStart) / 1000.0);
            }
            if (currentSpeed >= POMIAR_SPEED && timeStart > 0) {
                checkAcceleration = false;
                timeEnd = Date.now();
                var accelerationResult = parseFloat((timeEnd - timeStart) / 1000.0);
                mp.events.callRemote("callServer::sendMessageToPlayer", "Pomiar zakończony, przyspieszenie do "+POMIAR_SPEED+"km/h: " + accelerationResult, "orange");
            }
        } else {
            text = "";
        }
        if (currentSpeed > maxSpeed)
            maxSpeed = currentSpeed;

        mp.game.graphics.drawText(text + " " + (player.vehicle.rpm).toFixed(2) + " RPM", [0.5, 0.005], {
            font: 7,
            color: [255, 255, 255, 185],
            scale: [0.5, 0.5],
            outline: true
        });
        let maxs = (mp.game.vehicle.getVehicleModelMaxSpeed(player.vehicle.model)*3.6);
        mp.game.graphics.drawText("Max speed: " + parseInt(maxSpeed) + " / " + parseInt(maxs) + " km/h", [0.9, 0.005], {
            font: 7,
            color: [255, 0, 0, 185],
            scale: [0.5, 0.5],
            outline: true
        });
        mp.game.graphics.drawText("Przyspieszenie " + (player.vehicle.getAcceleration()).toFixed(5), [0.9, 0.800], {
            font: 7,
            color: [0, 255, 0, 185],
            scale: [0.5, 0.5],
            outline: true
        });
        mp.game.graphics.drawText("Hamowanie " + (player.vehicle.getMaxBraking()).toFixed(5), [0.9, 0.850], {
            font: 7,
            color: [0, 120, 255, 185],
            scale: [0.5, 0.5],
            outline: true
        });
        mp.game.graphics.drawText("Przyczepność " + (player.vehicle.getMaxTraction()).toFixed(5), [0.9, 0.900], {
            font: 7,
            color: [255, 255, 0, 185],
            scale: [0.5, 0.5],
            outline: true
        });
    } else {
        maxSpeed = 0;
        checkAcceleration = false;
        timeStart = 0;
        timeEnd = 0;
    }


});

var isPomiar = false;
var timeCount = -1;
var updateTime = null;
//pomiar test
mp.events.add("callClient::setHandbrake", (value, timer) => {
    if (player.vehicle) {
        player.vehicle.setHandbrake(value);
        if (value) {
            isPomiar = true;
            timeCount = timer;
            updateTime = setInterval(function () {
                timeCount = timeCount - 1;
                if (timeCount <= 0)
                    timeCount = -1;
            }, 1000 * 1);
        }
    }
});

//zmiana czas
mp.events.add("callClient::changeTime", (timer) => {
    if (isPomiar && timeCount > 0) {
        timeCount = (timer + 1);
    }
});

mp.events.add("callClient::endPomiar", () => {
    isPomiar = false;
    timeCount = -1;
    clearInterval(updateTime);
    updateTime = null;
});

mp.events.add("render", () => {
    if (isPomiar && player.vehicle && timeCount >= 0) {
        mp.game.graphics.drawText("Start za " + timeCount + "s", [0.5, 0.4], {
            font: 4,
            color: [255, 255, 255, 185],
            scale: [0.7, 0.7],
            outline: true
        });
    }
});


