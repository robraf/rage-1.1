let player = mp.players.local;
var characterCamera = null;

mp.events.add("playerCommand", (command) => {
    const args = command.split(/[ ]+/);
    const commandName = args.splice(0, 1)[0];
    //mp.events.callRemote("callServer::sendMessageToPlayer", "Ustawiam handling "+args+" "+args.length, "blue");
    // args.shift();
    if (commandName === "postac") {
        mp.gui.cursor.show(true, true);
        player.dimension = 0;
        player.clearTasksImmediately();
        player.freezePosition(true);
        player.clearFacialIdleAnimOverride(); //ew. usunąć
        player.position = new mp.Vector3(436.19, -993.55, 30.68);
        player.setHeading(272);
        characterCamera = mp.cameras.new("characterCamera", new mp.Vector3(437.00, -993.55, 30.68 + 0.5), new mp.Vector3(0, 0, 0), 60);
        characterCamera.pointAtCoord(436.19, -993.55, 30.68 + 0.5);
        characterCamera.setActive(true);
        mp.game.cam.renderScriptCams(true, false, 0, true, false);
        //player.setRotation(0, 0, 30, 0, true);
        player.model = mp.game.joaat("mp_f_freemode_01");
        //chat off
    }
    else if (commandName === "oczy") {
        player.setEyeColor(parseInt(args[0]));
        mp.events.callRemote("callServer::sendMessageToPlayer", "Oczy " + args[0], "pink");
    } else if (commandName === "twarz") {
        player.setFaceFeature(parseInt(args[0]), parseFloat(args[1]));
        mp.events.callRemote("callServer::sendMessageToPlayer", "twarz " + args[0] + " skala " + args[1], "pink");
    } else if (commandName === "glowa") {

        player.setHeadOverlay(parseInt(args[0]), parseInt(args[1]), parseFloat(args[2]), parseInt(args[3]), parseInt(args[4]));
        mp.events.callRemote("callServer::sendMessageToPlayer", "glowa", "pink");
    }
    else if (commandName === "model") {
        player.model = mp.game.joaat(args[0]);
        mp.events.callRemote("callServer::sendMessageToPlayer", "Model " + args[0], "pink");
    } else if (commandName === "wlosy") {
        //var i = 0;
        player.setComponentVariation(2, parseInt(args[0]), 0, 0);
        mp.events.callRemote("callServer::sendMessageToPlayer", "Włosy ID: " + args[0], "pink");
        // setInterval(function(){
        //     player.setComponentVariation(2, i, 0, 0);
        //     mp.events.callRemote("callServer::sendMessageToPlayer", "Włosy ID: "+i, "pink");
        //     i++;
        // }, 1000)
    } else if (commandName === "kolorwlosy") {
        player.setHairColor(parseInt(args[0]), parseInt(args[1]));
        mp.events.callRemote("callServer::sendMessageToPlayer", "Kolor wlosow", "pink");
    } else if (commandName === "twarzrandom") {
        for (var i = 0; i <= 19; i++) {
            var value = randomFloat(-1.00, 1.00);
            //mp.events.callRemote("callServer::sendMessageToPlayer", "random "+value, "pink");
            player.setFaceFeature(parseInt(i), parseFloat(value));
        }
    } else if (commandName === "headblend") {
        player.setHeadBlendData(
            // shape
            parseInt(args[0]),
            parseInt(args[1]),
            0,

            // skin
            parseInt(args[0]),
            parseInt(args[1]),
            0,

            // mixes
            parseFloat(args[2]),
            parseFloat(args[3]),
            0.0,

            false
        );
    }
    else if (commandName === "test") {
        //var colorRandom = parseInt(randomInt(0, 63));
        //var id = parseInt(randomInt(0, 23));
        player.setHeadOverlay(parseInt(args[0]), parseInt(args[1]), 1, 0, 0);
        player.setHeadOverlayColor(parseInt(args[0]), parseInt(args[2]), parseInt(args[3]), parseInt(args[4]));
        mp.events.callRemote("callServer::sendMessageToPlayer", "TEST ", "black");
    }
    else if (commandName === "blemishes") {
        var colorRandom = parseInt(randomInt(0, 63));
        var id = parseInt(randomInt(0, 23));
        player.setHeadOverlay(0, id, 1, colorRandom, colorRandom);
        mp.events.callRemote("callServer::sendMessageToPlayer", "Blemished " + id, "pink");
    }
    else if (commandName === "facialhair") {
        var colorRandom = parseInt(randomInt(0, 63));
        var id = parseInt(randomInt(0, 28));
        player.setHeadOverlay(1, id, 1, colorRandom, colorRandom);
        mp.events.callRemote("callServer::sendMessageToPlayer", "facialhair " + id, "pink");
    }
    else if (commandName === "eyebrows") {
        var colorRandom = parseInt(randomInt(0, 63));
        var id = parseInt(randomInt(0, 33));
        player.setHeadOverlay(2, id, 1, colorRandom, colorRandom);
        mp.events.callRemote("callServer::sendMessageToPlayer", "Eyebrows " + id, "pink");
    }
    else if (commandName === "ageing") {
        var colorRandom = parseInt(randomInt(0, 63));
        var id = parseInt(randomInt(0, 14));
        player.setHeadOverlay(3, id, 1, colorRandom, colorRandom);
        mp.events.callRemote("callServer::sendMessageToPlayer", "Ageing " + id, "pink");
    }
    else if (commandName === "makeup") {
        var colorRandom = parseInt(randomInt(0, 63));
        var id = parseInt(randomInt(0, 74));
        player.setHeadOverlay(4, parseInt(args[0]), 1, 0, 0);
        player.setHeadOverlayColor(4, 1, parseInt(args[1]), parseInt(args[2]));
        mp.events.callRemote("callServer::sendMessageToPlayer", "Makeup " + id, "pink");
    }
    else if (commandName === "blush") {
        var colorRandom = parseInt(randomInt(0, 63));
        var id = parseInt(randomInt(0, 32));
        player.setHeadOverlay(5, id, 1, colorRandom, colorRandom);
        mp.events.callRemote("callServer::sendMessageToPlayer", "Blush " + id, "pink");
    }
    else if (commandName === "complexion") {
        var colorRandom = parseInt(randomInt(0, 63));
        var id = parseInt(randomInt(0, 11));
        player.setHeadOverlay(6, id, 1, colorRandom, colorRandom);
        mp.events.callRemote("callServer::sendMessageToPlayer", "Complexion " + id, "pink");
    }
    else if (commandName === "sundamage") {
        var colorRandom = parseInt(randomInt(0, 63));
        var id = parseInt(randomInt(0, 10));
        player.setHeadOverlay(7, id, 1, colorRandom, colorRandom);
        mp.events.callRemote("callServer::sendMessageToPlayer", "sundamage " + id, "pink");
    }
    else if (commandName === "lipstick") {
        var colorRandom = parseInt(randomInt(0, 63));
        var id = parseInt(randomInt(0, 9));
        player.setHeadOverlay(8, id, 1, colorRandom, colorRandom);
        mp.events.callRemote("callServer::sendMessageToPlayer", "Lipstick " + id, "pink");
    }
    else if (commandName === "moles") {
        var colorRandom = parseInt(randomInt(0, 63));
        var id = parseInt(randomInt(0, 17));
        // player.setHeadOverlay(9, id, 1, colorRandom, colorRandom);
        player.setHeadOverlay(9, parseInt(args[0]), 1, 0, 0);
        player.setHeadOverlayColor(9, 0, parseInt(args[1]), parseInt(args[2]));
        mp.events.callRemote("callServer::sendMessageToPlayer", "Moles " + id, "pink");
    }

});

function randomFloat(min, max) {
    var value = (Math.random() * (min - max) + max).toFixed(4);
    return value;
}

function randomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var eyeColors = ["Zielony", "Szmaragdowy", "Jasnoniebieski", "Błękit oceanu", "Jasnobrązowy",
    "Ciemnobrązowy", "Piwny", "Ciemnoszary", "Jasnoszary", "Różowy", "Żółty", "Fioletowe",
    "Czarne", "Szare", "Tequila Sunrise", "Atomic", "Warp", "ECola", "Space Ranger",
    "Ying Yang", "Bullseye", "Lizard", "Dragon", "Extra Terrestrial", "Goat", "Smiley",
    "Possessed", "Demon", "Infected", "Alien", "Undead", "Zombie"];

var mothersId = [21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 45];
var mothersNames = ["Justyna", "Iwona", "Agata", "Wiktoria", "Weronika",
    "Agnieszka", "Aleksandra", "Julia", "Martyna", "Alicja",
    "Amelia", "Oliwia", "Marta", "Magdalena", "Daria",
    "Karolina", "Anna", "Anastazja", "Gabriela", "Halina",
    "Danuta", "Natalia"];

var fathersId = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 42, 43, 44];
var fathersNames = ["Piotr", "Paweł", "Rafał", "Robert", "Dawid",
    "Maksymilian", "Radosław", "Jan", "Kamil", "Michał",
    "Mateusz", "Filip", "Marek", "Oskar", "Szymon",
    "Adam", "Jacek", "Marcin", "Stefan", "Łukasz",
    "Maciej", "Stanisław", "Dominik", "Jakub"];

