mp.gui.chat.activate(false);
mp.gui.chat.show(false);
mp.game.ui.displayHud(false);
mp.game.ui.displayRadar(false);
mp.nametags.enabled = false;
mp.gui.cursor.show(false, false);

require('./login_register/login_controller.js');
require('./login_register/spawn_controller.js');
require('./login_register/camera.js');
require('./player/notify.js');
require('./join/ban.js');
require('./player/admin.js');
require('./player/nametag.js');
require('./player/mute.js');
require('./player/hud.js');
require('./player/character.js');
require('./player/statistic.js');
require('./player/blip.js');

require('./vehicle/enter.js');
require('./vehicle/exploitation.js');
require('./vehicle/synchro.js');
require('./vehicle/fuel_station.js');

require('./noclip/index.js')

require('./chat/chat_controller.js');
require('./vehicletest_c.js');
require('./character_c.js');