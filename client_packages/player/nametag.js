let player = mp.players.local;
const MAX_DISTANCE = 80;
var chatIconsColor = { red: 0, green: 0, blue: 0, alpha: 0 };

mp.events.add("render", () => {
    let playerData = player.getVariable("playerData");
    if (playerData!==null && playerData!==undefined) {
        mp.players.forEachInStreamRange((playerObj, id) => {
            if (playerData.spawned && playerObj.dimension == player.dimension && !playerObj.getVariable("playerNoclip")) { //&& !playerObj.getVariable("isPlayerObserver") kiedy admin bedzie obserwatorem //player != playerObj && 
                var isPlayerAfk = 0;
                var distance = mp.game.gameplay.getDistanceBetweenCoords(
                    player.position.x,
                    player.position.y,
                    player.position.z,
                    playerObj.position.x,
                    playerObj.position.y,
                    playerObj.position.z,
                    true
                );

                if (distance <= MAX_DISTANCE) {
                    var posX = playerObj.position.x;
                    var posY = playerObj.position.y;
                    var posZ = playerObj.position.z;
                    var scale = scaleFunc(0.4, (distance / MAX_DISTANCE));

                    let posNick = mp.game.graphics.world3dToScreen2d(posX, posY, posZ + 1);
                    if (posNick !== null && posNick !== undefined) {
                        var adminColor = "";
                        if (playerObj.getVariable("playerData").admin == "ADMIN") {
                            adminColor = "~r~";
                        } else {
                            adminColor = "";
                        }
                        if (playerObj.getVariable("playerAFK") !== null) {
                            if (playerObj.getVariable("playerAFK")) {
                                isPlayerAfk = 255;
                            } else {
                                isPlayerAfk = 0;
                            }
                        } else {
                            isPlayerAfk = 0
                        }

                        mp.game.graphics.drawText(
                            playerObj.getVariable("playerData").nick + "[" + playerObj.getVariable("playerData").id + "]",
                            [
                                posNick.x,
                                posNick.y
                            ],
                            {
                                font: 4,
                                color: [225, 225, 225, 255],
                                scale: [scale, scale],
                                outline: true
                            }
                        );
    
                            if (playerObj.getVariable("playerData").admin !== "") {
                                mp.game.graphics.drawText(
                                    adminColor + " " + playerObj.getVariable("playerData").admin,
                                    [
                                        posNick.x,
                                        posNick.y+(0.038*(scale*1.5))
                                    ],
                                    {
                                        font: 4,
                                        color: [225, 225, 225, 255],
                                        scale: [scaleFunc(0.3, (distance / MAX_DISTANCE)), scaleFunc(0.3, (distance / MAX_DISTANCE))],
                                        outline: true
                                    }
                                );
                            }
                        
                        if (playerObj.getVariable("playerMuted")) {
                            //gracz zmutwaony
                            chatIconsColor.green = 0;
                            chatIconsColor.blue = 0;
                            chatIconsColor.red = 255;
                            chatIconsColor.alpha = 255;

                        } else {
                            if (playerObj.getVariable("playerChatting")) {//gracz pisze                     
                                chatIconsColor.alpha = 255;
                                chatIconsColor.red = 0;
                                chatIconsColor.green = 200;
                                chatIconsColor.blue = 200;
                            } else {
                                chatIconsColor.alpha = 0;
                            }
                        }
                        //chat typing icon
                        if (!mp.game.graphics.hasStreamedTextureDictLoaded("commonmenu")) {
                            mp.game.graphics.requestStreamedTextureDict("commonmenu", true);
                        }

                        if (mp.game.graphics.hasStreamedTextureDictLoaded("commonmenu")) {
                            mp.game.graphics.drawSprite("commonmenu", "shop_makeup_icon_a", posNick.x, posNick.y+(-0.018*(scale*1.5)), parseFloat(scale * 0.05), parseFloat(scale * 0.07), 0, chatIconsColor.red, chatIconsColor.green, chatIconsColor.blue, chatIconsColor.alpha);
                        }
                        //AFK icon
                        if (!mp.game.graphics.hasStreamedTextureDictLoaded("mpinventory")) {
                            mp.game.graphics.requestStreamedTextureDict("mpinventory", true);
                        }

                        if (mp.game.graphics.hasStreamedTextureDictLoaded("mpinventory")) {
                            mp.game.graphics.drawSprite("mpinventory", "mp_specitem_ped", posNick.x+(-0.03*(scale*1.5)), posNick.y+(-0.018*(scale*1.5)), parseFloat(scale * 0.03), parseFloat(scale * 0.06), 0, 255, 0, 0, isPlayerAfk);
                        }
                    }
                }
            }
        });
    }
});



function scaleFunc(co, procent) {
    return co - co * procent;
}