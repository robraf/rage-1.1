var player = mp.players.local;
var lastPosition = { x: 0.0, y: 0.0, z: 0.0 }
var lastStatsPosition = new mp.Vector3(0.0, 0.0, 0.0);

var totalTimeInterval = null;
var totalGameTime = 0; //in seconds
var noMoveTime = 0;
var MAX_NO_MOVE_TIME = 2.5*60; //2.5 mins
var awayFromKeyboardTime = 0;
var isPlayerAfk = false;
var playerVehicleSeat = -2;
var isWSADpressing = false;

//non-vehicle stats
var canCountWalking = false;
var statsWalking = 0.0;

//vehicle stats
var statsTraveledCarAsDriver = 0.0;  //ID -1
var statsTraveledCarAsPassenger = 0.0; //ID 0-2 pasazer

var objectStats = {
    total: totalGameTime,
    afk: awayFromKeyboardTime,
    drive: statsTraveledCarAsDriver,
    passenger: statsTraveledCarAsPassenger,
    walking: statsWalking
}

function countTotalTime() {
    lastStatsPosition = player.position;
    totalTimeInterval = setInterval(function () {
        canCountWalking = true;
        totalGameTime++;
        var distance = mp.game.gameplay.getDistanceBetweenCoords(
            player.position.x,
            player.position.y,
            player.position.z,
            lastPosition.x,
            lastPosition.y,
            lastPosition.z,
            true
        );
        lastPosition.x = player.position.x;
        lastPosition.y = player.position.y;
        lastPosition.z = player.position.z;

        if (!isWSADpressing) { //gracz sie nie ruszył if (distance < 0.3) {
            noMoveTime++;
            if (noMoveTime >= MAX_NO_MOVE_TIME) { //naliczaj AFK
                awayFromKeyboardTime++;
                isPlayerAfk = true;
            }
        } else {//gracz się przemieścił
            // if (distance < 0.2) {
            //     noMoveTime++;
            //     if (noMoveTime >= MAX_NO_MOVE_TIME) { //naliczaj AFK
            //         awayFromKeyboardTime++;
            //         isPlayerAfk = true;
            //     }
            // } else {
            noMoveTime = 0;
            isPlayerAfk = false;
            // }
        }
        if (totalGameTime % 60 == 0) { //minuta gry na serwerze
            // mp.events.callRemote("callServer::sendMessageToPlayer", "Minęła " + parseInt(totalGameTime / 60) + " minuta gry na serwerze, AFK " + awayFromKeyboardTime + "s", "orange");
        }

        objectStats.total = totalGameTime;
        objectStats.afk = awayFromKeyboardTime;
        objectStats.drive = statsTraveledCarAsDriver;
        objectStats.passenger = statsTraveledCarAsPassenger;
        objectStats.walking = statsWalking;

        mp.events.callRemote("callServer::updatePlayerTime", JSON.stringify(objectStats), isPlayerAfk);
    }, 1000);
}
mp.events.add("call::countActiveTotalTime", countTotalTime);

mp.events.add("changeChatState", function (value) {
    noMoveTime = 0;
    isPlayerAfk = false;
    objectStats.total = totalGameTime;
    objectStats.afk = awayFromKeyboardTime;
    objectStats.drive = statsTraveledCarAsDriver;
    objectStats.passenger = statsTraveledCarAsPassenger;
    objectStats.walking = statsWalking;
    mp.events.callRemote("callServer::updatePlayerTime", JSON.stringify(objectStats), isPlayerAfk);
});

mp.events.add('render', () => {
    if (!player.getVariable("playerNoclip")) {
        if (player.vehicle) { //liczenie dla auta
            var distance = mp.game.gameplay.getDistanceBetweenCoords(player.vehicle.position.x, player.vehicle.position.y, player.vehicle.position.z, lastStatsPosition.x, lastStatsPosition.y, lastStatsPosition.z, true);
            if (distance > 0.001) {
                if (playerVehicleSeat == -1) {
                    statsTraveledCarAsDriver = statsTraveledCarAsDriver + parseFloat(distance / 1000); //km
                }
                else if (playerVehicleSeat > -1) {
                    statsTraveledCarAsPassenger = statsTraveledCarAsPassenger + parseFloat(distance / 1000); //km
                }
            }
            lastStatsPosition = player.vehicle.position;
        } else { //liczenie pieszo
            if (canCountWalking) {
                var distance = mp.game.gameplay.getDistanceBetweenCoords(player.position.x, player.position.y, player.position.z, lastStatsPosition.x, lastStatsPosition.y, lastStatsPosition.z, true);
                if (distance > 0.001) {
                    statsWalking = statsWalking + parseFloat(distance / 1000); //km
                }
            }
            lastStatsPosition = player.position;
        }
    }else{
        lastStatsPosition = player.position;
        isPlayerAfk = false;
        isWSADpressing = true;
        noMoveTime = 0;
    }
    //dodać w przyszlosci klawisze F, G, ENTER, SPACE, SHIFT
    if ((mp.game.controls.isControlJustPressed(32, 32) || mp.game.controls.isControlJustPressed(32, 33) || mp.game.controls.isControlJustPressed(32, 34) || mp.game.controls.isControlJustPressed(32, 35)) && isPlayerAfk) {
        isPlayerAfk = false;
        isWSADpressing = true;
        noMoveTime = 0;
        objectStats.total = totalGameTime;
        objectStats.afk = awayFromKeyboardTime;
        objectStats.drive = statsTraveledCarAsDriver;
        objectStats.passenger = statsTraveledCarAsPassenger;
        objectStats.walking = statsWalking;
        mp.events.callRemote("callServer::updatePlayerTime", JSON.stringify(objectStats), isPlayerAfk);
    }

    //sprawdzanie czy WSAD kliknięto
    if (mp.game.controls.isControlPressed(32, 32) || mp.game.controls.isControlPressed(32, 33) || mp.game.controls.isControlPressed(32, 34) || mp.game.controls.isControlPressed(32, 35)) {
        isWSADpressing = true;
        noMoveTime = 0;
        isPlayerAfk = false;
    } else {
        isWSADpressing = false;
    }
});

mp.events.add("playerEnterVehicle", (vehicle, seat) => {
    //mp.events.callRemote("callServer::sendMessageToPlayer", "Seat: " + seat + " dirt" + parseFloat(vehicle.getDirtLevel()).toFixed(3), "orange");
    playerVehicleSeat = seat;
});

mp.events.add("playerCommand", (command) => {
    const args = command.split(/[ ]+/);
    const commandName = args.splice(0, 1)[0];

    if (commandName === "stats") {
        var onlineTime = parseInt(totalGameTime / 3600) + "h " + parseInt(totalGameTime / 60) + "m " + (totalGameTime % 60) + "s";
        var AFKTime = parseInt(awayFromKeyboardTime / 3600) + "h " + parseInt(awayFromKeyboardTime / 60) + "m " + (awayFromKeyboardTime % 60) + "s";
        mp.events.callRemote("callServer::sendMessageToPlayer", "<b>Statystki tej sesji</b><br>kierowca: "
            + statsTraveledCarAsDriver.toFixed(3) + " km<br>pasażer: "
            + statsTraveledCarAsPassenger.toFixed(3) + " km<br>pieszo: "
            + statsWalking.toFixed(3) + " km<br>czas online: "
            + onlineTime + "<br>czas AFK: "
            + AFKTime, "blue");
    }
});