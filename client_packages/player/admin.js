let browserAdmin = null;
const player = mp.players.local;
var lastKeyPressedTime = Date.now();
var playerListInterval = null;

//from html
mp.events.add("callFromHTML::onAdminPanelAction", function (event, args) {
    args = JSON.parse(args);
    switch (event) {
        case "get_user_by_id":
            mp.events.callRemote("callServer::onAdminPanelAction", "get_user_by_id", args[0], null, null);
            break;
        case "get_users_list":
            getPlayerList();
            break;
        case "get_last_players":
            mp.events.callRemote("callServer::onAdminPanelAction", "get_last_players");
            break;
        case "kick_player":
            mp.events.callRemote("callServer::onAdminPanelAction", "kick_player", args[0], args[1], null); //playerid, reason, time
            break;
        case "ban_player":
            mp.events.callRemote("callServer::onAdminPanelAction", "ban_player", args[0], args[1], 60 * 5); //playerid, reason, time
            break;
        case "mute_player":
            break;
        default:
            break;
    }
});

//from server
mp.events.add("callClient::onAdminPanelAction", function (event, args) {
    switch (event) {
        case "update_last_players":
            if (browserAdmin) {
                var list = "`" + args + "`";
                browserAdmin.execute(`updateLastPlayerList(${list})`)
            }
            break;
        case "get_user_by_id":
            if (browserAdmin) {
                var list = "`" + args + "`";
                browserAdmin.execute(`updatePlayerModal(${list})`)
            }
            break;
        default:
            break;
    }
});

//F9 key pressed
mp.keys.bind(0x78, true, function () {
    if (!isPlayerTyping && player.getVariable("playerData")) {
        if (player.getVariable("playerData").admin === "ADMIN") {
            var currentTime = Date.now();
            if ((currentTime - lastKeyPressedTime) >= 200) {
                if (browserAdmin == null) {
                    createBrowser();
                }
                else {
                    destroyBrowser();
                }
                lastKeyPressedTime = Date.now();
            }

        }
    }
});

function createBrowser() {
    if (browserAdmin == null) {
        browserAdmin = mp.browsers.new("package://CEF/admin-panel/index.html");
        mp.gui.cursor.show(true, true);
        isAnyBrowserOpen = true;
        mp.events.call("call::canPlayerUseChat", false);
        getPlayerList();
        playerListInterval = setInterval(getPlayerList, 2000);
    }
}

function destroyBrowser() {
    if (browserAdmin) {
        browserAdmin.destroy();
        browserAdmin = null;
        mp.gui.cursor.show(false, false);
        isAnyBrowserOpen = false;
        mp.events.call("call::canPlayerUseChat", true);
        if (playerListInterval != null) {
            clearInterval(playerListInterval);
        }
    }
}

function getPlayerList() {
    if (browserAdmin) {
        //mp.events.call("call::addNotify", "Lista", "lista graczy", "green");
        var userListToSend = [];

        mp.players.forEach(
            (playerList, id) => {
                var playerData = playerList.getVariable("playerData");
                if (playerData) {
                    var playerObj = {
                        nick: playerData.nick,
                        id: playerData
                    }
                    userListToSend.push(playerObj)
                }
            });

        var list = "`" + JSON.stringify(userListToSend) + "`";
        browserAdmin.execute(`updatePlayerList(${list})`)
        userListToSend = [];
        userListToSend = null;
    }
}

mp.events.add("playerCommand", (command) => {
    const args = command.split(/[ ]+/);
    const commandName = args.splice(0, 1)[0];
    //mp.events.callRemote("callServer::sendMessageToPlayer", "Ustawiam handling "+args+" "+args.length, "blue");
    // args.shift();
    var playerData = player.getVariable("playerData");
    if (playerData) {
        if (playerData.admin == "ADMIN") {
            var reason = ""
            for (var i = 2; i < args.length; i++) {
                reason = reason + " " + args[i];
            }

            if (commandName === "kick") {
                mp.events.callRemote("callServer::onAdminPanelAction", "kick_player", args[0], reason, null); //playerid, reason, time
            } else if (commandName === "ban") {
                mp.events.callRemote("callServer::onAdminPanelAction", "ban_player", args[0], reason, parseInt(args[1])); //playerid, reason, time
            } else if (commandName === "mute") {
                mp.events.callRemote("callServer::onAdminPanelAction", "mute_player", args[0], reason, parseInt(args[1])); //playerid, reason, time
            }
        }
    }
});