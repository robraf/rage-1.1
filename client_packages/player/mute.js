var player = mp.players.local;

var timeout = null;
//onLoginReady itd

//kiedy nadano mute
function checkMuteTimeTo(serverTime) {
    var muteTime = player.getVariable("playerMuted");
    if (muteTime) {
        muteTime = parseInt(muteTime);
        var diffrence = muteTime-serverTime;
        timeout = setTimeout(function () {
            mp.events.callRemote("callServer::removeMute")
            mp.events.call("call::addNotify", "Czat", "Mute się zakończył, możesz teraz pisać na czacie", "green", null);
        }, diffrence+100)
    }
}
mp.events.add("callClient::addedMute", checkMuteTimeTo)
