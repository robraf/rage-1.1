var blipList = [];
var player = mp.players.local;
var BLIP_UPDATE_TIME = 50; //miliseconds
var blipVisible = 1;
var playerList = [];

/*
zamiast usuwac blipy, updatowac pozycje
*/

function updateBlipPositions() {
    if (player.getVariable("playerData")) {
        //clearBlipList();

        //aktualizacja dla najbliższych
        mp.players.forEachInStreamRange(
            (playerObj, id) => {

                var playerData = playerObj.getVariable("playerData");
                if (playerData != null && playerData!=undefined) {
                    if (playerData.spawned && !playerData.inCreator && !playerObj.getVariable("playerNoclip")) {
                        var distance = mp.game.gameplay.getDistanceBetweenCoords(
                            player.position.x,
                            player.position.y,
                            player.position.z,
                            playerObj.position.x,
                            playerObj.position.y,
                            playerObj.position.z,
                            false
                        );
                        if (distance <= 400) {
                            var playerSessionTimes = playerObj.getVariable("playerSessionStats");
                            if (playerSessionTimes !== null && playerSessionTimes.total > 0) {

                                var playerSessionId = playerObj.getVariable("playerSessionID");
                                if (playerSessionId >= 0) {
                                    if (blipList[playerSessionId] === null || blipList[playerSessionId] === undefined) {

                                        var playerBlip = mp.blips.new(1, new mp.Vector3(playerObj.position.x, playerObj.position.y, playerObj.position.z,),
                                            {
                                                name: "" + playerData.nick,
                                                color: 4,
                                                scale: 0.65
                                            });

                                        playerBlip.setCategory(7);
                                        playerBlip.setPriority(2);
                                        playerBlip.setFlashes(false);
                                        //playerBlip.setDisplay(8);

                                        blipList[playerSessionId] = playerBlip;

                                    } else {
                                        //zupdatować aktualny blip o nowe informacje
                                        var currentBlip = blipList[playerSessionId];
                                        currentBlip.setCoords(playerObj.position)
                                        //currentBlip.setDisplay(3);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        );
    }
}

function updateFarPlayers() {
    for (var i = 0; i < playerList.length; i++) { //forEachInDimension, forEachInStreamRange
        // mp.players.forEach((playerFor, id) => {
        var playerObj = playerList[i];
        var distance = mp.game.gameplay.getDistanceBetweenCoords(
            player.position.x,
            player.position.y,
            player.position.z,
            playerObj.position.x,
            playerObj.position.y,
            playerObj.position.z,
            false
        );
        if (distance >= 400) {
            if (playerObj != null && playerObj!=undefined) {

                if (playerObj.spawned && playerObj.totalTime > 0) { //sprawdzac czy obserwer potem jako admin
                    var playerSessionId = playerObj.sessionId;
                    if (blipList[playerSessionId] === null || blipList[playerSessionId] === undefined) {

                        var playerBlip = mp.blips.new(1, new mp.Vector3(playerObj.position.x, playerObj.position.y, playerObj.position.z,),
                            {
                                name: "" + playerObj.nick,
                                color: 4,
                                scale: 0.65
                            });
                        //playerBlip.setRotation(180);
                        playerBlip.setCategory(7);
                        playerBlip.setPriority(2);
                        playerBlip.setFlashes(false);
                        //playerBlip.setDisplay(3);

                        blipList[playerSessionId] = playerBlip;

                    } else {

                        //zupdatować aktualny blip o nowe informacje
                        var currentBlip = blipList[playerSessionId];
                        currentBlip.setCoords(playerObj.position)

                        //currentBlip.setDisplay(3);

                    }
                }
                //blipList.push(playerBlip);
            }
        }
        // });
    }
}

function clearBlipList() {
    for (var i = 0; i < blipList.length; i++) {
        blipList[i].destroy();
    }
    blipList = [];
}
//blip.setDisplay(displayID); 
/*
2(selectable)/8(nieselectable) na minimapie i mapie
3/4 tylko na duzej mapie (np. kiedy w promieniu 1km)
*/

var blipUpdateInterval = setInterval(function () {
    updateBlipPositions();
}, BLIP_UPDATE_TIME);

function changeBlipUpdateTime(newTime) {
    BLIP_UPDATE_TIME = newTime;
    clearInterval(blipUpdateInterval);
    blipUpdateInterval = setInterval(function () {
        updateBlipPositions();
    }, BLIP_UPDATE_TIME);
}
mp.events.add("callClient::changeBlipUpdateTime", changeBlipUpdateTime)

function changeBlipVisible(value) {
    blipVisible = value;
    if (value == 1) {
        if (blipUpdateInterval != null) {
            clearInterval(blipUpdateInterval);
        }
        blipUpdateInterval = setInterval(function () {
            updateBlipPositions();
        }, BLIP_UPDATE_TIME);

    } else {
        clearInterval(blipUpdateInterval);
        blipUpdateInterval = null;
        clearBlipList();
    }
}
mp.events.add("callClient::changeBlipVisible", changeBlipVisible)


mp.events.add("callClient::sendPlayerList", (array) => {
    //mp.events.callRemote("callServer::sendMessageToPlayer", "test", "red");
    playerList = [];

    playerList = JSON.parse(array);
    updateFarPlayers();
});


mp.events.add("callClient::blipPlayerManager", (event, playerSessionId, boolean) => {
    switch (event) {
        case "incognito_blip":
            var blip = blipList[playerSessionId];
            if (boolean == 1) {
                blip.setAlpha(0);
            }
            else {
                blip.setAlpha(255);
            }
            break;
        case "remove_blip":
            //mp.events.callRemote("callServer::sendMessageToPlayer", "wyszedl gracz o id sesyjnym: " + playerSessionId, "red");
            if(blipList[playerSessionId] !== null && blipList[playerSessionId] !== undefined){
                blipList[playerSessionId].destroy();
                blipList[playerSessionId] = null;
            }   
            break;
        default:
            break;
    }
});
