const player = mp.players.local;
var refreshCharacter = setInterval(refreshPlayersCharacter, 1 * 1000);
var characterCamera = null;
const DEFAULT_HEADING = 252;
var browserCharacter = null;

var appearanceObject = {
    created: 0,
    sex: "m",
    eye: 0,
    face: "0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0",
    hair: 0,
    hairColor1: 0,
    hairColor2: 0,
    mother: 21,
    father: 0,
    shape: 0.5,
    skin: 0.5,
    blemishes: 255,
    facialhair: 255,
    facialhairColor: 0,
    eyebrows: 255,
    eyebrowsColor: 0,
    ageing: 255,
    makeup: 255,
    makeupColor1: 0,
    makeupColor2: 0,
    blush: 255,
    blushColor: 0,
    complexion: 255,
    sundamage: 255,
    lipstick: 255,
    lipstickColor: 0,
    moles: 255
}

const defaultObject = {
    created: 0,
    sex: "m",
    eye: 0,
    face: "0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0",
    hair: 0,
    hairColor1: 0,
    hairColor2: 0,
    mother: 21,
    father: 0,
    shape: 0.5,
    skin: 0.5,
    blemishes: 255,
    facialhair: 255,
    facialhairColor: 0,
    eyebrows: 255,
    eyebrowsColor: 0,
    ageing: 255,
    makeup: 255,
    makeupColor1: 0,
    makeupColor2: 0,
    blush: 255,
    blushColor: 0,
    complexion: 255,
    sundamage: 255,
    lipstick: 255,
    lipstickColor: 0,
    moles: 255
}

function setPlayerCharacter(playerObject, object) {
    var playerAppearance = object;
    if (playerAppearance != null && playerAppearance != undefined) {
        var isAnyValueNull = false;
        for (var i = 0; i < playerAppearance.length; i++) {
            if (playerAppearance[i] == null || playerAppearance[i] == undefined) {
                isAnyValueNull = true;
                break;
            }
        }
        if (!isAnyValueNull) {
            playerObject.model = mp.game.joaat("mp_" + playerAppearance.sex + "_freemode_01");
            playerObject.setEyeColor(parseInt(playerAppearance.eye));
            var face = (playerAppearance.face).split(",");
            for (var i = 0; i < face.length; i++) {
                playerObject.setFaceFeature(parseInt(i), parseInt(face[i]));
            }
            playerObject.setComponentVariation(2, parseInt(playerAppearance.hair), 0, 0);
            playerObject.setHairColor(parseInt(playerAppearance.hairColor1), parseInt(playerAppearance.hairColor2));
            playerObject.setHeadBlendData(
                parseInt(playerAppearance.mother), parseInt(playerAppearance.father), 0,
                parseInt(playerAppearance.mother), parseInt(playerAppearance.father), 0,
                parseFloat(playerAppearance.shape), parseFloat(playerAppearance.skin), 0.0, false);

            playerObject.setHeadOverlay(0, playerAppearance.blemishes, 1, 0, 0);

            playerObject.setHeadOverlay(1, playerAppearance.facialhair, 1, 0, 0);
            playerObject.setHeadOverlayColor(1, 1, playerAppearance.facialhairColor, playerAppearance.facialhairColor);

            playerObject.setHeadOverlay(2, playerAppearance.eyebrows, 1, 0, 0);
            playerObject.setHeadOverlayColor(2, 1, playerAppearance.eyebrowsColor, playerAppearance.eyebrowsColor);

            playerObject.setHeadOverlay(3, playerAppearance.ageing, 1, 0, 0);

            playerObject.setHeadOverlay(4, playerAppearance.makeup, 1, 0, 0);
            playerObject.setHeadOverlayColor(4, 1, playerAppearance.makeupColor1, playerAppearance.makeupColor2);

            playerObject.setHeadOverlay(5, playerAppearance.blush, 1, 0, 0);
            playerObject.setHeadOverlayColor(5, 1, playerAppearance.blushColor, playerAppearance.blushColor);

            playerObject.setHeadOverlay(6, playerAppearance.complexion, 1, 0, 0);

            playerObject.setHeadOverlay(7, playerAppearance.sundamage, 1, 0, 0);

            playerObject.setHeadOverlay(8, playerAppearance.lipstick, 1, 0, 0);
            playerObject.setHeadOverlayColor(8, 1, playerAppearance.lipstickColor, playerAppearance.lipstickColor);

            playerObject.setHeadOverlay(9, playerAppearance.moles, 1, 0, 0);

        }
    }
}

function refreshPlayersCharacter() {
    mp.players.forEachInStreamRange((playerObj, id) => {
        var playerData = playerObj.getVariable("playerData");
        if (playerData != null && playerData != undefined) {
            if (!playerData.inCreator) {
                var distance = mp.game.gameplay.getDistanceBetweenCoords(
                    player.position.x,
                    player.position.y,
                    player.position.z,
                    playerObj.position.x,
                    playerObj.position.y,
                    playerObj.position.z,
                    false
                );
                if (distance <= 300) {
                    setPlayerCharacter(playerObj, playerObj.getVariable("playerAppearance"));
                }
            }
        }
    });
}


//creator 
function setPlayerInCreator(event, apperaranceList) {
    if (event === "set_player_in_creator") {
        var playerSession = player.getVariable("playerSessionID");
        if (playerSession != null && playerSession != undefined) {
            if (playerSession > -1) {
                //odblurować ekran (po zakończeniu zablurować)
                mp.gui.cursor.show(true, true);
                player.dimension = playerSession + 1;
                player.clearTasksImmediately();
                player.freezePosition(true);
                player.clearFacialIdleAnimOverride(); //ew. usunąć
               
                var list = JSON.parse(apperaranceList);
                appearanceObject.created = list.created,
                appearanceObject.sex= list.sex,
                appearanceObject.eye=  list.eye,
                appearanceObject.face= list.face,
                appearanceObject.hair= list.hair,
                appearanceObject.hairColor1= list.hairColor1,
                appearanceObject.hairColor2= list.hairColor2,
                appearanceObject.mother= list.mother,
                appearanceObject.father= list.father,
                appearanceObject.shape= list.shape,
                appearanceObject.skin = list.skin,
                appearanceObject.blemishes = list.blemishes,
                appearanceObject.facialhair= list.facialhair,
                appearanceObject.facialhairColor= list.facialhairColor,
                appearanceObject.eyebrows = list.eyebrows,
                appearanceObject.eyebrowsColor= list.eyebrowsColor,
                appearanceObject.ageing= list.ageing,
                appearanceObject.makeup= list.makeup,
                appearanceObject.makeupColor1= list.makeupColor1,
                appearanceObject.makeupColor2= list.makeupColor2,
                appearanceObject.blush= list.blush,
                appearanceObject.blushColor= list.blushColor,
                appearanceObject.complexion= list.complexion,
                appearanceObject.sundamage= list.sundamage,
                appearanceObject.lipstick= list.lipstick,
                appearanceObject.lipstickColor= list.lipstickColor,
                appearanceObject.moles= list.moles
                //mp.events.callRemote("callServer::addConsoleLog", "Obiekt "+JSON.stringify(appearanceObject));
                setPlayerCharacter(player, appearanceObject);
               
                /*
                ZABLKOWAC CALKOWICIE PORUSZANIE SIE
                */
                player.position = new mp.Vector3(436.19, -993.30, 30.68);
                player.model = mp.game.joaat("mp_m_freemode_01");
                player.setHeading(DEFAULT_HEADING);
                player.setVisible(true, true);
                mp.game.graphics.transitionFromBlurred(0);
                characterCamera = mp.cameras.new("characterCamera", new mp.Vector3(437.00, -993.55, 30.68 + 0.6), new mp.Vector3(0, 0, 0), 60);
                characterCamera.pointAtCoord(436.19, -993.55, 30.68 + 0.55);
                setTimeout(function () {
                    characterCamera.setActive(true);
                    mp.game.cam.renderScriptCams(true, false, 0, true, false);
                    createBrowser();
                }, 1 * 1000)
                //wyłączyć CHAT

                //acceptPlayer();
            }
        }
    }
}
mp.events.add("call::playerCreatorBrowser", setPlayerInCreator);


function receivedFromHTML(event, values) {
    var values = JSON.parse(values);
    switch (event) {
        case "rotate":
            var direction = values[0];
            rotatePlayer(direction);
            break;
        case "change_element":
            changePlayerElement(values)
            break;
        case "reset":
            resetPlayerElements();
            break;
        case "save":
            acceptPlayer();
            break;
        default:
            break;
    }
}
mp.events.add("callFromHTML::playerCreatorBrowser", receivedFromHTML);


function changePlayerElement(values) {
    //mp.events.callRemote("callServer::addConsoleLog", "WCHODZE W CHANGE PLAYER ELEMENT");
    switch (values.element) {
        case "sex":
            appearanceObject.sex = values.value;
            break;
        case "eye":
            appearanceObject.eye = values.value;
            break;
        case "face":
            appearanceObject.face = values.value;
            break;
        case "hair":
            appearanceObject.hair = values.value;
            break;
        case "hairColor1":
            appearanceObject.hairColor1 = values.value;
            break;
        case "hairColor2":
            appearanceObject.hairColor2 = values.value;
            break;
        case "mother":
            appearanceObject.mother = values.value;
            break;
        case "father":
            appearanceObject.father = values.value;
            break;
        case "shape":
            appearanceObject.shape = values.value;
            break;
        case "skin":
            appearanceObject.skin = values.value;
            break;
        case "blemishes":
            appearanceObject.blemishes = values.value;
            break;
        case "facialhair":
            appearanceObject.facialhair = values.value;
            break;
        case "facialhairColor":
            appearanceObject.facialhairColor = values.value;
            break;
        case "eyebrows":
            appearanceObject.eyebrows = values.value;
            break;
        case "eyebrowsColor":
            appearanceObject.eyebrowsColor = values.value;
            break;
        case "ageing":
            appearanceObject.ageing = values.value;
            break;
        case "makeup":
            appearanceObject.makeup = values.value;
            break;
        case "makeupColor1":
            appearanceObject.makeupColor1 = values.value;
            break;
        case "makeupColor2":
            appearanceObject.makeupColor2 = values.value;
            break;
        case "blush":
            appearanceObject.blush = values.value;
            break;
        case "blushColor":
            appearanceObject.blushColor = values.value;
            break;
        case "complexion":
            appearanceObject.complexion = values.value;
            break;
        case "sundamage":
            appearanceObject.sundamage = values.value;
            break;
        case "lipstick":
            appearanceObject.lipstick = values.value;
            break;
        case "lipstickColor":
            appearanceObject.lipstickColor = values.value;
            break;
        case "moles":
            appearanceObject.moles = values.value;
            break;
        default:
            break;

    }
    setPlayerCharacter(player, appearanceObject);
}

function acceptPlayer() {
    player.setVisible(false, false);
    player.dimension = 0;
    //mp.game.graphics.transitionToBlurred(0);
    if (characterCamera != null) {
        characterCamera.destroy();
        characterCamera = null;
    }
    appearanceObject.created = 1;
    setPlayerCharacter(player, appearanceObject);
    mp.events.callRemote("callServer::onPlayerInCreator", "player_save_character", JSON.stringify(appearanceObject));
    destroyBrowser();
    mp.events.call("call::onSpawnSelectAction", "start_camera_anim", null);
    mp.events.callRemote("callServer::onSpawnAction", "get_list");
}

function rotatePlayer(value) {
    player.setHeading(DEFAULT_HEADING + value);
}



function resetPlayerElements() {
    setPlayerCharacter(player, defaultObject);
    for (appearance in appearanceObject) {
        appearanceObject.appearance = defaultObject.appearance;
    }
}

function createBrowser() {
    if (browserCharacter == null) {
        browserCharacter = mp.browsers.new("package://CEF/creator-panel/index.html");
        var appearanceList =  "`" + JSON.stringify(appearanceObject) + "`";
        browserCharacter.execute(`setHTMLelements(${appearanceList})`)
        mp.gui.cursor.show(true, true);
    }
}

function destroyBrowser() {
    if (browserCharacter) {
        browserCharacter.destroy();
        browserCharacter = null;
    }
}