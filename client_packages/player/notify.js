var browserNotify = mp.browsers.new("package://CEF/notifications/index.html");
      
function addNotify(notifyHeader, notifyText, notifyColor, notifyIcon){
    if(browserNotify){
        var object = {header: notifyHeader, content: notifyText, color: notifyColor, icon: notifyIcon};
        list = "`" + JSON.stringify(object) + "`";
        browserNotify.execute(`newNotify(${list})`);
    }
}
mp.events.add("call::addNotify", addNotify);
mp.events.add("callClient::addNotify", addNotify);