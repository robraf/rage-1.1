var bigmap = [];
let player = mp.players.local;

const Natives = {
    IS_RADAR_HIDDEN: "0x157F93B036700462",
    IS_RADAR_ENABLED: "0xAF754F20EB5CD51A",
    SET_TEXT_OUTLINE: "0x2513DFB0FB8400FE",
    GET_SAFE_ZONE_SIZE: "0xBAF107B6BB2C97F0"
};

bigmap.status = 0;
bigmap.timer = null;
mp.game.ui.setRadarZoom(1.0);
mp.game.ui.setRadarBigmapEnabled(false, false);

mp.events.add("render", () => {
    mp.game.ui.hideHudComponentThisFrame(1);
    mp.game.ui.hideHudComponentThisFrame(3);
    mp.game.ui.hideHudComponentThisFrame(4);
    mp.game.ui.hideHudComponentThisFrame(6);
    mp.game.ui.hideHudComponentThisFrame(7);
    mp.game.ui.hideHudComponentThisFrame(8);
    mp.game.ui.hideHudComponentThisFrame(9);
    mp.game.ui.hideHudComponentThisFrame(10);
    mp.game.ui.hideHudComponentThisFrame(11);
    mp.game.ui.hideHudComponentThisFrame(12);
    mp.game.ui.hideHudComponentThisFrame(13);
    mp.game.ui.hideHudComponentThisFrame(14);
    mp.game.ui.hideHudComponentThisFrame(15);
    mp.game.ui.hideHudComponentThisFrame(18);
    mp.game.ui.hideHudComponentThisFrame(17);
    mp.game.ui.hideHudComponentThisFrame(22);
    mp.game.ui.hideHudComponentThisFrame(21);
    mp.game.ui.hideHudComponentThisFrame(141);

    //attacks
    mp.game.controls.disableControlAction(32, 24, true);
    mp.game.controls.disableControlAction(2, 44, true);
    mp.game.controls.disableControlAction(2, 69, true);
    mp.game.controls.disableControlAction(2, 70, true);
    mp.game.controls.disableControlAction(2, 92, true);
    mp.game.controls.disableControlAction(2, 114, true);
    mp.game.controls.disableControlAction(2, 121, true);
    mp.game.controls.disableControlAction(32, 140, true);
    mp.game.controls.disableControlAction(32, 141, true);
    mp.game.controls.disableControlAction(32, 142, true);
    mp.game.controls.disableControlAction(32, 257, true);
    mp.game.controls.disableControlAction(2, 263, true);
    mp.game.controls.disableControlAction(2, 264, true);
    mp.game.controls.disableControlAction(2, 331, true);

    mp.game.controls.disableControlAction(0, 48, true);
    if (mp.game.controls.isDisabledControlJustPressed(0, 48) && !isPlayerTyping) {

        if (bigmap.status === 0) {

            mp.game.ui.setRadarZoom(0.0);
            bigmap.status = 1;

            bigmap.timer = setTimeout(() => {

                mp.game.ui.setRadarBigmapEnabled(false, true);
                mp.game.ui.setRadarZoom(1.0);

                bigmap.status = 0;
                bigmap.timer = null;

            }, 3 * 1000);

        } else if (bigmap.status === 1) {

            if (bigmap.timer != null) {
                clearTimeout(bigmap.timer);
                bigmap.timer = null;
            }

            mp.game.ui.setRadarBigmapEnabled(true, false);
            mp.game.ui.setRadarZoom(0.0);
            bigmap.status = 2;

            bigmap.timer = setTimeout(() => {

                mp.game.ui.setRadarBigmapEnabled(false, true);
                mp.game.ui.setRadarZoom(1.0);

                bigmap.status = 0;
                bigmap.timer = null;

            }, 3 * 1000);

        } else {

            if (bigmap.timer != null) {
                clearTimeout(bigmap.timer);
                bigmap.timer = null;
            }

            mp.game.ui.setRadarBigmapEnabled(false, false);
            mp.game.ui.setRadarZoom(1.0);
            bigmap.status = 0;
        }
    }
    let minimap = getMinimapAnchor();

    //AREA
    if (minimap !== null && minimap !== undefined) {

        let getStreet = mp.game.pathfind.getStreetNameAtCoord(player.position.x, player.position.y, player.position.z, 0, 0);
        // Returns obj {"streetName": hash, crossingRoad: hash}

        let streetName = mp.game.ui.getStreetNameFromHashKey(getStreet.streetName); // Return string, if exist
        let crossingRoad = mp.game.ui.getStreetNameFromHashKey(getStreet.crossingRoad); // Return string, if exist
        let crossingRoadName = "";
        if (crossingRoad !== null && crossingRoad !== undefined && crossingRoad !== "") {
            crossingRoadName = " / róg " + crossingRoad;
        }
        let zone = mp.game.gxt.get(mp.game.zone.getNameOfZone(player.position.x, player.position.y, player.position.z));
        let playerData = player.getVariable("playerData");
        if (playerData !== null && playerData !== undefined && playerData.spawned) {
            drawText(zone + " (ul. " + streetName + crossingRoadName + ")", [minimap.leftX + 0.153, minimap.bottomY - 0.030], 4, [255, 255, 255, 255], 0.35);

            //vehicle data
            if (player.vehicle) {
                let vehicleName = mp.game.vehicle.getDisplayNameFromVehicleModel(player.vehicle.model);
                let vehicleData = player.vehicle.getVariable("vehicleData");
                if (vehicleData !== null && vehicleData !== undefined) {
                    let engineType = vehicleData.engineType;
                    let enginePower = vehicleData.enginePower;
                    let maxSpeed = vehicleData.maxSpeed;
                    let fuelUsage = vehicleData.fuelUsage;
                    let fuelTank = vehicleData.fuelTank;
                    let trunkCapacity = vehicleData.trunkCapacity;
                    let handlingEngine = vehicleData.handlingEngine;
                    let vehicleDetails = "Pojazd: "+vehicleName+
                    "\nSilnik: "+engineType+" ("+enginePower+")"+
                    "\nSpalanie "+fuelUsage+" l/1km (bak: "+fuelTank+" l)";
                    let vehicleDetails1 = "Maks. prędkość: "+maxSpeed+" km/h, bagażnik: "+trunkCapacity+" kg"+
                    "\nEngine: "+handlingEngine;

                    let vehicleDetails2 = "Kondycja silnika: "+(player.vehicle.getEngineHealth()).toFixed(3)+"\nKondycja karoserii: "+(player.vehicle.getBodyHealth()).toFixed(3); 
                   
                    drawText(vehicleDetails, [minimap.leftX + 0.153, minimap.bottomY - 0.150], 4, [125, 155, 255, 255], 0.35);
                    drawText(vehicleDetails1, [minimap.leftX + 0.153, minimap.bottomY - 0.090], 4, [125, 155, 255, 255], 0.35);
                    drawText(vehicleDetails2, [minimap.leftX + 0.153, minimap.bottomY - 0.190], 4, [25, 55, 255, 255], 0.35);

                    let fuelData = player.vehicle.getVariable("vehicleFuel");
                    if (fuelData !== null && fuelData !== undefined) {
                        drawText("Paliwo: "+fuelData.toFixed(5)+ " l", [minimap.leftX + 0.253, minimap.bottomY - 0.070], 4, [125, 155, 055, 255], 0.4);
                    }
                    let mileageData = player.vehicle.getVariable("vehicleMileage");
                    if (mileageData !== null && mileageData !== undefined) {
                        drawText("Przebieg: "+mileageData.toFixed(3)+ "km", [minimap.leftX + 0.253, minimap.bottomY - 0.050], 4, [125, 155, 055, 255], 0.4);
                    }
                   
                }
            }
        }

    }
});

function getMinimapAnchor() {
    try {
        let sfX = 1.0 / 20.0;
        let sfY = 1.0 / 20.0;
        let safeZone = mp.game.graphics.getSafeZoneSize();
        let aspectRatio = mp.game.graphics.getScreenAspectRatio(false);
        let resolution = mp.game.graphics.getScreenActiveResolution(0, 0);
        let scaleX = 1.0 / resolution.x;
        let scaleY = 1.0 / resolution.y;

        let minimap = {
            width: scaleX * (resolution.x / (4 * aspectRatio)),
            height: scaleY * (resolution.y / 5.674),
            scaleX: scaleX,
            scaleY: scaleY,
            leftX: scaleX * (resolution.x * (sfX * (Math.abs(safeZone - 1.0) * 10))),
            bottomY: 1.0 - scaleY * (resolution.y * (sfY * (Math.abs(safeZone - 1.0) * 10))),
        };

        minimap.rightX = minimap.leftX + minimap.width;
        minimap.topY = minimap.bottomY - minimap.height;
        return minimap;
    } catch (error) { }
}

function drawText(text, drawXY, font, color, scale, alignRight = false) {
    try {
        mp.game.ui.setTextEntry("STRING");
        mp.game.ui.addTextComponentSubstringPlayerName(text);
        mp.game.ui.setTextFont(font);
        mp.game.ui.setTextScale(scale, scale);
        mp.game.ui.setTextColour(color[0], color[1], color[2], color[3]);
        mp.game.invoke(Natives.SET_TEXT_OUTLINE);

        if (alignRight) {
            mp.game.ui.setTextRightJustify(true);
            mp.game.ui.setTextWrap(0, drawXY[0]);
        }

        mp.game.ui.drawText(drawXY[0], drawXY[1]);
    } catch (error) { }
}