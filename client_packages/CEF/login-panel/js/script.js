$('#nickname_login_info').hide();
$('#password_login_info').hide();
$('#server_running_info').hide();
$('#nickname_info').hide();
$('#password_info').hide();
$('#password_repeat_info').hide();
$('.alert-success').hide();
$('.alert-danger').hide();
$('.loader-login').hide();
$('.loader-register').hide();

const container = document.querySelector('.container');
const left = document.querySelector('.left');
const text = document.querySelector('.text');
const tabs = document.querySelector('.tabs');
const login = document.querySelector('.right-login');
const register = document.querySelector('.right-register');
const changelog = document.querySelector('.changelog');
const changelog_content = document.querySelector('.changelog-content');

var log_vis = window.getComputedStyle(login).getPropertyValue("opacity");
var reg_vis = window.getComputedStyle(register).getPropertyValue("opacity");
var cha_vis = window.getComputedStyle(changelog).getPropertyValue("opacity");

const tl = new TimelineMax();

tl.fromTo(left, 1, { top: "50%", height: "1%" }, { top: "0%", height: "100%", ease: Power2.easeInOut })
    .fromTo(tabs, 1, { left: "65%", opacity: "0" }, { left: "54%", opacity: "1", ease: Power2.easeInOut }, "-=0.5");

function show_login() {
    if (cha_vis == 1) {
        tl.fromTo(left, 1, { width: "95%", opacity: "0.1" }, { width: "65%", opacity: "1", ease: Power2.easeInOut })
            .fromTo(changelog, 0.5, { opacity: "1" }, { opacity: "0" }, "-=0.5")
            .fromTo(text, 0.9, { opacity: "0" }, { opacity: "1" }, "-=0.9")
            .fromTo(tabs, 1, { left: "84%" }, { left: "54%", ease: Power2.easeInOut }, "-=1")
            .fromTo(login, 0, { visibility: "hidden" }, { visibility: "visible" }, "-=0.9")
            .fromTo(login, 0.3, { left: "95%" }, { left: "65%", ease: Power2.easeInOut }, "-=0.5")
            .fromTo(login, 0.3, { opacity: "0" }, { opacity: "1", ease: Power2.easeInOut }, "-=0.3")
            .fromTo(register, 0.3, { left: "95%" }, { left: "65%", ease: Power2.easeInOut }, "-=0.3")
            .fromTo(changelog, 0, { visibility: "visible" }, { visibility: "hidden" });
    } else if (log_vis == 0) {
        login.style.visibility = "visible";
        tl.fromTo(register, 0.6, { opacity: "1" }, { opacity: "0", ease: Power2.easeInOut })
            .fromTo(login, 0.6, { opacity: "0" }, { opacity: "1", ease: Power2.easeInOut }, "-=0.3")
            .fromTo(register, 0, { visibility: "visible" }, { visibility: "hidden" });
    }
    log_vis = 1;
    reg_vis = 0;
    cha_vis = 0;
}

function show_register() {
    if (cha_vis == 1) {
        tl.fromTo(left, 1, { width: "95%", opacity: "0.1" }, { width: "65%", opacity: "1", ease: Power2.easeInOut })
            .fromTo(changelog, 0.5, { opacity: "1" }, { opacity: "0" }, "-=0.5")
            .fromTo(text, 0.9, { opacity: "0" }, { opacity: "1" }, "-=0.9")
            .fromTo(tabs, 1, { left: "84%" }, { left: "54%", ease: Power2.easeInOut }, "-=1")
            .fromTo(register, 0, { visibility: "hidden" }, { visibility: "visible" }, "-=0.9")
            .fromTo(register, 0.3, { left: "95%" }, { left: "65%", ease: Power2.easeInOut }, "-=0.5")
            .fromTo(register, 0.3, { opacity: "0" }, { opacity: "1", ease: Power2.easeInOut }, "-=0.3")
            .fromTo(login, 0.3, { left: "95%" }, { left: "65%", ease: Power2.easeInOut }, "-=0.3")
            .fromTo(changelog, 0, { visibility: "visible" }, { visibility: "hidden" });
    } else if (reg_vis == 0) {
        register.style.visibility = "visible";
        tl.fromTo(login, 0.6, { opacity: "1" }, { opacity: "0", ease: Power2.easeInOut })
            .fromTo(register, 0.6, { opacity: "0" }, { opacity: "1", ease: Power2.easeInOut }, "-=0.3")
            .fromTo(login, 0, { visibility: "visible" }, { visibility: "hidden" });
    }
    log_vis = 0;
    reg_vis = 1;
    cha_vis = 0;
}

function show_changelog() {
    if (cha_vis == 0) {
        changelog.style.visibility = "visible";
        if (log_vis == 1) {
            tl.fromTo(login, 0.3, { opacity: "1" }, { opacity: "0", ease: Power2.easeInOut })
                .fromTo(login, 0.3, { left: "65%" }, { left: "95%", ease: Power2.easeInOut })
                .fromTo(register, 0.3, { left: "65%" }, { left: "95%", ease: Power2.easeInOut });
        } else {
            tl.fromTo(register, 0.3, { opacity: "1" }, { opacity: "0", ease: Power2.easeInOut })
                .fromTo(register, 0.3, { left: "65%" }, { left: "95%", ease: Power2.easeInOut })
                .fromTo(login, 0.3, { left: "65%" }, { left: "95%", ease: Power2.easeInOut });
        }
        tl.fromTo(left, 1, { width: "65%", opacity: "1" }, { width: "95%", opacity: "0.1", ease: Power2.easeInOut }, "-=1")
            .fromTo(tabs, 1, { left: "54%" }, { left: "84%", ease: Power2.easeInOut }, "-=1")
            .fromTo(text, 1, { opacity: "1" }, { opacity: "0" }, "-=1")
            .fromTo(changelog, 0.5, { opacity: "0" }, { opacity: "1" }, "-=0.5")
            .fromTo(login, 0, { visibility: "visible" }, { visibility: "hidden" })
            .fromTo(register, 0, { visibility: "visible" }, { visibility: "hidden" });
        log_vis = 0;
        reg_vis = 0;
        cha_vis = 1;
    }
}

function loading(button, enable) {
    var btn;
    switch (button) {
        case "login":
            btn = document.querySelector('.right-login .btn');
            if (enable) {
                btn.disabled = false;
                $('.loader-login').hide();
            } else {
                btn.disabled = true;
                $('.loader-login').show();
            }
            break;
        case "register":
            btn = document.querySelector('.right-register .btn');
            if (enable) {
                btn.disabled = false;
                $('.loader-register').hide();
            } else {
                btn.disabled = true;
                $('.loader-register').show();
            }
            break;
        default:
            break;
    }
}

document.getElementById('nickname_login').addEventListener("keypress", event => {
    if (/["'<>]+/.test(event.key)) {
        console.log("if");
        event.preventDefault();
    }
});
document.getElementById('nickname').addEventListener("keypress", event => {
    if (/["'<>]+/.test(event.key)) {
        console.log("if");
        event.preventDefault();
    }
});

function onLoginClick() {
    loading("login", false);
    var isDataCorrect = true;
    var nickname = document.getElementById('nickname_login').value;
    var password = document.getElementById('password_login').value;
    var remember = document.getElementById('remember').checked;

    if (nickname != "") {
        $('#nickname_login_info').hide();
        $('#nickname_login').attr('style', "border-bottom: 1px solid gray !important");
    } else {
        loading("login", true);
        $('#nickname_login').attr('style', "border-bottom: 1px solid red !important");
        document.getElementById('nickname_login_info').innerHTML = "Login nie może być pusty!";
        $('#nickname_login_info').show();
        isDataCorrect = false;
    }

    if (password != "") {
        $('#password_login_info').hide();
        $('#password_login').attr('style', "border-bottom: 1px solid gray !important");
    } else {
        loading("login", true);
        $('#password_login').attr('style', "border-bottom: 1px solid red !important");
        document.getElementById('password_login_info').innerHTML = "Hasło nie może być puste!";
        $('#password_login_info').show();
        isDataCorrect = false;
    }

    if (isDataCorrect) {
        mp.trigger("callFromHTML::onLoginPanelBrowserAction", "login_click", JSON.stringify([nickname, password, remember]));
    }
}

function onRegisterClick() {
    $('#register_info').hide();
    loading("register", false);
    var isDataCorrect = true;
    var nickname = document.getElementById('nickname').value;
    var password = document.getElementById('password').value;
    var password_repeat = document.getElementById('password_repeat').value;

    if (nickname != "") {
        $('#nickname_info').hide();
        $('#nickname').attr('style', "border-bottom: 1px solid gray !important");
    } else {
        loading("register", true);
        $('#nickname').attr('style', "border-bottom: 1px solid red !important");
        document.getElementById('nickname_info').innerHTML = "Login nie może być pusty!";
        $('#nickname_info').show();
        isDataCorrect = false;
    }

    if (nickname.length >= 3) {
        $('#nickname_info').hide();
        $('#nickname').attr('style', "border-bottom: 1px solid gray !important");
    } else {
        loading("register", true);
        $('#nickname').attr('style', "border-bottom: 1px solid red !important");
        document.getElementById('nickname_info').innerHTML = "Minimalna długość to 3 znaki!";
        $('#nickname_info').show();
        isDataCorrect = false;
    }

    if (password != "") {
        $('#password_info').hide();
        $('#password').attr('style', "border-bottom: 1px solid gray !important");
    } else {
        loading("register", true);
        $('#password').attr('style', "border-bottom: 1px solid red !important");
        document.getElementById('password_info').innerHTML = "Hasło nie może być puste!";
        $('#password_info').show();
        isDataCorrect = false;
    }

    if (password_repeat != "") {
        $('#password_repeat_info').hide();
        $('#password_repeat').attr('style', "border-bottom: 1px solid gray !important");
    } else {
        loading("register", true);
        $('#password_repeat').attr('style', "border-bottom: 1px solid red !important");
        document.getElementById('password_repeat_info').innerHTML = "Hasło nie może być puste!";
        $('#password_repeat_info').show();
        isDataCorrect = false;
    }

    if (password != "" && password_repeat != "") {
        if (password == password_repeat) {
            $('#password_info').hide();
            $('#password_repeat_info').hide();
            $('#password').attr('style', "border-bottom: 1px solid gray !important");
            $('#password_repeat').attr('style', "border-bottom: 1px solid gray !important");
        } else {
            loading("register", true);
            $('#password').attr('style', "border-bottom: 1px solid red !important");
            $('#password_repeat').attr('style', "border-bottom: 1px solid red !important");
            document.getElementById('password_info').innerHTML = "Hasła muszą być takie same!";
            document.getElementById('password_repeat_info').innerHTML = "Hasła muszą być takie same!";
            $('#password_info').show();
            $('#password_repeat_info').show();
            isDataCorrect = false;
        }
    }

    if (password.length >= 3) {
        $('#password_info').hide();
        $('#password').attr('style', "border-bottom: 1px solid gray !important");
    } else {
        loading("register", true);
        $('#password').attr('style', "border-bottom: 1px solid red !important");
        document.getElementById('password_info').innerHTML = "Minimalna długość to 3 znaki!";
        $('#password_info').show();
        isDataCorrect = false;
    }

    if (password_repeat.length >= 3) {
        $('#password_repeat_info').hide();
        $('#password_repeat').attr('style', "border-bottom: 1px solid gray !important");
    } else {
        loading("register", true);
        $('#password_repeat').attr('style', "border-bottom: 1px solid red !important");
        document.getElementById('password_repeat_info').innerHTML = "Minimalna długość to 3 znaki!";
        $('#password_repeat_info').show();
        isDataCorrect = false;
    }

    if (isDataCorrect) {
        mp.trigger("callFromHTML::onLoginPanelBrowserAction", "register_click", JSON.stringify([nickname, password]));
    }
}

function answerFromServer(status) {
    parseStatus = JSON.parse(status);
    switch (parseStatus[0].status) {
        case "login_taken": //login zajęty (przy rejestracji)
            loading("register", true);
            $('#nickname').attr('style', "border-bottom: 1px solid red !important");
            document.getElementById('nickname_info').innerHTML = "Ten login jest zajęty!";
            $('#nickname_info').show();
            break;
        case "registration_success": //rejestracja pomyślna, zwraca ID
            setTimeout(function () {
                loading("register", true);
            }, 3000);
            $('#register_info').show();
            document.getElementById('register_info').style = "color: green";
            document.getElementById('register_info').innerHTML = "Zarejestrowano pomyślnie, przejdź teraz do logowania!";
            document.querySelector('.alert-success').innerHTML = "Zarejestrowano pomyślnie! ID Twojego konta: " + parseStatus[0].id + ". Możesz się teraz zalogować.";
            $('.alert-success').fadeIn();
            setTimeout(function () {
                $('.alert-success').fadeOut();
            }, 3000);
            break;
        case "incorrect_login": //przy logowaniu taki login nie istnieje
            loading("login", true);
            $('#nickname_login').attr('style', "border-bottom: 1px solid red !important");
            document.getElementById('nickname_login_info').innerHTML = "Taki login nie istnieje!";
            $('#nickname_login_info').show();
            break;
        case "bad_password": //podano błedne haslo przy logowaniu
            loading("login", true);
            $('#password_login').attr('style', "border-bottom: 1px solid red !important");
            document.getElementById('password_login_info').innerHTML = "Nieprawidłowe hasło!";
            $('#password_login_info').show();
            break;
        case "success": //podano dobry login i hasło, zalogowano
            tl.fromTo(container, 1.5, { top: "12%" }, { top: "150%" })
                .fromTo(container, 1.5, { visibility: "visible" }, { visibility: "hidden" });
            break;
        case "logged_in": //ktoś już jest zalogowany na koncie
            loading("login", true);
            document.querySelector('.alert-danger').innerHTML = "Ktoś już jest aktualnie zalogowany na tym koncie!";
            $('.alert-danger').fadeIn();
            setTimeout(function () {
                $('.alert-danger').fadeOut();
            }, 3000);
            break;
        case "banned": //konto jest zbanowane
            loading("login", true);
            document.querySelector('.alert-danger').innerHTML = "To konto jest zbanowane!";
            $('.alert-danger').fadeIn();
            setTimeout(function () {
                $('.alert-danger').fadeOut();
            }, 3000);
            break;
        default:
            break;
    }
}

function setInitialFields(fields) //remember, login, password
{
    parseFields = JSON.parse(fields);
    document.getElementById('nickname_login').value = parseFields[0].login;
    document.getElementById('password_login').value = parseFields[0].password;
    document.getElementById('remember').checked = true;
}

function setServerActive(value){
    if(value == 0){
        $('#server_running_info').show();
        document.getElementById("login_button").disabled = true;
    }else if(value == 1){
        $('#server_running_info').hide();
        document.getElementById("login_button").disabled = false;
    }
}