const tl = new TimelineMax();
const spawns = document.getElementsByClassName('spawn');
const details = document.getElementsByClassName('spawn_details');
var spawnClicked = false;

function animateSpawns() {
    var even = false;
    var first = true;
    for (var spawn of spawns) {
        if (even) { //po prawej
            tl.fromTo(spawn, 0.8, { right: "-100%" }, { right: "0" }, "-=0.5");
            even = false;
        } else { //po lewej
            if (first) {
                tl.fromTo(spawn, 0.2, { left: "-100%" }, { left: "0" });
                first = false;
            } else {
                tl.fromTo(spawn, 0.8, { left: "-100%" }, { left: "0" }, "-=0.5");
            }
            even = true;
        }
    }
}

function onSpawnClick(id) {
    if (!spawnClicked) {
        spawnClicked = true;
        mp.trigger("callFromHTML::onSpawnSelectAction", "spawn_click", JSON.stringify([id[0].id]));
    }
}

function onSpawnHover(id) {
    mp.trigger("callFromHTML::onSpawnSelectAction", "mouse_hover", JSON.stringify([id[0].id]));
    for (var spawn of spawns) {
        if (spawn.id != id[0].id) {
            spawn.style.opacity = 0.15;
        }
    }
    for (var det of details) {
        if (det.id == id[0].id) {
            det.style.opacity = 1;
        }
    }
}

function onSpawnOut() {
    for (var spawn of spawns) {
        spawn.style.opacity = 1;
    }
    for (var det of details) {
        det.style.opacity = 0;
    }
}

function getSpawnList(spawnList) {
    var list = JSON.parse(spawnList);
    var html = "";
    var top_left = 10;
    var top_right = 20;
    var spawn_height = 0;
    var spawn_name_bottom = 0;
    for (var i = 0; i < list.length; i++) {
        console.log("details: " + list[i].details);
        switch (list.length) {
            case 4:
                spawn_height = 30;
                spawn_name_bottom = 10;
                if (i % 2 != 0) {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-right" style="top: ' + top_right + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_right += 40;
                } else {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-left" style="top: ' + top_left + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_left += 40;
                }
                break;
            case 5:
                spawn_height = 23;
                spawn_name_bottom = 12;
                if (i % 2 != 0) {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-right" style="top: ' + top_right + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_right += 30;
                } else {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-left" style="top: ' + top_left + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_left += 30;
                }
                break;
            case 6:
                spawn_height = 21;
                spawn_name_bottom = 14;
                if (i % 2 != 0) {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-right" style="top: ' + top_right + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_right += 26;
                } else {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-left" style="top: ' + top_left + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_left += 26;
                }
                break;
            case 7:
                spawn_height = 19;
                spawn_name_bottom = 16;
                if (i % 2 != 0) {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-right" style="top: ' + top_right + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_right += 22;
                } else {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-left" style="top: ' + top_left + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_left += 22;
                }
                break;
            case 8:
                spawn_height = 16;
                spawn_name_bottom = 18;
                if (i % 2 != 0) {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-right" style="top: ' + top_right + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_right += 19;
                } else {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-left" style="top: ' + top_left + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_left += 19;
                }
                break;
            case 9:
                spawn_height = 15;
                spawn_name_bottom = 19;
                if (i % 2 != 0) {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-right" style="top: ' + top_right + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_right += 18;
                } else {
                    html += '<div id="' + list[i].id + '" class="spawn spawn-left" style="top: ' + top_left + '%;height: ' + spawn_height + '%;"><div class="row">';
                    top_left += 18;
                }
                break;
        }
        if (i % 2 != 0) { //parzyste, czyli spawn po prawej stronie
            html += '<div id="' + list[i].id + '" class="col image" onmouseover="onSpawnHover(' + list[i].id + ')" onmouseout="onSpawnOut()" onclick="onSpawnClick(' + list[i].id + ')">';
            html += '<img src="img/' + list[i].img + '" class="spawn_image"></div>';
            html += '<div class="col name"><div class="row"><p id="' + list[i].id + '" class="spawn_details">' + list[i].details + '</p></div>';
            html += '<div class="row"><h5 class="spawn_name" style="bottom: ' + spawn_name_bottom + '%;">' + list[i].text + '</h5></div></div></div></div>';
        } else { //nieparzyste, czyli spawn po lewej stronie
            html += '<div class="col name"><div class="row"><p id="' + list[i].id + '" class="spawn_details">' + list[i].details + '</p></div>';
            html += '<div class="row"><h5 class="spawn_name" style="bottom: ' + spawn_name_bottom + '%;">' + list[i].text + '</h5></div></div>';
            html += '<div id="' + list[i].id + '" class="col image" onmouseover="onSpawnHover(' + list[i].id + ')" onmouseout="onSpawnOut()" onclick="onSpawnClick(' + list[i].id + ')">';
            html += '<img src="img/' + list[i].img + '" class="spawn_image"></div></div></div>';
        }
    }
    $('.container').append(html);
    animateSpawns();
}