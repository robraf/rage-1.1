const container = document.querySelector('.container');
const equipment = document.querySelector('.equipment');
const crafting = document.querySelector('.crafting');
const tl = new TimelineMax();

openEQ();
// hideEQ();

setTimeout(function() {
  changeInvCraft("crafting");
}, 3000);

setTimeout(function() {
  changeInvCraft("inventory");
}, 6000);

function openEQ() {
  tl.fromTo(container, 0, { visibility: "hidden" }, { visibility: "visible" })
    .fromTo(container, 0.4, { width: "0%" }, { width: "70%" })
    .fromTo(container, 0.6, { height: "20%" }, { height: "70%" });
}

function hideEQ() {
  tl.fromTo(container, 0.3, { opacity: "1" }, { opacity: "0" })
    .fromTo(container, 0, { visibility: "visible" }, { visibility: "hidden" });
}

function changeInvCraft(window) {
  if (window == "inventory") {
    tl.fromTo(equipment, 0.1, { visibility: "hidden" }, { visibility: "visible" })
      .fromTo(crafting, 0.6, { width: "100%" }, { width: "0%" })
      .fromTo(equipment, 0.1, { opacity: "0" }, { opacity: "1" }, "-=0.6")
      .fromTo(equipment, 0.6, { width: "10%" }, { width: "100%" }, "-=0.6")
      .fromTo(crafting, 0, { visibility: "visible" }, { visibility: "hidden" });
  } else if (window == "crafting") {
    tl.fromTo(crafting, 0.1, { visibility: "hidden" }, { visibility: "visible" })
      .fromTo(equipment, 0.5, { width: "100%" }, { width: "10%" })
      .fromTo(crafting, 0.5, { width: "0%" }, { width: "100%" }, "-=0.5")
      .fromTo(equipment, 0.1, { opacity: "1" }, { opacity: "0" }, "-=0.1")
      .fromTo(equipment, 0, { visibility: "visible" }, { visibility: "hidden" });
  }  
}
