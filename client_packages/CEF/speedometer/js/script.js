var poppins = new FontFace('Poppins', 'url(../assets/fonts/Poppins-Regular.ttf)');
poppins.load().then(function (loaded_face) {
  document.fonts.add(loaded_face);
  document.body.style.fontFamily = '"Poppins", Arial';
}).catch(function (error) {
  // error occurred
});

var speedGauge = new RadialGauge({
  //Basic
  renderTo: 'speedometer',
  width: 300,
  height: 300,

  //Speed value
  valueBox: true,
  minValue: 0,
  maxValue: 300,
  value: 0,
  valueDec: 0,
  valueBoxWidth: 25,
  valueBoxBorderRadius: 4,

  //Text - mileage etc
  title: '',
  units: '',

  //Ticks
  startAngle: 50,
  ticksAngle: 260,
  majorTicks: [0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300],
  minorTicks: 2,
  strokeTicks: true,

  //Needle
  needle: true,
  needleType: 'arrow',
  needleWidth: 3,
  needleCircleSize: 7,
  needleCircleOuter: true,
  needleCircleInner: true,

  //Animations
  animatedValue: true,
  animateOnInit: false,
  animationDuration: 300,
  animationRule: 'linear',

  //Fonts
  fontValue: 'Poppins',
  fontNumbers: 'Poppins',
  fontTitle: 'Poppins',
  fontUnits: 'Poppins',

  //Colors
  colorPlate: '#222',
  colorPlateEnd: '#111',
  colorMajorTicks: 'lightgray',
  colorMinorTicks: 'lightgray',
  colorTitle: 'gray',
  colorUnits: 'gray',
  colorNumbers: 'lightgray',
  colorNeedle: 'darkcyan',
  colorNeedleEnd: 'darkcyan',
  colorNeedleShadowDown: '#333',
  borders: false,
  borderShadowWidth: 3,
  highlights: [{ from: 280, to: 300, color: 'rgba(200, 50, 50, .75)' }]
}).draw();

function setGaugeValues(speed, mileage) {
  var maxspeed = speedGauge.options.maxValue;
  if (speed > maxspeed) {
    speedGauge.update({ value: maxspeed });
  } else {
    speedGauge.update({ value: speed });
  }
  speedGauge.update({ units: mileage });
}

function adjustSpeedGauge(maxspeed) {
  var ticks = [];
  for (var tick = 0; tick <= maxspeed; tick += 20) {
    ticks.push(tick);
  }
  speedGauge.update({ maxValue: maxspeed });
  speedGauge.update({ majorTicks: ticks });
  speedGauge.update({ highlights: [{ from: maxspeed - 20, to: maxspeed, color: 'rgba(200, 50, 50, .75)' }] });
}

function changeIcon(icon, value) {
  switch (icon) {
    case "body":
      if (value == 0) {
        $('.carbody').css("fill", "rgba(0, 0, 0, 0.8)");
      } else if (value == 1) {
        $('.carbody').css("fill", "orange");
      } else if (value == 2) {
        $('.carbody').css("fill", "rgb(200, 50, 50)");
      }
      break;
    case "engine":
      if (value == 0) {
        $('.engine').css("fill", "rgba(0, 0, 0, 0.8)");
      } else if (value == 1) {
        $('.engine').css("fill", "orange");
      } else if (value == 2) {
        $('.engine').css("fill", "rgb(200, 50, 50)");
      }
      break;
    case "headlights":
      if (value == 0) {
        $('.headlights').css("fill", "#111");
      } else if (value == 1) {
        $('.headlights').css("fill", "darkgreen");
      }
      break;
    case "handbrake":
      if (value == 0) {
        $('.handbrake').css("fill", "#111");
      } else if (value == 1) {
        $('.handbrake').css("fill", "rgb(200, 50, 50)");
      }
      break;
    case "seatbelts":
      if (value == 0) {
        $('.seatbelt').css("fill", "rgb(200, 50, 50)");
      } else if (value == 1) {
        $('.seatbelt').css("fill", "#111");
      }
      break;
    case "lock":
      if (value == 0) {
        $('.doorlock').css("fill", "rgb(200, 50, 50)");
      } else if (value == 1) {
        $('.doorlock').css("fill", "#111");
      }
      break;
    default:
      break;
  }
}