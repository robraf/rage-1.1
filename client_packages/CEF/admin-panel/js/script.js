$('.container').hide();
$('.alert-success').hide();
$('.alert-danger').hide();
$('.time-info').hide();

function showAdminPanel() {
  $('.container').show();
}

$('#playermodal').on('show.bs.modal', function (event) {
  var id = event.relatedTarget.id;
  mp.trigger("callFromHTML::onAdminPanelAction", "get_user_by_id", JSON.stringify([id]));

  //var button = $(event.relatedTarget);
  //var nickname = button.data('nickname');

  //var modal = $(this);
  //modal.find('.modal-title').text(nickname + '[' + id + ']');
});

function updatePlayerModal(json) {
  //nick
  //czy gracz jest online
  //historia kar
  json = JSON.parse(json);
  var modal = $('#playermodal');
  modal.find('.modal-title').text('serial: ' + json.serial);
}

var customSelectState = false;
$(document).ready(
  hideSelect()
);

// window.onclick = function (event) {
//   setTimeout(function () {
//     if (customSelectState) {
//       if (event.target != document.getElementById("customSelect")) {
//         hideSelect();
//         console.log("kil poza lista");
//       } else {
//         console.log("kil w liscie");
//       }
//     }
//   }, 50);
// }

var mouseOverPlayerListStatus = false;
function mouseOverPlayerList(status) {
  if (status) {
    mouseOverPlayerListStatus = true;
    console.log("true");
  } else {
    mouseOverPlayerListStatus = false;
    console.log("false");
  }
}

// var test = [{ "nick": "rafista", "id": "1" }, { "nick": "rezus", "id": "2" }, { "nick": "bambik", "id": "123" }];
// updatePlayerList(JSON.stringify(test));
var firstScripLoad = true;
function updatePlayerList(json) {
  if (!mouseOverPlayerListStatus) {
    if (!firstScripLoad) {
      var playerUl = document.getElementById("playerUl");
      playerUl.remove();
    }
    var json = JSON.parse(json);
    var html = '';
    for (var player of json) {
      html += '<li><button id="' + player.id + '" type="button" class="btn btn-playerlist"';
      html += 'data-toggle="modal" data-target="#playermodal" data-nickname="' + player.nick + '">';
      html += player.nick + '</button></li>';
    }
    $('.playerlist').append('<ul id="playerUl">' + html + '</ul>');
    html = '';
    firstScripLoad = false;
  }
}


function hideSelect() {
  $('.customSelectItems').hide();
  $('#customSelectHeader').text("Wybierz pogodę...");
}

function customSelect() {
  if (!customSelectState) {
    customSelectState = true;
    $('.customSelectItems').show();
    $('#customSelect').css("box-shadow", "0 0 5px 1px darkcyan");
  } else {
    customSelectState = false;
    $('.customSelectItems').hide();
    $('#customSelect').css("box-shadow", "none");
  }
}

function selectOption(id) {
  if (customSelectState) {
    var customSelectItems = document.getElementsByClassName("customSelectItem");
    var selectedItem = null;
    for (var item of customSelectItems) {
      if (id == item.id) {
        selectedItem = item;
      }
    }
    var newHeader = selectedItem.id;
    $('#customSelectHeader').html(newHeader);
    $('.customSelectItems').hide();
  }
}

function setTime() {
  var time = document.getElementById("time-input");
  var timeSplit = time.value.split(":");
  if (/^([0-9]{2}:[0-9]{2})$/.test(time.value) && parseInt(timeSplit[0]) < 24 && parseInt(timeSplit[0]) >= 0 && parseInt(timeSplit[1]) < 60 && parseInt(timeSplit[1]) >= 0) {
    console.log("Ustaw czas na " + time.value);
    document.querySelector('.alert-success').innerHTML = "Poprawnie zmieniono godzinę na " + time.value;
    $('.alert-success').fadeIn();
    setTimeout(function () {
      $('.alert-success').fadeOut();
    }, 3000);
    document.getElementById("time-input").value = "";
    document.getElementById("time-input").style = "border: 1px solid lightgray";
  } else {
    document.querySelector('.alert-danger').innerHTML = "Podaj czas w formacie HH:MM!";
    $('.alert-danger').fadeIn();
    setTimeout(function () {
      $('.alert-danger').fadeOut();
    }, 3000);
    document.getElementById("time-input").style = "border: 1px solid red";
  }
}

function setWeather() {
  var weather = document.getElementById("customSelectHeader").innerHTML;
  if (weather != "Wybierz pogodę...") {
    console.log("Ustaw pogodę na " + weather);
    document.querySelector('.alert-success').innerHTML = "Poprawnie zmieniono pogodę na " + weather;
    $('.alert-success').fadeIn();
    setTimeout(function () {
      $('.alert-success').fadeOut();
    }, 3000);
    $('#customSelectHeader').text("Wybierz pogodę...");
    document.getElementById("customSelect").style = "border: 1px solid lightgray";
  } else {
    document.querySelector('.alert-danger').innerHTML = "Najpierw wybierz pogodę!";
    $('.alert-danger').fadeIn();
    setTimeout(function () {
      $('.alert-danger').fadeOut();
    }, 3000);
    document.getElementById("customSelect").style = "border: 1px solid red";
  }
}

function savePlayerPositions() {
  console.log("Zapisz pozycje");
}

function teleportToPlayer() {
  console.log("Teleportuj do gracza");
}

function teleportPlayer() {
  console.log("Teleportuj gracza do siebie");
}

function setPenalty() {
  var penalty_type;
  var reason = document.getElementById("penalty_reason").value;
  var time = document.getElementById("penalty_time").value;
  if (/^\d+[a-zA-Z]$/.test(time)) {
    $('.time-info').hide();
    switch (time.slice(-1).toLowerCase()) {
      case "m":
        console.log("Czas: " + time.slice(0, -1) + " minut");
        break;
      case "h":
        console.log("Czas: " + time.slice(0, -1) + " godzin");
        break;
      case "d":
        console.log("Czas: " + time.slice(0, -1) + " dni");
        break;
      case "t":
        console.log("Czas: " + time.slice(0, -1) + " tygodni");
        break;
      default:
        break;
    }
  } else {
    $('.time-info').show();
  }

  var radios = document.getElementsByName("penalty_type");
  for (var radio of radios) {
    if (radio.checked) {
      penalty_type = radio.value;
      break;
    }
  }
}

function removePenalty() {
  console.log("Usuń karę");
}