//INITIAL FUNCTIONS
$(".custom-slider").ionRangeSlider({
  skin: "round",
  type: "single"
});

$("#roundSlider").roundSlider({
  circleShape: "full",
  svgMode: true,
  startAngle: 180,
  endAngle: 360,
  min: 180,
  max: -180,
  startValue: 0,
  radius: 130,
  handleSize: 25,
  width: 15,
  editableTooltip: false
});

const tl = new TimelineMax();
const container = document.querySelector('.container');
const control_buttons = document.querySelector('.control-buttons');
const sex_chosen = document.querySelector('.sex-chosen');

$(document).ready(
  tl.fromTo(container, 0.6, { left: "-40%" }, { left: "0" })
    .fromTo(control_buttons, 0.6, { bottom: "-60%" }, { bottom: "5%" }, "-=0.6")
);

//DATA ARRAYS AND VARIABLES
var isFirstLoad = true;
var previousSex = "m"

var appearanceObject = {
  created: 0,
  sex: "m",
  eye: 0,
  face: "0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0",
  hair: 0,
  hairColor1: 0,
  hairColor2: 0,
  mother: 21,
  father: 0,
  shape: 0.5,
  skin: 0.5,
  blemishes: 255,
  facialhair: 255,
  facialhairColor: 0,
  eyebrows: 255,
  eyebrowsColor: 0,
  ageing: 255,
  makeup: 255,
  makeupColor1: 0,
  makeupColor2: 0,
  blush: 255,
  blushColor: 0,
  complexion: 255,
  sundamage: 255,
  lipstick: 255,
  lipstickColor: 0,
  moles: 255
}

var defaultObject = {
  created: 0,
  sex: "m",
  eye: 0,
  face: "0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0",
  hair: 0,
  hairColor1: 0,
  hairColor2: 0,
  mother: 21,
  father: 0,
  shape: 0.5,
  skin: 0.5,
  blemishes: 255,
  facialhair: 255,
  facialhairColor: 0,
  eyebrows: 255,
  eyebrowsColor: 0,
  ageing: 255,
  makeup: 255,
  makeupColor1: 0,
  makeupColor2: 0,
  blush: 255,
  blushColor: 0,
  complexion: 255,
  sundamage: 255,
  lipstick: 255,
  lipstickColor: 0,
  moles: 255
}

var mothersId = [21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 45];
var mothersNames = ["Justyna", "Iwona", "Agata", "Wiktoria", "Weronika",
  "Agnieszka", "Aleksandra", "Julia", "Martyna", "Alicja",
  "Amelia", "Oliwia", "Marta", "Magdalena", "Daria",
  "Karolina", "Anna", "Anastazja", "Gabriela", "Halina",
  "Danuta", "Natalia"];
var fathersId = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 42, 43, 44];
var fathersNames = ["Piotr", "Paweł", "Rafał", "Robert", "Dawid",
  "Maksymilian", "Radosław", "Jan", "Kamil", "Michał",
  "Mateusz", "Filip", "Marek", "Oskar", "Szymon",
  "Adam", "Jacek", "Marcin", "Stefan", "Łukasz",
  "Maciej", "Stanisław", "Dominik", "Jakub"];
var eyeColors = ["Zielony", "Szmaragdowy", "Jasnoniebieski", "Błękit oceanu", "Jasnobrązowy",
  "Ciemnobrązowy", "Piwny", "Ciemnoszary", "Jasnoszary", "Różowy", "Żółty", "Fioletowe",
  "Czarne", "Szare", "Tequila Sunrise", "Atomic", "Warp", "ECola", "Space Ranger",
  "Ying Yang", "Bullseye", "Lizard", "Dragon", "Extra Terrestrial", "Goat", "Smiley",
  "Possessed", "Demon", "Infected", "Alien", "Undead", "Zombie"];
var eyebrowTypes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34];
var womanHair = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
  39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78];
var manHair = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
  39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74];
var beardTypes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
var makeupTypes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
var blushesTypes = [0, 1, 2, 3];
var frecklesTypes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
var agingTypes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
var complexionTypes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
var discolorationTypes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
var burnTypes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

var currentFace = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0];
var currentMother = 0;
var currentFather = 0;
var currentEye = 0;
var currentEyebrowType = 0;
var currentEyebrowColor = 0;
var currentHair = 0;
var currentHairColor1 = 0;
var currentHairColor2 = 0;
var currentBeardType = 0;
var currentBeardColor = 0;
var currentLipstickColor = 0;
var currentMakeupType = 0;
var currentMakeupColor1 = 0;
var currentMakeupColor2 = 0;
var currentBlushesType = 0;
var currentBlushesColor = 0;
var currentFreckles = 0;
var currentAging = 0;
var currentComplexion = 0;
var currentDiscoloration = 0;
var currentBurn = 0;

//ONLOAD FUNCTIONS
window.onload = function () {
  //resetCharacter();
  isFirstLoad = false;
};

//HANDLING CREATOR ELEMENTS
function changeSex(sex) {
  if (sex == "woman" && appearanceObject.sex == "m") {
    appearanceObject.sex = "f";
    tl.fromTo(sex_chosen, 0.5, { left: "100px" }, { left: "2px" })
      .fromTo(sex_chosen, 0.4, { borderRadius: "0 8px 8px 0" }, { borderRadius: "8px 0 0 8px" }, "-=0.5");
    previousSex = "f";
  } else if (sex == "man" && appearanceObject.sex == "f") {
    appearanceObject.sex = "m";
    tl.fromTo(sex_chosen, 0.5, { left: "2px" }, { left: "100px" })
      .fromTo(sex_chosen, 0.4, { borderRadius: "8px 0 0 8px" }, { borderRadius: "0 8px 8px 0" }, "-=0.5");
    previousSex = "m";
  }
  currentHair = 0;
  appearanceObject.hair = 0;
  document.querySelector('.hair-id').innerHTML = 0;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "sex", value: appearanceObject.sex }));
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "hair", value: appearanceObject.hair }));
}

function changeParent(element, value) {
  if (element == "mother") {
    if (value == "prev") {
      if (--currentMother < 0) {
        currentMother = mothersNames.length - 1;
      }
    } else if (value == "next") {
      if (++currentMother > mothersNames.length - 1) {
        currentMother = 0;
      }
    }
    document.querySelector('.mother-name').innerHTML = mothersNames[currentMother];
    appearanceObject.mother = mothersId[currentMother];
    mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "mother", value: appearanceObject.mother }));
  } else if (element == "father") {
    if (value == "prev") {
      if (--currentFather < 0) {
        currentFather = fathersNames.length - 1;
      }
    } else if (value == "next") {
      if (++currentFather > fathersNames.length - 1) {
        currentFather = 0;
      }
    }
    document.querySelector('.father-name').innerHTML = fathersNames[currentFather];
    appearanceObject.father = fathersId[currentFather];
    mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "father", value: appearanceObject.father }));
  }
}

function changeSimilarity(element, value) {
  if (element == "skin") {
    appearanceObject.skin = value;
    mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "skin", value: appearanceObject.skin }));
  } else if (element == "shape") {
    appearanceObject.shape = value;
    mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "shape", value: appearanceObject.shape }));
  }

}

function changeEyes(element, value) {
  switch (element) {
    case "eye":
      //kolor oczu
      if (value == "prev") {
        if (--currentEye < 0) {
          currentEye = eyeColors.length - 1;
        }
      } else if (value == "next") {
        if (++currentEye > eyeColors.length - 1) {
          currentEye = 0;
        }
      }
      document.querySelector('.eye-id').innerHTML = eyeColors[currentEye];
      appearanceObject.eye = currentEye;
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "eye", value: appearanceObject.eye }));
      break;
    case "eye-open":
      //otwartość oczu
      currentFace[11] = value;
      updateFace();
      break;
    case "type":
      //typ brwi
      if (value == "prev") {
        if (--currentEyebrowType < 0) {
          currentEyebrowType = eyebrowTypes.length - 1;
        }
      } else if (value == "next") {
        if (++currentEyebrowType > eyebrowTypes.length - 1) {
          currentEyebrowType = 0;
        }
      }
      document.querySelector('.eyebrow-type').innerHTML = eyebrowTypes[currentEyebrowType];
      if (currentEyebrowType == 0) {
        appearanceObject.eyebrows = 255;
      } else {
        appearanceObject.eyebrows = currentEyebrowType - 1;
        mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "eyebrows", value: appearanceObject.eyebrows }));
      }
      break;
    case "color":
      //kolor brwi
      if (value == "prev") {
        if (--currentEyebrowColor < 0) {
          currentEyebrowColor = 63;
        }
      } else if (value == "next") {
        if (++currentEyebrowColor > 63) {
          currentEyebrowColor = 0;
        }
      }
      document.querySelector('.eyebrow-color').innerHTML = currentEyebrowColor;
      appearanceObject.eyebrowsColor = currentEyebrowColor;
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "eyebrowsColor", value: appearanceObject.eyebrowsColor }));
      break;
    case "width":
      //szerokość brwi
      currentFace[7] = value;
      updateFace();
      break;
    case "height":
      //wysokość brwi
      currentFace[6] = value;
      updateFace();
      break;
    default:
      break;
  }
}

function changeNose(element, value) {
  switch (element) {
    case "width":
      //szerokośc nosa
      currentFace[0] = value;
      updateFace();
      break;
    case "length":
      //długość nosa
      currentFace[2] = value;
      updateFace();
      break;
    case "height":
      //wysokość nosa
      currentFace[1] = value;
      updateFace();
      break;
    case "end":
      //końcówka nosa
      currentFace[4] = value;
      updateFace();
      break;
    case "bridge":
      //mostek nosa
      currentFace[3] = value;
      updateFace();
      break;
    case "move":
      //przesunięcie mostka nosa
      currentFace[5] = value;
      updateFace();
      break;
    default:
      break;
  }
}

function changeCheeks(element, value) {
  switch (element) {
    case "cheek-width":
      //szerokość policzków
      currentFace[10] = value;
      updateFace();
      break;
    case "height":
      //wysokość kości policzkowych
      currentFace[8] = value;
      updateFace();
      break;
    case "width":
      //szerokość kości policzkowych
      currentFace[9] = value;
      updateFace();
      break;
    case "jaw-height":
      //wysokość szczęki
      currentFace[14] = value;
      updateFace();
      break;
    case "jaw-width":
      //szerokość szczęki
      currentFace[13] = value;
      updateFace();
      break;
    default:
      break;
  }
}

function changeChin(element, value) {
  switch (element) {
    case "length":
      //długość podbródka
      currentFace[15] = value;
      updateFace();
      break;
    case "width":
      //szerokość podbródka
      currentFace[17] = value;
      updateFace();
      break;
    case "position":
      //pozycja podbródka
      currentFace[16] = value;
      updateFace();
      break;
    case "shape":
      //kształt podbródka
      currentFace[18] = value;
      updateFace();
      break;
    case "neck":
      //szerokość szyji
      currentFace[19] = value;
      updateFace();
      break;
    default:
      break;
  }
}

function changeHair(element, value) {
  switch (element) {
    case "hair":
      //rodzaj włosów
      if (value == "prev") {
        if (--currentHair < 0) {
          if (appearanceObject.sex == "f") {
            currentHair = womanHair.length - 1;
          } else {
            currentHair = manHair.length - 1;
          }
        }
      } else if (value == "next") {
        if (appearanceObject.sex == "f") {
          if (++currentHair > womanHair.length - 1) {
            currentHair = 0;
          }
        } else {
          if (++currentHair > manHair.length - 1) {
            currentHair = 0;
          }
        }
      }
      if (appearanceObject.sex == "f") {
        document.querySelector('.hair-id').innerHTML = currentHair;
        appearanceObject.hair = womanHair[currentHair];
        mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "hair", value: appearanceObject.hair }));
      } else {
        document.querySelector('.hair-id').innerHTML = currentHair;
        appearanceObject.hair = manHair[currentHair];
        mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "hair", value: appearanceObject.hair }));
      }
      break;
    case "hair-color1":
      //1 kolor włosów
      if (value == "prev") {
        if (--currentHairColor1 < 0) {
          currentHairColor1 = 63;
        }
      } else if (value == "next") {
        if (++currentHairColor1 > 63) {
          currentHairColor1 = 0;
        }
      }
      document.querySelector('.hair-color1').innerHTML = currentHairColor1;
      appearanceObject.hairColor1 = currentHairColor1;
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "hairColor1", value: appearanceObject.hairColor1 }));
      break;
    case "hair-color2":
      //2 kolor włosów
      if (value == "prev") {
        if (--currentHairColor2 < 0) {
          currentHairColor2 = 63;
        }
      } else if (value == "next") {
        if (++currentHairColor2 > 63) {
          currentHairColor2 = 0;
        }
      }
      document.querySelector('.hair-color2').innerHTML = currentHairColor2;
      appearanceObject.hairColor2 = currentHairColor2;
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "hairColor2", value: appearanceObject.hairColor2 }));
      break;
    case "beard-type":
      //rodzaj zarostu
      if (value == "prev") {
        if (--currentBeardType < 0) {
          currentBeardType = beardTypes.length - 1;
        }
      } else if (value == "next") {
        if (++currentBeardType > beardTypes.length - 1) {
          currentBeardType = 0;
        }
      }
      document.querySelector('.beard-type').innerHTML = beardTypes[currentBeardType];
      if (currentBeardType == 0) {
        appearanceObject.facialhair = 255;
      } else {
        appearanceObject.facialhair = currentBeardType - 1;

      }
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "facialhair", value: appearanceObject.facialhair }));
      break;
    case "beard-color":
      //kolor zarostu
      if (value == "prev") {
        if (--currentBeardColor < 0) {
          currentBeardColor = 63;
        }
      } else if (value == "next") {
        if (++currentBeardColor > 63) {
          currentBeardColor = 0;
        }
      }
      document.querySelector('.beard-color').innerHTML = currentBeardColor;
      appearanceObject.facialhairColor = currentBeardColor;
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "facialhairColor", value: appearanceObject.facialhairColor }));
      break;
    default:
      break;
  }
}

function changeMouth(element, value) {
  switch (element) {
    case "mouth":
      //szerokość ust
      currentFace[12] = value;
      updateFace();
      break;
    case "lipstick-type":
      //czy jest szminka
      if (document.querySelector('.lipstick-type').innerHTML == "Nie") {
        document.querySelector('.lipstick-type').innerHTML = "Tak";
        appearanceObject.lipstick = 2;
      } else {
        document.querySelector('.lipstick-type').innerHTML = "Nie";
        appearanceObject.lipstick = 255;
      }
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "lipstick", value: appearanceObject.lipstick }));
      break;
    case "lipstick-color":
      //kolor szminki
      if (value == "prev") {
        if (--currentLipstickColor < 0) {
          currentLipstickColor = 63;
        }
      } else if (value == "next") {
        if (++currentLipstickColor > 63) {
          currentLipstickColor = 0;
        }
      }
      document.querySelector('.lipstick-color').innerHTML = currentLipstickColor;
      appearanceObject.lipstickColor = currentLipstickColor;
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "lipstickColor", value: appearanceObject.lipstickColor }));
      break;
    case "makeup-type":
      //rodzaj makeupu
      if (value == "prev") {
        if (--currentMakeupType < 0) {
          currentMakeupType = makeupTypes.length - 1;
        }
      } else if (value == "next") {
        if (++currentMakeupType > makeupTypes.length - 1) {
          currentMakeupType = 0;
        }
      }
      document.querySelector('.makeup-type').innerHTML = makeupTypes[currentMakeupType];
      if (currentMakeupType == 0) {
        appearanceObject.makeup = 255;
      } else {
        appearanceObject.makeup = currentMakeupType - 1;
      }
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "makeup", value: appearanceObject.makeup }));
      break;
    case "makeup-color1":
      //kolor 1 makeupu
      if (value == "prev") {
        if (--currentMakeupColor1 < 0) {
          currentMakeupColor1 = 63;
        }
      } else if (value == "next") {
        if (++currentMakeupColor1 > 63) {
          currentMakeupColor1 = 0;
        }
      }
      document.querySelector('.makeup-color1').innerHTML = currentMakeupColor1;
      appearanceObject.makeupColor1 = currentMakeupColor1;
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "makeupColor1", value: appearanceObject.makeupColor1 }));
      break;
    case "makeup-color2":
      //kolor 2 makeupu
      if (value == "prev") {
        if (--currentMakeupColor2 < 0) {
          currentMakeupColor2 = 63;
        }
      } else if (value == "next") {
        if (++currentMakeupColor2 > 63) {
          currentMakeupColor2 = 0;
        }
      }
      document.querySelector('.makeup-color2').innerHTML = currentMakeupColor2;
      appearanceObject.makeupColor2 = currentMakeupColor2;
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "makeupColor2", value: appearanceObject.makeupColor2 }));
      break;
    default:
      break;
  }
}

function changeBlushes(element, value) {
  switch (element) {
    case "type":
      //rodzaj rumieńców
      if (value == "prev") {
        if (--currentBlushesType < 0) {
          currentBlushesType = blushesTypes.length - 1;
        }
      } else if (value == "next") {
        if (++currentBlushesType > blushesTypes.length - 1) {
          currentBlushesType = 0;
        }
      }
      document.querySelector('.blushes-type').innerHTML = blushesTypes[currentBlushesType];
      if (currentBlushesType == 0) {
        appearanceObject.blush = 255;
      } else {
        appearanceObject.blush = currentBlushesType - 1;
      }
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "blush", value: appearanceObject.blush }));
      break;
    case "color":
      //kolor rumieńców
      if (value == "prev") {
        if (--currentBlushesColor < 0) {
          currentBlushesColor = 63;
        }
      } else if (value == "next") {
        if (++currentBlushesColor > 63) {
          currentBlushesColor = 0;
        }
      }
      document.querySelector('.blushes-color').innerHTML = currentBlushesColor;
      appearanceObject.blushColor = currentBlushesColor;
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "blushColor", value: appearanceObject.blushColor }));
      break;
    case "freckles":
      //piegi
      if (value == "prev") {
        if (--currentFreckles < 0) {
          currentFreckles = frecklesTypes.length - 1;
        }
      } else if (value == "next") {
        if (++currentFreckles > frecklesTypes.length - 1) {
          currentFreckles = 0;
        }
      }
      document.querySelector('.freckles-id').innerHTML = frecklesTypes[currentFreckles];
      if (currentFreckles == 0) {
        appearanceObject.moles = 255;
      } else {
        appearanceObject.moles = currentFreckles - 1;
      }
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "moles", value: appearanceObject.moles }));
      break;
    default:
      break;
  }
}

function changeAgingSkin(element, value) {
  switch (element) {
    case "aging":
      //postarzenie postaci
      if (value == "prev") {
        if (--currentAging < 0) {
          currentAging = agingTypes.length - 1;
        }
      } else if (value == "next") {
        if (++currentAging > agingTypes.length - 1) {
          currentAging = 0;
        }
      }
      document.querySelector('.aging-id').innerHTML = agingTypes[currentAging];
      if (currentAging == 0) {
        appearanceObject.ageing = 255;
      } else {
        appearanceObject.ageing = currentAging - 1;
      }
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "ageing", value: appearanceObject.ageing }));
      break;
    case "complexion":
      //cera postaci
      if (value == "prev") {
        if (--currentComplexion < 0) {
          currentComplexion = complexionTypes.length - 1;
        }
      } else if (value == "next") {
        if (++currentComplexion > complexionTypes.length - 1) {
          currentComplexion = 0;
        }
      }
      document.querySelector('.complexion-id').innerHTML = complexionTypes[currentComplexion];
      if (currentComplexion == 0) {
        appearanceObject.complexion = 255;
      } else {
        appearanceObject.complexion = currentComplexion - 1;
      }
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "complexion", value: appearanceObject.complexion }));
      break;
    default:
      break;
  }

}

function changeBlemishes(element, value) {
  switch (element) {
    case "discoloration":
      //przebarwienia,skazy
      if (value == "prev") {
        if (--currentDiscoloration < 0) {
          currentDiscoloration = discolorationTypes.length - 1;
        }
      } else if (value == "next") {
        if (++currentDiscoloration > discolorationTypes.length - 1) {
          currentDiscoloration = 0;
        }
      }
      document.querySelector('.discoloration-id').innerHTML = discolorationTypes[currentDiscoloration];
      if (currentDiscoloration == 0) {
        appearanceObject.blemishes = 255;
      } else {
        appearanceObject.blemishes = currentDiscoloration - 1;
      }
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "blemishes", value: appearanceObject.blemishes }));
      break;
    case "burns":
      //poparzenia
      if (value == "prev") {
        if (--currentBurn < 0) {
          currentBurn = burnTypes.length - 1;
        }
      } else if (value == "next") {
        if (++currentBurn > burnTypes.length - 1) {
          currentBurn = 0;
        }
      }
      document.querySelector('.burns-id').innerHTML = burnTypes[currentBurn];
      if (currentBurn == 0) {
        appearanceObject.sundamage = 255;
      } else {
        appearanceObject.sundamage = currentBurn - 1;
      }
      mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "sundamage", value: appearanceObject.sundamage }));
      break;
    default:
      break;
  }

}

function updateFace() {
  var string = "";
  for (var i = 0; i < currentFace.length; i++) {
    if (i != currentFace.length - 1) {
      string += currentFace[i] + ",";
    } else {
      string += currentFace[i];
    }
  }
  appearanceObject.face = string;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "face", value: appearanceObject.face }));
}

function resetCharacter() {
  if (!isFirstLoad) {
    changeSex("man");
    $('.custom-slider').each(function () {
      var currentSlider = $(this);
      currentSlider.data('ionRangeSlider').update({ from: "0.0" });
    });
    $('.custom-slider.skin-slider').data('ionRangeSlider').update({ from: "0.5" });
    $('.custom-slider.shape-slider').data('ionRangeSlider').update({ from: "0.5" });
    document.querySelector('.mother-name').innerHTML = mothersNames[0];
    document.querySelector('.father-name').innerHTML = fathersNames[0];
    document.querySelector('.eye-id').innerHTML = eyeColors[0];
    document.querySelector('.eyebrow-type').innerHTML = eyebrowTypes[0];
    document.querySelector('.eyebrow-color').innerHTML = 0;
    document.querySelector('.hair-id').innerHTML = 0;
    document.querySelector('.hair-color1').innerHTML = 0;
    document.querySelector('.hair-color2').innerHTML = 0;
    document.querySelector('.beard-type').innerHTML = 0;
    document.querySelector('.beard-color').innerHTML = 0;
    document.querySelector('.lipstick-type').innerHTML = "Nie";
    document.querySelector('.lipstick-color').innerHTML = 0;
    document.querySelector('.makeup-type').innerHTML = 0;
    document.querySelector('.makeup-color1').innerHTML = 0;
    document.querySelector('.makeup-color2').innerHTML = 0;
    document.querySelector('.blushes-type').innerHTML = 0;
    document.querySelector('.blushes-color').innerHTML = 0;
    document.querySelector('.freckles-id').innerHTML = 0;
    document.querySelector('.aging-id').innerHTML = 0;
    document.querySelector('.complexion-id').innerHTML = 0;
    document.querySelector('.discoloration-id').innerHTML = 0;
    document.querySelector('.burns-id').innerHTML = 0;
    currentFace = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0];
    currentMother = 0;
    currentFather = 0;
    currentEye = 0;
    currentEyebrowType = 0;
    currentEyebrowColor = 0;
    currentHair = 0;
    currentHairColor1 = 0;
    currentHairColor2 = 0;
    currentBeardType = 0;
    currentBeardColor = 0;
    currentLipstickColor = 0;
    currentMakeupType = 0;
    currentMakeupColor1 = 0;
    currentMakeupColor2 = 0;
    currentBlushesType = 0;
    currentBlushesColor = 0;
    currentFreckles = 0;
    currentAging = 0;
    currentComplexion = 0;
    currentDiscoloration = 0;
    currentBurn = 0;
    for (appearance in appearanceObject) {
      appearanceObject.appearance = defaultObject.appearance;
    }
    mp.trigger("callFromHTML::playerCreatorBrowser", "reset", JSON.stringify([""]));
  }

}

function saveCharacter() {
  mp.trigger("callFromHTML::playerCreatorBrowser", "save", JSON.stringify([""]));
}

$("#roundSlider").on("update", function (e) {
  mp.trigger("callFromHTML::playerCreatorBrowser", "rotate", JSON.stringify([e.value]));
});

function randomCharacter() {
  var sex = getRandomInt(0, 1);
  if (sex == 0) {
    appearanceObject.sex = "f";
    appearanceObject.hair = womanHair[getRandomInt(0, womanHair.length - 1)];
  } else {
    appearanceObject.sex = "m";
    appearanceObject.hair = manHair[getRandomInt(0, manHair.length - 1)];
  }
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "sex", value: appearanceObject.sex }));
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "hair", value: appearanceObject.hair }));

  appearanceObject.eye = getRandomInt(0, eyeColors.length - 1);
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "eye", value: appearanceObject.eye }));

  var faceString = "";
  for (var i = 0; i < 20; i++) {
    if (i != 19) {
      faceString += getRandomFloat(-1, 1) + ",";
    } else {
      faceString += getRandomFloat(-1, 1)
    }
  }
  appearanceObject.face = faceString;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "face", value: appearanceObject.face }));

  appearanceObject.hairColor1 = getRandomInt(0, 63);
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "hairColor1", value: appearanceObject.hairColor1 }));

  appearanceObject.hairColor2 = getRandomInt(0, 63);
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "hairColor2", value: appearanceObject.hairColor2 }));

  appearanceObject.mother = mothersId[getRandomInt(0, mothersId.length - 1)];
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "mother", value: appearanceObject.mother }));

  appearanceObject.father = fathersId[getRandomInt(0, fathersId.length - 1)];
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "father", value: appearanceObject.father }));

  appearanceObject.shape = getRandomFloat(0, 1);
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "shape", value: appearanceObject.shape }));

  appearanceObject.skin = getRandomFloat(0, 1);
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "skin", value: appearanceObject.skin }));

  var randomBlemishes = discolorationTypes[getRandomInt(0, discolorationTypes.length - 1)];
  appearanceObject.blemishes = randomBlemishes == 0 ? 255 : randomBlemishes - 1;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "blemishes", value: appearanceObject.blemishes }));

  var randomFacialhair = beardTypes[getRandomInt(0, beardTypes.length - 1)];
  appearanceObject.facialhair = randomFacialhair == 0 ? 255 : randomFacialhair - 1;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "facialhair", value: appearanceObject.facialhair }));

  appearanceObject.facialhairColor = getRandomInt(0, 63);
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "facialhairColor", value: appearanceObject.facialhairColor }));

  var randomEyebrows = eyebrowTypes[getRandomInt(0, eyebrowTypes.length - 1)];
  appearanceObject.eyebrows = randomEyebrows == 0 ? 255 : randomEyebrows - 1;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "eyebrows", value: appearanceObject.eyebrows }));

  appearanceObject.eyebrowsColor = getRandomInt(0, 63);
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "eyebrowsColor", value: appearanceObject.eyebrowsColor }));

  var randomAgeing = agingTypes[getRandomInt(0, agingTypes.length - 1)];
  appearanceObject.ageing = randomAgeing == 0 ? 255 : randomAgeing - 1;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "ageing", value: appearanceObject.ageing }));

  var randomMakeup = makeupTypes[getRandomInt(0, makeupTypes.length - 1)];
  appearanceObject.makeup = randomMakeup == 0 ? 255 : randomMakeup - 1;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "makeup", value: appearanceObject.makeup }));

  appearanceObject.makeupColor1 = getRandomInt(0, 63);
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "makeupColor1", value: appearanceObject.makeupColor1 }));

  appearanceObject.makeupColor2 = getRandomInt(0, 63);
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "makeupColor2", value: appearanceObject.makeupColor2 }));

  var randomBlush = blushesTypes[getRandomInt(0, blushesTypes.length - 1)];
  appearanceObject.blush = randomBlush == 0 ? 255 : randomBlush - 1;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "blush", value: appearanceObject.blush }));

  appearanceObject.blushColor = getRandomInt(0, 63);
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "blushColor", value: appearanceObject.blushColor }));

  var randomComplexion = complexionTypes[getRandomInt(0, complexionTypes.length - 1)];
  appearanceObject.complexion = randomComplexion == 0 ? 255 : randomComplexion - 1;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "complexion", value: appearanceObject.complexion }));

  var randomSundamage = burnTypes[getRandomInt(0, burnTypes.length - 1)];
  appearanceObject.sundamage = randomSundamage == 0 ? 255 : randomSundamage - 1;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "sundamage", value: appearanceObject.sundamage }));

  var lipstick = getRandomInt(0, 1);
  if (lipstick == 0) {
    appearanceObject.lipstick = 2;
  } else {
    appearanceObject.lipstick = 255;
  }
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "lipstick", value: appearanceObject.lipstick }));

  appearanceObject.lipstickColor = getRandomInt(0, 63);
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "lipstickColor", value: appearanceObject.lipstickColor }));

  var randomMoles = frecklesTypes[getRandomInt(0, frecklesTypes.length - 1)];
  appearanceObject.moles = randomMoles == 0 ? 255 : randomMoles - 1;
  mp.trigger("callFromHTML::playerCreatorBrowser", "change_element", JSON.stringify({ element: "moles", value: appearanceObject.moles }));

  setHTMLelements(JSON.stringify(appearanceObject));
}

function setHTMLelements(listElement) {
  var listElement = JSON.parse(listElement);
  if (listElement.sex == "f") {
    appearanceObject.sex = "f";
    if (previousSex !== "f") {
      tl.fromTo(sex_chosen, 0.5, { left: "100px" }, { left: "2px" })
        .fromTo(sex_chosen, 0.4, { borderRadius: "0 8px 8px 0" }, { borderRadius: "8px 0 0 8px" }, "-=0.5");
    }
    previousSex = "f";
    currentHair = womanHair.indexOf(listElement.hair);
    document.querySelector('.hair-id').innerHTML = currentHair;

  } else if (listElement.sex == "m") {
    appearanceObject.sex = "m";
    if (previousSex !== "m") {
      tl.fromTo(sex_chosen, 0.5, { left: "2px" }, { left: "100px" })
        .fromTo(sex_chosen, 0.4, { borderRadius: "8px 0 0 8px" }, { borderRadius: "0 8px 8px 0" }, "-=0.5");
    }
    previousSex = "m";
    currentHair = manHair.indexOf(listElement.hair);
    document.querySelector('.hair-id').innerHTML = currentHair;
  }

  $('.custom-slider.skin-slider').data('ionRangeSlider').update({ from: listElement.skin });
  document.querySelector('.skin-input').value = listElement.skin;
  $('.custom-slider.shape-slider').data('ionRangeSlider').update({ from: listElement.shape });
  document.querySelector('.shape-input').value = listElement.shape;

  var faceElement = (listElement.face).split(",");
  currentFace[0] = faceElement[0];
  currentFace[1] = faceElement[1];
  currentFace[2] = faceElement[2];
  currentFace[3] = faceElement[3];
  currentFace[4] = faceElement[4];
  currentFace[5] = faceElement[5];
  currentFace[6] = faceElement[6];
  currentFace[7] = faceElement[7];
  currentFace[8] = faceElement[8];
  currentFace[9] = faceElement[9];
  currentFace[10] = faceElement[10];
  currentFace[11] = faceElement[11];
  currentFace[12] = faceElement[12];
  currentFace[13] = faceElement[13];
  currentFace[14] = faceElement[14];
  currentFace[15] = faceElement[15];
  currentFace[16] = faceElement[16];
  currentFace[17] = faceElement[17];
  currentFace[18] = faceElement[18];
  currentFace[19] = faceElement[19];

  $('.nose-width-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[0] });
  document.querySelector('.nose-width-slider-label').innerHTML = parseFloat(faceElement[0]).toFixed(2);
  $('.eye-open-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[11] });
  document.querySelector('.eye-open-slider-label').innerHTML = parseFloat(faceElement[11]).toFixed(2);
  $('.eyebrow-width-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[7] });
  document.querySelector('.eyebrow-width-slider-label').innerHTML = parseFloat(faceElement[7]).toFixed(2);
  $('.eyebrow-height-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[6] });
  document.querySelector('.eyebrow-height-slider-label').innerHTML = parseFloat(faceElement[6]).toFixed(2);
  $('.nose-length-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[2] });
  document.querySelector('.nose-length-slider-label').innerHTML = parseFloat(faceElement[2]).toFixed(2);
  $('.nose-height-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[1] });
  document.querySelector('.nose-height-slider-label').innerHTML = parseFloat(faceElement[1]).toFixed(2);
  $('.nose-end-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[4] });
  document.querySelector('.nose-end-slider-label').innerHTML = parseFloat(faceElement[4]).toFixed(2);
  $('.nose-bridge-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[3] });
  document.querySelector('.nose-bridge-slider-label').innerHTML = parseFloat(faceElement[3]).toFixed(2);
  $('.nose-bridge-move-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[5] });
  document.querySelector('.nose-bridge-move-slider-label').innerHTML = parseFloat(faceElement[5]).toFixed(2);
  $('.cheek-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[10] });
  document.querySelector('.cheek-slider-label').innerHTML = parseFloat(faceElement[10]).toFixed(2);
  $('.cheek-height-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[8] });
  document.querySelector('.cheek-height-slider-label').innerHTML = parseFloat(faceElement[8]).toFixed(2);
  $('.cheek-width-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[9] });
  document.querySelector('.cheek-width-slider-label').innerHTML = parseFloat(faceElement[9]).toFixed(2);
  $('.jaw-height-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[14] });
  document.querySelector('.jaw-height-slider-label').innerHTML = parseFloat(faceElement[14]).toFixed(2);
  $('.jaw-width-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[13] });
  document.querySelector('.jaw-width-slider-label').innerHTML = parseFloat(faceElement[13]).toFixed(2);
  $('.chin-length-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[15] });
  document.querySelector('.chin-length-slider-label').innerHTML = parseFloat(faceElement[15]).toFixed(2);
  $('.chin-width-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[17] });
  document.querySelector('.chin-width-slider-label').innerHTML = parseFloat(faceElement[17]).toFixed(2);
  $('.chin-position-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[16] });
  document.querySelector('.chin-position-slider-label').innerHTML = parseFloat(faceElement[16]).toFixed(2);
  $('.chin-shape-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[18] });
  document.querySelector('.chin-shape-slider-label').innerHTML = parseFloat(faceElement[18]).toFixed(2);
  $('.neck-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[19] });
  document.querySelector('.neck-slider-label').innerHTML = parseFloat(faceElement[19]).toFixed(2);
  $('.mouth-slider .custom-slider').data('ionRangeSlider').update({ from: faceElement[12] });
  document.querySelector('.mouth-slider-label').innerHTML = parseFloat(faceElement[12]).toFixed(2);

  document.querySelector('.mother-name').innerHTML = mothersNames[mothersId.indexOf(listElement.mother)];
  currentMother = mothersId.indexOf(listElement.mother);

  document.querySelector('.father-name').innerHTML = fathersNames[fathersId.indexOf(listElement.father)];
  currentFather = fathersId.indexOf(listElement.father);

  document.querySelector('.eye-id').innerHTML = eyeColors[listElement.eye];
  currentEye = listElement.eye;

  if (listElement.eyebrows == 255) {
    currentEyebrowType = 0;
  } else {
    currentEyebrowType = eyebrowTypes[eyebrowTypes.indexOf(listElement.eyebrows + 1)];
  }
  document.querySelector('.eyebrow-type').innerHTML = currentEyebrowType;


  document.querySelector('.eyebrow-color').innerHTML = listElement.eyebrowsColor;
  currentEyebrowColor = listElement.eyebrowsColor;

  document.querySelector('.hair-color1').innerHTML = listElement.hairColor1;
  currentHairColor1 = listElement.hairColor1;

  document.querySelector('.hair-color2').innerHTML = listElement.hairColor2;
  currentHairColor2 = listElement.hairColor2;

  if (listElement.facialhair == 255) {
    currentBeardType = 0;
  } else {
    currentBeardType = beardTypes[beardTypes.indexOf(listElement.facialhair + 1)]
  }
  document.querySelector('.beard-type').innerHTML = currentBeardType

  document.querySelector('.beard-color').innerHTML = listElement.facialhairColor;
  currentBeardColor = listElement.facialhairColor;

  if (listElement.lipstick == 255) {
    document.querySelector('.lipstick-type').innerHTML = "Nie";
  } else {
    document.querySelector('.lipstick-type').innerHTML = "Tak";
  }

  document.querySelector('.lipstick-color').innerHTML = listElement.lipstickColor;
  currentLipstickColor = listElement.lipstickColor;

  if (listElement.makeup == 255) {
    currentMakeupType = 0;
  } else {
    currentMakeupType = makeupTypes.indexOf(listElement.makeup + 1)
  }
  document.querySelector('.makeup-type').innerHTML = currentMakeupType;

  document.querySelector('.makeup-color1').innerHTML = listElement.makeupColor1;
  currentMakeupColor1 = listElement.makeupColor1;

  document.querySelector('.makeup-color2').innerHTML = listElement.makeupColor2;
  currentMakeupColor2 = listElement.makeupColor2;

  if (listElement.blush == 255) {
    currentBlushesType = 0;
  } else {
    currentBlushesType = blushesTypes.indexOf(listElement.blush + 1);
  }
  document.querySelector('.blushes-type').innerHTML = currentBlushesType;

  document.querySelector('.blushes-color').innerHTML = listElement.blushColor;
  currentBlushesColor = listElement.blushColor;

  if (listElement.moles == 255) {
    currentFreckles = 0;
  } else {
    currentFreckles = frecklesTypes.indexOf(listElement.moles + 1)
  }
  document.querySelector('.freckles-id').innerHTML = currentFreckles;

  if (listElement.ageing == 255) {
    currentAging = 0;
  } else {
    currentAging = agingTypes.indexOf(listElement.ageing + 1)
  }
  document.querySelector('.aging-id').innerHTML = currentAging;

  if (listElement.complexion == 255) {
    currentComplexion = 0;
  } else {
    currentComplexion = complexionTypes.indexOf(listElement.complexion + 1);
  }
  document.querySelector('.complexion-id').innerHTML = currentComplexion;

  if (listElement.blemishes == 255) {
    currentDiscoloration = 0;
  } else {
    currentDiscoloration = discolorationTypes.indexOf(listElement.blemishes + 1);
  }
  document.querySelector('.discoloration-id').innerHTML = currentDiscoloration;

  if (listElement.sundamage == 255) {
    currentBurn = 0;
  } else {
    currentBurn = burnTypes.indexOf(listElement.sundamage + 1);
  }
  document.querySelector('.burns-id').innerHTML = currentBurn

}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max + 1);
  return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomFloat(min, max) {
  var value = (Math.random() * (min - max) + max).toFixed(4);
  return value;
}

//CUSTOM SELECT
var customSelectState = false;
var currentCarouselItem = 0;
var isCarouselSliding = false;

$(document).ready(
  hideSelect()
);

$('#carousel').on('slide.bs.carousel', function (e) {
  if (!isCarouselSliding) {
    isCarouselSliding = true;
    var customSelectItems = document.getElementsByClassName("customSelectItem");
    var selectedItem = null;
    if (e.direction == "left") {
      if (++currentCarouselItem > customSelectItems.length - 1) {
        currentCarouselItem = 0;
      }
    } else if (e.direction == "right") {
      if (--currentCarouselItem < 0) {
        currentCarouselItem = 8;
      }
    }
    for (var item of customSelectItems) {
      if (currentCarouselItem == item.id) {
        selectedItem = item;
      }
    }
    var newHeader = selectedItem.innerHTML;
    $('#customSelectHeader').html(newHeader);
  }
  isCarouselSliding = false;
});

function hideSelect() {
  $('.customSelectItems').hide();
  $('#customSelectHeader').text("Brwi i oczy");
}

function customSelect() {
  if (!customSelectState) {
    customSelectState = true;
    $('.customSelectItems').show();
    $('#customSelect').css("box-shadow", "0 0 5px 1px darkcyan");
  } else {
    customSelectState = false;
    $('.customSelectItems').hide();
    $('#customSelect').css("box-shadow", "none");
  }
}

function selectOption(id) {
  if (customSelectState) {
    $('#carousel').carousel(parseInt(id));
    currentCarouselItem = id;
    var customSelectItems = document.getElementsByClassName("customSelectItem");
    var selectedItem = null;
    for (var item of customSelectItems) {
      if (id == item.id) {
        selectedItem = item;
      }
    }
    var newHeader = selectedItem.innerHTML;
    $('#customSelectHeader').html(newHeader);
    $('.customSelectItems').hide();
  }
}