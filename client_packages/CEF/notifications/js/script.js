const btn = document.getElementById("btn");
const container = document.querySelector(".container");
const tl = new TimelineMax();
var id = 1;
const NOTIFY_TIME = 8;

function newNotify(json) {
  var notify = JSON.parse(json);
  var header = notify.header;
  var content = notify.content;
  var color = notify.color;
  var icon = notify.icon;
  createNotification(header, content, color, icon);
}

function createNotification(head, content, color, icon) {
  const notif = document.createElement("div");
  notif.setAttribute("id", id);
  notif.classList.add("toast");

  const row = document.createElement("div");
  row.classList.add("row");
  notif.append(row);

  const col1 = document.createElement("div");
  if (icon != null) {
    col1.classList.add("col-2");
  }  
  col1.classList.add("col-left");
  row.append(col1);

  const col2 = document.createElement("div");
  col2.classList.add("col");
  col2.classList.add("col-right");
  row.append(col2);

  const header = document.createElement("h6");
  header.innerHTML = head;
  col2.append(header);

  const hr = document.createElement("hr");
  col2.append(hr);

  const text = document.createElement("div");
  text.classList.add("content");
  text.innerHTML = content;
  col2.append(text);

  const iconSpan = document.createElement("span");
  switch(icon) {
    case "wykrzyknik":
      iconSpan.innerHTML = '<i id="' + id + '" class="fas fa-exclamation"></i>';
      break;
    default:
      iconSpan.innerHTML = '';
      break;
  }
  col1.append(iconSpan);

  container.appendChild(notif);

  $("#" + id).css("border-top", "3px solid " + color);
  var rightHeight = col2.offsetHeight;
  console.log(rightHeight + " id: " + id)
  $("#" + id +".fas").css("transform", "scale(" + rightHeight/16 + ", " + rightHeight/16 + ")");
  $("#" + id +".fas").css("margin-top", rightHeight * 0.65 - 30)
  id++;

  tl.fromTo(notif, 0.3, { opacity: "0" }, { opacity: "0.7" });
  setTimeout(() => {
    tl.fromTo(notif, 0.3, { opacity: "0.7" }, { opacity: "0" });
    setTimeout(() => {
      notif.remove();
    }, 500);
  }, NOTIFY_TIME * 1000);
}