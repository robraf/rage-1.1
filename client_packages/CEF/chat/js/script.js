var canPlayerChat = false;
var msg_id = 0;
var playerId = -1;
var playerNick = "";

let chat =
{
    size: 0,
    maxLength: 300,
    container: null,
    input: null,
    enabled: false,
    active: true,
    timer: null,
    previous: [],
    prevIndex: 0,
    isOpen: false, 
    currentIndex: 0,
    historyLength: 5,
    hide_chat: 30 * 60 * 1000 // 30min
};

function setPlayerChat(value) {
    if (value) {
        canPlayerChat = true;
    } else {
        canPlayerChat = false;
    }
}

function drawMessage(text, idOfPlayer, nickOfPlayer) {
    playerId = idOfPlayer;
    playerNick = nickOfPlayer.toLowerCase();
    chatAPI.push(text);
}

function enableChatInput(enable) {
    if (chat.active == false
        && enable == true)
        return;
    if (enable != (chat.input != null)) {
        mp.invoke("focus", enable);
        if (enable) {
            //mp.trigger("changeChatState", true);
            $("#chat").css("opacity", 1);
            chat.input = $("#chat").append('<div><input id="chat_msg" type="text" /></div>').children(":last");
            chat.input.children("input").focus();
        } else {
            chat.input.fadeOut('fast', function () {
                //mp.trigger("changeChatState", false);
                chat.input.remove();
                chat.input = null;
            });
        }
    }
}
var chatAPI =
{
    push: (text) => {
        chat.size++;
        if (chat.size >= 50) {
            chat.container.children(":first").remove();
        }
        var message = JSON.parse(text);
        var timeNow = new Date();
        var hours = timeNow.getHours();
        var minutes = timeNow.getMinutes();
        var timeString = "" + ((hours < 10) ? "0" : "") + hours;
        timeString += ((minutes < 10) ? ":0" : ":") + minutes;
        var html = "";
        message.text = message.text.replace(/[\r]+/g, "\\r");
        message.text = message.text.replace(/[\n]+/g, "\\n");
        message.text = message.text.replace(/[\t]+/g, "\\t");
        if (message.type == "chat") {//wiadomosc napisana przez gracza do innych
            html += '<div class="row info"><div class="message badge badge-secondary badge-pill">' + timeString + '</div>';
            switch (message.chatType) {
                case "Admin":
                    html += '<div class="message badge badge-pill badge-danger chat-type">A</div>';
                    break;
                case "Globalny":
                    html += '<div class="message badge badge-pill badge-warning chat-type">G</div>';
                    break;
                case "Policja":
                    html += '<div class="message badge badge-pill badge-info chat-type">P</div>';
                    break;
                default: //lokalny
                    html += '<div class="message badge badge-pill badge-dark chat-type"></div>';
                    break;
            }
            switch (message.playerType) {
                case "ADMIN":
                    html += '<div class="message nick text-danger">' + message.nick + ' [' + message.playerId + ']</div></div>';
                    break;
                case "SUP":
                    html += '<div class="message nick text-success">' + message.nick + ' [' + message.playerId + ']</div></div>';
                    break;
                case "MOD":
                    html += '<div class="message nick text-info">' + message.nick + ' [' + message.playerId + ']</div></div>';
                    break;
                default: //zwykły gracz, bez rangi
                    html += '<div class="message nick">' + message.nick + ' [' + message.playerId + ']</div></div>';
                    break;
            }
            html += '<div class="row"><div id="' + msg_id + '" class="message text"></div></div>';
        } else if (message.type == "announcement") {
            html += '<div class="row info"><div class="message badge badge-secondary badge-pill">' + timeString + '</div>';
            switch (message.color) {
                case "green":
                    html += '<div class="message header bg-success">' + message.header + '</div></div>';
                    html += '<div class="row"><div class="message text announcement border border-success">' + message.text + '</div></div>';
                    break;
                case "red":
                    html += '<div class="message header bg-danger">' + message.header + '</div></div>';
                    html += '<div class="row"><div class="message text announcement border border-danger">' + message.text + '</div></div>';
                    break;
                case "orange":
                    html += '<div class="message header bg-warning">' + message.header + '</div></div>';
                    html += '<div class="row"><div class="message text announcement border border-warning">' + message.text + '</div></div>';
                    break;
                case "blue":
                    html += '<div class="message header bg-info">' + message.header + '</div></div>';
                    html += '<div class="row"><div class="message text announcement border border-info">' + message.text + '</div></div>';
                    break;
                default:
                    html += '<div class="message header">' + message.header + '</div></div>';
                    html += '<div class="row"><div class="message text announcement">' + message.text + '</div></div>';
                    break;
            }
        }
        else if (message.type == "player") { //informacja dla gracza
            html += '<div class="row info"></div>';
            switch (message.color) {
                case "green":
                    html += '<div class="row"><div class="message text player-info border border-success">' + message.text + '</div></div>';
                    break;
                case "red":
                    html += '<div class="row"><div class="message text player-info border border-danger">' + message.text + '</div></div>';
                    break;
                case "orange":
                    html += '<div class="row"><div class="message text player-info border border-warning">' + message.text + '</div></div>';
                    break;
                case "blue":
                    html += '<div class="row"><div class="message text player-info border border-info">' + message.text + '</div></div>';
                    break;
                default:
                    html += '<div class="row"><div class="message text player-info">' + message.text + '</div></div>';
                    break;
            }
        }
        var isIdMarked = (message.text).includes("@" + playerId);
        var messageTemp = (message.text).toLowerCase();
        var isNickMarked = (messageTemp).includes("@" + playerNick);
        if (/^\s*\w+/.test(message.text) || !/^\s+/.test(message.text)) {
            chat.container.append('<li class="msg">' + html + '</li>');
            var msg_content = document.getElementById(msg_id);
            if (message.type == "chat") {
                msg_content.innerText = message.text;
                if (isIdMarked || isNickMarked) {
                    msg_content.classList.add("border");
                    msg_content.classList.add("border-warning");
                }
                msg_id++;
            }
        }
        var element = document.getElementById("chat");
        element.scrollTop = element.scrollHeight;
    },
    clear: () => {
        chat.container.html("");
    },
    activate: (toggle) => {
        if (toggle == false
            && (chat.input != null))
            enableChatInput(false);

        chat.active = toggle;
    },
    show: (toggle) => {
        if (toggle)
            $("#chat").show();
        else
            $("#chat").hide();

        chat.active = toggle;
    }
};

function hide() {
    chat.timer = setTimeout(function () {
        $("#chat").css("opacity", 0.5);
    }, chat.hide_chat);
}

function show() {
    clearTimeout(chat.timer);
    $("#chat").css("opacity", 1);
}

var lastKeyPressedTime = Date.now();

$(document).ready(function () {
    chat.container = $("#chat ul#chat_messages");
    hide();
    $(".ui_element").show();
    $("body").keydown(function (event) {
        if (event.which == 84 && chat.input == null
            && chat.active == true && canPlayerChat) {
            chat.isOpen = true;
            var currentTime = Date.now();
            if ((currentTime - lastKeyPressedTime) >= 500) {
                mp.trigger("changeChatState", true);
                enableChatInput(true);
                event.preventDefault();
                show();
                chat.prevIndex = chat.currentIndex;
                lastKeyPressedTime = Date.now();
            }
        }
        else if (event.which == 13 && chat.input != null) {
            var value = chat.input.children("input").val();
            mp.trigger("changeChatState", false);
            chat.isOpen = false;
            if (value.length > 0) {
                if (chat.previous.length >= chat.historyLength) {
                    chat.previous.splice(0, 1);
                }
                if (chat.previous.length > 0) {
                    if (chat.previous[chat.previous.length - 1] != value) {
                        chat.previous.push(value);
                        if (chat.currentIndex < 5) {
                            chat.currentIndex++;
                        }
                    }
                } else {
                    chat.previous.push(value);
                    if (chat.currentIndex < 5) {
                        chat.currentIndex++;
                    }
                }
                chat.prevIndex = chat.currentIndex;
                if (value[0] == "/") {
                    value = value.substr(1);

                    if (value.length > 0 && value.length <= chat.maxLength)
                        mp.invoke("command", value);
                }
                else {
                    if (value.length <= chat.maxLength)
                        mp.invoke("chatMessage", value);
                }
            }
            enableChatInput(false);
            hide();
        }
        else if (event.which == 27 && chat.input != null) {
            enableChatInput(false);
            mp.trigger("changeChatState", false);
            hide();
        }
        else if (event.which == 38 && chat.input != null) {
            event.preventDefault();
            if (chat.prevIndex > 0) {
                chat.prevIndex--;
                chat.input.children("input").val(chat.previous[chat.prevIndex]);
            }
        }
        else if (event.which == 40 && chat.input != null) {
            event.preventDefault();
            if (chat.prevIndex < chat.historyLength) {
                chat.prevIndex++;
                chat.input.children("input").val(chat.previous[chat.prevIndex]);
            }
        }
    });
});

//prevent autoscroll 
$(document).keydown(function(e) {
    if (e.which == 32 && chat.isOpen===false) {
        return false;
    }
});