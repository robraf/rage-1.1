//synchronizacja hamulcow recznych po stworzeniu pojazdow

//zniszczenia szyb, drzwi
let player = mp.players.local;

setInterval(function () {
    mp.vehicles.forEachInRange(player.position, 400,
        (vehicle) => {
            var isVehiclePlayer = false;
            if (player.vehicle) {
                if (player.vehicle == vehicle) {
                    isVehiclePlayer = true;
                }
            }
           if (!isVehiclePlayer) {
                let vehicleFreeze = vehicle.getVariable("vehicleFreeze");
                if (vehicleFreeze !== null && vehicleFreeze !== undefined) {
                    vehicle.freezePosition(vehicleFreeze);
                    if(vehicleFreeze){
                        vehicle.setCanBeVisiblyDamaged(false);
                    }else{
                        vehicle.setCanBeVisiblyDamaged(true);
                    }
                }

                //doors damage
                let vehicleDoors = vehicle.getVariable("vehicleDoors");
                if (vehicleDoors !== null && vehicleDoors !== undefined) {
                    var doorsToBroken = [];
                    var isAnyDoorsToFix = false;
                    if (vehicleDoors.door0 == 1) {
                        //vehicle.setDoorBroken(0, true)
                        doorsToBroken.push(0);
                    } else {
                        if (vehicle.isDoorDamaged(0)) {
                            isAnyDoorsToFix = true;
                        }
                    }
                    if (vehicleDoors.door1 == 1) {
                        //vehicle.setDoorBroken(1, true)
                        doorsToBroken.push(1);
                    } else {
                        if (vehicle.isDoorDamaged(1)) {
                            isAnyDoorsToFix = true;
                        }
                    }
                    if (vehicleDoors.door2 == 1) {
                        //vehicle.setDoorBroken(2, true)
                        doorsToBroken.push(2);
                    } else {
                        if (vehicle.isDoorDamaged(2)) {
                            isAnyDoorsToFix = true;
                        }
                    }
                    if (vehicleDoors.door3 == 1) {
                        //vehicle.setDoorBroken(3, true)
                        doorsToBroken.push(3);
                    } else {
                        if (vehicle.isDoorDamaged(3)) {
                            isAnyDoorsToFix = true;
                        }
                    }
                    if (vehicleDoors.door4 == 1) {
                        //vehicle.setDoorBroken(4, true)
                        doorsToBroken.push(4);
                    } else {
                        if (vehicle.isDoorDamaged(4)) {
                            isAnyDoorsToFix = true;
                        }
                    }
                    if (vehicleDoors.door5 == 1) {
                        //vehicle.setDoorBroken(5, true)
                        doorsToBroken.push(5);
                    } else {
                        if (vehicle.isDoorDamaged(5)) {
                            isAnyDoorsToFix = true;
                        }
                    }

                    if (isAnyDoorsToFix) {
                        vehicle.setFixed();
                    }

                    for (var i = 0; i < doorsToBroken.length; i++) {
                        vehicle.setDoorBroken(i, true)
                    }
                }

                //windows damage
                let vehicleWindows = vehicle.getVariable("vehicleWindows");
                if (vehicleWindows !== null && vehicleWindows !== undefined) {
                    vehicleWindows.window0 == 1 ? vehicle.smashWindow(0) : vehicle.fixWindow(0);
                    vehicleWindows.window1 == 1 ? vehicle.smashWindow(1) : vehicle.fixWindow(1);
                    vehicleWindows.window2 == 1 ? vehicle.smashWindow(2) : vehicle.fixWindow(2);
                    vehicleWindows.window3 == 1 ? vehicle.smashWindow(3) : vehicle.fixWindow(3);
                    vehicleWindows.window4 == 1 ? vehicle.smashWindow(4) : vehicle.fixWindow(4);
                    vehicleWindows.window5 == 1 ? vehicle.smashWindow(5) : vehicle.fixWindow(5);
                    vehicleWindows.window6 == 1 ? vehicle.smashWindow(6) : vehicle.fixWindow(6);
                    vehicleWindows.window7 == 1 ? vehicle.smashWindow(7) : vehicle.fixWindow(7);
                }
            }
        }
    );
}, 500);

//synchro drzwi
//vehicle.isDoorDamaged(doorID);
//setDoorBroken