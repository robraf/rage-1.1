let player = mp.players.local;
let fuelStationsList = [];
const MAX_DISTANCE = 80;
let isFuelStationsListUpdateing = false;
let isPlayerInDistributor = null;

mp.events.add('playerEnterColshape', (shape) => {
    if (isPlayerInDistributor===null) {
        var shapeDistributor = shape.getVariable("distributorData");
        if (shapeDistributor !== null && shapeDistributor !== undefined) {
            isPlayerInDistributor = shapeDistributor;
        }
    }
});

mp.events.add('playerExitColshape', (shape) => {
    if (isPlayerInDistributor!==null) {
        var shapeDistributor = shape.getVariable("distributorData");
        if (shapeDistributor !== null && shapeDistributor !== undefined) {
            isPlayerInDistributor = null;
        }
    }
});

mp.events.add('callClient::updatePlayerFuelStationList', (stationsList) => {
    isFuelStationsListUpdateing = true;
    fuelStationsList = [];
    fuelStationsList = JSON.parse(stationsList);
    isFuelStationsListUpdateing = false;
});

mp.events.add("render", () => {
    if (isPlayerInDistributor!==null) {
        if (!player.vehicle) {
            let playerChatting = player.getVariable("playerChatting");
            if (playerChatting !== null && playerChatting !== undefined && !playerChatting) {
                //pressed 'E'
                    if(mp.game.controls.isControlJustPressed(0, 38)){
                        //sprawdzić pobliski pojazd
                        let closestVehicle = getClosestVehicleInRange(3);
                        if(closestVehicle!==null){
                            var vehicleFuel = closestVehicle.getVariable("vehicleFuel");
                            var vehicleData = closestVehicle.getVariable("vehicleData");
                            
                            if(vehicleFuel!==null && vehicleFuel!==undefined && vehicleData!==null && vehicleData!==undefined){
                                if(vehicleData.engineType!=="-"){
                                    let distributorTypes = (isPlayerInDistributor.types).toString();
                                    let engineType = getEngineType(vehicleData.engineType);
                                    if(distributorTypes.includes(engineType)){
                                        if(!closestVehicle.getIsEngineRunning()){
                                            //otwórz okno
                                            //na bieżąco sprawdzać czy jest włączony silnik i czy auto jest obok
                                            var price = 0;
                                            var amount = 0;
                                            if(engineType==="pb"){
                                                price = isPlayerInDistributor.pricePB;
                                                amount = isPlayerInDistributor.amountPB;
                                            }else if(engineType==="on"){
                                                price = isPlayerInDistributor.priceON;
                                                amount = isPlayerInDistributor.amountON;
                                            }else if(engineType==="lpg"){
                                                price = isPlayerInDistributor.priceLPG;
                                                amount = isPlayerInDistributor.amountLPG;
                                            }
                                            mp.events.callRemote("callServer::sendMessageToPlayer", "Paliwo: "+vehicleFuel.toFixed(2)+"/"+parseFloat(vehicleData.fuelTank), "orange");
                                            mp.events.callRemote("callServer::sendMessageToPlayer", "Cena za litr "+engineType+" - "+price+"$", "orange");
                                            mp.events.callRemote("callServer::sendMessageToPlayer", "Na stacji pozostało "+amount+" litrów", "orange");                                      
                                            
                                        }else{
                                            mp.events.call("call::addNotify", "Silnik", "Przed zatankowaniem zgaś silnik w pojeździe!", "red", null);
                                        }
                                    }else{
                                        mp.events.call("call::addNotify", "Dystrybutor", "Przy tym dystrybutorze nie zatankujesz!", "red", null);
                                    }                                  
                                }
                                else{
                                    mp.events.call("call::addNotify", "Paliwo", "Tego pojazdu nie da się zatankować!", "red", null);
                                }
                            }
                        }
                    }
            }
        }
    }
    if (!isFuelStationsListUpdateing) {
        var playerData = player.getVariable("playerData");
        if (playerData !== null && playerData !== undefined && playerData.spawned) {
            for (var i = 0; i < fuelStationsList.length; i++) {
                var fuelStation = fuelStationsList[i];
                var distance = mp.game.gameplay.getDistanceBetweenCoords(
                    player.position.x,
                    player.position.y,
                    player.position.z,
                    fuelStation.position.x,
                    fuelStation.position.y,
                    fuelStation.position.z,
                    true
                );

                if (distance <= MAX_DISTANCE) {
                    var posX = fuelStation.position.x;
                    var posY = fuelStation.position.y;
                    var posZ = fuelStation.position.z;
                    var scale = scaleFunc(0.4, (distance / MAX_DISTANCE));

                    let posFuel = mp.game.graphics.world3dToScreen2d(posX, posY, posZ + 1);
                    if (posFuel !== null && posFuel !== undefined) {
                        mp.game.graphics.drawText(
                            fuelStation.name,
                            [
                                posFuel.x,
                                posFuel.y
                            ],
                            {
                                font: 4,
                                color: [125, 205, 205, 255],
                                scale: [scale, scale],
                                outline: true
                            }
                        );
                        mp.game.graphics.drawText(
                            "BENZYNA - " + fuelStation.pricePB + " $/litr",
                            [
                                posFuel.x,
                                posFuel.y + (0.038 * (scale * 1.5))
                            ],
                            {
                                font: 4,
                                color: [225, 225, 225, 255],
                                scale: [scale, scale],
                                outline: true
                            }
                        );
                        mp.game.graphics.drawText(
                            "DIESEL - " + fuelStation.priceON + " $/litr",
                            [
                                posFuel.x,
                                posFuel.y + (0.076 * (scale * 1.5))
                            ],
                            {
                                font: 4,
                                color: [225, 225, 225, 255],
                                scale: [scale, scale],
                                outline: true
                            }
                        );
                        mp.game.graphics.drawText(
                            "LPG - " + fuelStation.priceLPG + " $/m3",
                            [
                                posFuel.x,
                                posFuel.y + (0.114 * (scale * 1.5))
                            ],
                            {
                                font: 4,
                                color: [225, 225, 225, 255],
                                scale: [scale, scale],
                                outline: true
                            }
                        );
                    }
                }
            }
        }
    }
});

function scaleFunc(co, procent) {
    return co - co * procent;
}

const getClosestVehicleInRange = (range) => {
    let closestVehicle = null;
    let closestDistance = range + 1;
    const position = mp.players.local.position;
    mp.vehicles.forEachInRange(position, range, (vehicle) => {
      const distToPlayer = calcDist(position, vehicle.position);
      if (distToPlayer < closestDistance) {
        closestVehicle = vehicle;
        closestDistance = distToPlayer;
      }
    });
    return closestVehicle;
  }

  const calcDist = (v1, v2) => {
    return Math.sqrt(Math.pow(v1.x - v2.x, 2) + Math.pow(v1.y - v2.y, 2) + Math.pow(v1.z - v2.z, 2));
  };

  function getEngineType(type) {
    var engineType = "";
    switch (type) {
        case "benzyna":
            engineType = "pb";
            break;
        case "diesel":
            engineType = "on";
            break;
        case "gaz":
            engineType = "lpg";
            break;
        case "elektryczny":
            engineType = "e";
            break;
        default:
            break;
    }
    return engineType;
}