var isPlayerBanned = "";
var date = "";
var alertTime = 0;

mp.events.add("callClient::playerHasActiveBan", (date, reason, kickTime) => {
    isPlayerBanned = "" + reason;
    alertTime = kickTime;
    this.date = date;
});


var updateTime = setInterval(function () {
    alertTime = alertTime - 1;
    if (alertTime < 0)
        alertTime = 0;
}, 1000 * 1);

mp.events.add("render", () => {
    if (isPlayerBanned.length > 0) {
        mp.game.graphics.drawText("Zostałeś zbanowany na serwerze za:" , [0.5, 0.4], {
            font: 4,
            color: [255, 255, 255, 185],
            scale: [0.7, 0.7],
            outline: true
        });
        mp.game.graphics.drawText("~g~"+isPlayerBanned , [0.5, 0.45], {
            font: 4,
            color: [255, 255, 255, 185],
            scale: [0.7, 0.7],
            outline: true
        });
        mp.game.graphics.drawText("Ban wygasa:" , [0.5, 0.5], {
            font: 4,
            color: [255, 255, 255, 185],
            scale: [0.7, 0.7],
            outline: true
        });
        mp.game.graphics.drawText("~g~"+date , [0.5, 0.55], {
            font: 4,
            color: [255, 255, 255, 185],
            scale: [0.7, 0.7],
            outline: true
        });
        mp.game.graphics.drawText("Automatyczne rozłączenie za ~r~" + alertTime + " ~w~sekund/y", [0.5, 0.6], {
            font: 4,
            color: [255, 255, 255, 185],
            scale: [0.7, 0.7],
            outline: true
        });
    }
});