-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 18 Gru 2020, 16:42
-- Wersja serwera: 10.4.16-MariaDB
-- Wersja PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `rage`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `action_history`
--

CREATE TABLE `action_history` (
  `action_id` int(11) NOT NULL,
  `action_type` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `action_user_id` int(255) NOT NULL,
  `action_reason` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `action_time` datetime NOT NULL DEFAULT current_timestamp(),
  `action_who` int(11) NOT NULL,
  `action_length` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `action_history`
--

INSERT INTO `action_history` (`action_id`, `action_type`, `action_user_id`, `action_reason`, `action_time`, `action_who`, `action_length`) VALUES
(58, 'mute', 20, ' tak', '2020-12-06 23:27:21', 20, 1),
(59, 'mute', 22, ' wypad', '2020-12-07 00:37:00', 20, 1),
(60, 'mute', 20, ' Mute testowy', '2020-12-07 02:08:16', 20, 1),
(61, 'mute', 20, ' Mute testowy', '2020-12-07 02:09:18', 20, 1),
(62, 'mute', 24, ' Mutowanie testowe', '2020-12-07 02:10:11', 20, 1),
(63, 'mute', 24, ' Mutowanie testowe', '2020-12-07 02:11:32', 20, 1),
(64, 'mute', 24, ' Mutowanie testowe', '2020-12-07 02:12:35', 20, 1),
(65, 'mute', 1, ' idz do konkurencji, FL pff', '2020-12-07 21:17:45', 20, 10),
(66, 'ban', 20, ' wyzwiska', '2020-12-07 21:29:27', 20, 1),
(67, 'ban', 20, ' wypad', '2020-12-07 21:32:26', 20, 1),
(68, 'ban', 20, ' wypad', '2020-12-07 21:35:33', 20, 1),
(69, 'ban', 20, ' tak', '2020-12-07 21:38:50', 20, 1),
(70, 'ban', 20, ' joł', '2020-12-07 21:40:30', 20, 1),
(71, 'mute', 20, ' test', '2020-12-08 00:25:41', 20, 1),
(72, 'mute', 20, ' mute', '2020-12-08 00:29:29', 20, 1),
(73, 'mute', 20, ' test', '2020-12-09 14:48:00', 20, 1),
(74, 'mute', 22, ' nie wolno', '2020-12-10 00:42:01', 20, 1),
(75, 'mute', 20, ' a', '2020-12-10 22:00:08', 20, 1),
(76, 'ban', 20, ' sprawdzam coś', '2020-12-12 03:22:43', 20, 1),
(77, 'mute', 20, ' test', '2020-12-13 01:28:45', 20, 0),
(78, 'mute', 20, ' witam', '2020-12-13 01:32:15', 20, 1),
(79, 'mute', 1, ' wita', '2020-12-13 01:34:37', 20, 1),
(80, 'mute', 20, ' za co', '2020-12-13 01:35:57', 20, 1),
(81, 'mute', 20, ' klamstwa', '2020-12-16 15:46:13', 20, 1),
(82, 'mute', 20, ' klamstwa', '2020-12-16 15:48:05', 20, 1),
(83, 'mute', 19, ' test', '2020-12-16 15:54:05', 20, 1),
(84, 'mute', 24, ' obraza', '2020-12-16 15:54:51', 20, 2),
(85, 'mute', 20, ' a', '2020-12-16 15:55:16', 20, 1),
(86, 'mute', 18, ' test', '2020-12-16 16:13:46', 20, 1),
(87, 'ban', 32, ' ziomal', '2020-12-16 16:15:02', 20, 10),
(88, 'ban', 31, ' witam', '2020-12-16 16:17:09', 20, 1),
(89, 'ban', 32, ' test', '2020-12-16 16:18:49', 20, 1),
(90, 'ban', 31, ' we', '2020-12-16 16:19:58', 20, 1),
(91, 'ban', 32, ' eureka', '2020-12-16 16:20:56', 20, 1),
(92, 'ban', 24, ' bambiku', '2020-12-16 16:21:24', 20, 10),
(93, 'kick', 1, ' reconnecta', '2020-12-16 17:14:36', 20, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `announcements`
--

CREATE TABLE `announcements` (
  `ann_id` int(255) NOT NULL,
  `ann_header` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `ann_text` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `ann_color` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `ann_start` datetime DEFAULT NULL,
  `ann_expires` datetime NOT NULL,
  `ann_interval` int(255) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `announcements`
--

INSERT INTO `announcements` (`ann_id`, `ann_header`, `ann_text`, `ann_color`, `ann_start`, `ann_expires`, `ann_interval`) VALUES
(1, 'Testy', 'Trwają beta testy naszego serwera, prosimy zgłaszać wszelkie uwagi w dziale <b>POMOC</b> na forum! (co ~10 min)', 'red', NULL, '2023-02-20 21:25:43', 600);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `appearances`
--

CREATE TABLE `appearances` (
  `appearance_user_id` int(11) NOT NULL,
  `appearance_chosen` tinyint(1) NOT NULL DEFAULT 0,
  `appearance_sex` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT 'm',
  `appearance_eye` int(11) NOT NULL DEFAULT 0,
  `appearance_hair` int(11) NOT NULL DEFAULT 0,
  `appearance_hair_color` int(11) NOT NULL DEFAULT 0,
  `appearance_mother` int(11) NOT NULL DEFAULT 0,
  `appearance_father` int(11) NOT NULL DEFAULT 0,
  `appearance_shape` float NOT NULL DEFAULT 0.5,
  `appearance_skin` float NOT NULL DEFAULT 0.5,
  `appearance_face` mediumtext COLLATE utf8_polish_ci NOT NULL DEFAULT '0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0',
  `appearance_blemishes` int(11) NOT NULL DEFAULT 255,
  `appearance_facialhair` int(11) NOT NULL DEFAULT 255,
  `appearance_facialhair_color` int(11) NOT NULL DEFAULT 0,
  `appearance_eyebrows` int(11) NOT NULL DEFAULT 255,
  `appearance_eyebrows_color` int(11) NOT NULL DEFAULT 0,
  `appearance_ageing` int(11) NOT NULL DEFAULT 255,
  `appearance_makeup` int(11) NOT NULL DEFAULT 255,
  `appearance_makeup_color1` int(11) NOT NULL DEFAULT 0,
  `appearance_makeup_color2` int(11) NOT NULL DEFAULT 0,
  `appearance_blush` int(11) NOT NULL DEFAULT 255,
  `appearance_blush_color` int(11) NOT NULL DEFAULT 0,
  `appearance_complexion` int(11) NOT NULL DEFAULT 255,
  `appearance_sundamage` int(11) NOT NULL DEFAULT 255,
  `appearance_lipstick` int(11) NOT NULL DEFAULT 255,
  `appearance_lipstick_color` int(11) NOT NULL DEFAULT 0,
  `appearance_moles` int(11) NOT NULL DEFAULT 255
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `appearances`
--

INSERT INTO `appearances` (`appearance_user_id`, `appearance_chosen`, `appearance_sex`, `appearance_eye`, `appearance_hair`, `appearance_hair_color`, `appearance_mother`, `appearance_father`, `appearance_shape`, `appearance_skin`, `appearance_face`, `appearance_blemishes`, `appearance_facialhair`, `appearance_facialhair_color`, `appearance_eyebrows`, `appearance_eyebrows_color`, `appearance_ageing`, `appearance_makeup`, `appearance_makeup_color1`, `appearance_makeup_color2`, `appearance_blush`, `appearance_blush_color`, `appearance_complexion`, `appearance_sundamage`, `appearance_lipstick`, `appearance_lipstick_color`, `appearance_moles`) VALUES
(1, 1, 'm', 7, 0, 0, 14, 14, 0.5, 0.5, '0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0', 255, 255, 0, 255, 0, 255, 255, 0, 0, 255, 0, 255, 255, 255, 0, 255),
(2, 0, 'm', 0, 0, 0, 0, 0, 0.5, 0.5, '0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0', 255, 255, 0, 255, 0, 255, 255, 0, 0, 255, 0, 255, 255, 255, 0, 255),
(20, 1, 'm', 13, 0, 0, 19, 5, 0.5, 0.5, '0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0', 255, 255, 0, 255, 0, 255, 255, 0, 0, 255, 0, 255, 255, 255, 0, 255),
(22, 1, 'm', 27, 0, 0, 9, 22, 0.5, 0.5, '0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0', 255, 255, 0, 255, 0, 255, 255, 0, 0, 255, 0, 255, 255, 255, 0, 255),
(24, 1, 'm', 10, 0, 0, 9, 1, 0.5, 0.5, '0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0', 255, 255, 0, 255, 0, 255, 255, 0, 0, 255, 0, 255, 255, 255, 0, 255),
(25, 0, 'm', 1, 1, 5, 1, 2, 0.5, 0.5, '0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 0, 'm', 1, 1, 5, 1, 2, 0.5, 0.5, '0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(27, 0, 'm', 1, 1, 5, 1, 2, 0.5, 0.5, '0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(28, 1, 'm', 1, 1, 5, 1, 2, 0.5, 0.5, '0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 0, 'm', 1, 1, 5, 1, 2, 0.5, 0.5, '0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 0, 'm', 1, 1, 5, 1, 2, 0.5, 0.5, '0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(31, 0, 'm', 1, 1, 5, 1, 2, 0.5, 0.5, '0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(32, 0, 'm', 1, 1, 5, 1, 2, 0.5, 0.5, '0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3,0.2,0.3', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bans`
--

CREATE TABLE `bans` (
  `ban_id` int(11) NOT NULL,
  `ban_serial` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `ban_socialclub` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `ban_reason` text COLLATE utf8_polish_ci NOT NULL,
  `ban_admin` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `ban_time` datetime NOT NULL DEFAULT current_timestamp(),
  `ban_expiration_date` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `mutes`
--

CREATE TABLE `mutes` (
  `mute_user_id` int(11) NOT NULL,
  `mute_reason` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `mute_expires` bigint(20) NOT NULL,
  `mute_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `mutes`
--

INSERT INTO `mutes` (`mute_user_id`, `mute_reason`, `mute_expires`, `mute_admin`) VALUES
(18, ' test', 1608131686848, 20),
(19, ' test', 1608130505805, 20);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `user_id` int(255) NOT NULL,
  `user_is_loggon` tinyint(1) NOT NULL DEFAULT 0,
  `user_login` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `user_password` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `user_admin` mediumtext COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `user_money` float NOT NULL DEFAULT 500,
  `registration_date` datetime NOT NULL DEFAULT current_timestamp(),
  `user_posX` float NOT NULL,
  `user_posY` float NOT NULL,
  `user_posZ` float NOT NULL,
  `user_heading` float NOT NULL,
  `user_banned` tinyint(1) NOT NULL DEFAULT 0,
  `user_online_time` bigint(20) NOT NULL DEFAULT 0,
  `user_serial` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `user_socialclub` mediumtext COLLATE utf8_polish_ci DEFAULT NULL,
  `user_logout_time` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`user_id`, `user_is_loggon`, `user_login`, `user_password`, `user_admin`, `user_money`, `registration_date`, `user_posX`, `user_posY`, `user_posZ`, `user_heading`, `user_banned`, `user_online_time`, `user_serial`, `user_socialclub`, `user_logout_time`) VALUES
(1, 0, 'r', '$2a$10$/NxVjT/Lj4.N86FzXwc/8O6ctDUS9a7.csOswIl8tQ43CxUloCp5q', 'ADMIN', 500, '2020-11-28 00:03:51', 888.346, -843.811, 43.2921, 153.507, 0, 0, 'D8903A045B0CF9D8A6CA0E6C753476B075F41EF80902CCA825F018C8DD223EE05BACC3E84D565A90E3120E38AEA66DC0574A08A056B6E970C7DEB2BCC28CD980', 'Rafistaa', 1608136689588),
(20, 1, 'Rafista', '$2a$10$.khoV2cj3XQDU0tmPrsYoefaUroBvsXIenuP0VzkHFqJVKN9J64EW', 'ADMIN', 500, '2020-11-27 19:00:02', 252.635, -361.611, 44.6283, 179.256, 0, 0, 'D8903A045B0CF9D8A6CA0E6C753476B075F41EF80902CCA825F018C8DD223EE05BACC3E84D565A90E3120E38AEA66DC0574A08A056B6E970C7DEB2BCC28CD980', 'Rafistaa', 1608282165585),
(22, 0, 'ReZus', '$2a$10$IqXNEfO4qczgzEGdMOLLeuOBRVuDmH135Mwprpb9Ykfty/ilcG/GW', 'ADMIN', 500, '2020-12-03 19:24:11', -2110.29, 284.271, 126.294, -4.02175, 0, 0, 'D8903A045BAC82500452872045A0B2B075186A5C9C7011D825F018C8DD224EC02A481F7478B65030E31264E88FE85850B94A08A056B6E9F0FE048CEC6136CD00', 'ReZusinho', 1608136925417),
(24, 0, 'test', '$2a$10$.khoV2cj3XQDU0tmPrsYoefaUroBvsXIenuP0VzkHFqJVKN9J64EW', '', 500, '2020-12-06 14:08:40', 243.254, -370.796, 44.3453, 176.781, 0, 0, 'D8903A045B0CF9D8A6CA0E6C753476B075F41EF80902CCA825F018C8DD223EE05BACC3E84D565A90E3120E38AEA66DC0574A08A056B6E970C7DEB2BCC28CD980', 'Rafistaa', 1608224439278),
(25, 0, 'Damian', '$2a$10$wWF4ti4BvOsYb1BDaT66d.XkYDkNZ66nklS8AZY.LHF7yyH6lsQbe', '', 500, '2020-12-09 16:42:07', 0, 0, 0, 0, 0, 0, 'D8903A045B0CF9D8A6CA0E6C753476B075F41EF80902CCA825F018C8DD223EE05BACC3E84D565A90E3120E38AEA66DC0574A08A056B6E970C7DEB2BCC28CD980', 'Rafistaa', NULL),
(26, 0, 'raf', '$2a$10$i1N9L9sAHXc63G1vx7.pTuzwmEzKKqRlcNC.bZuMT0QzMwp./87Xe', '', 500, '2020-12-10 21:43:46', 0, 0, 0, 0, 0, 0, 'D8903A045B0CF9D8A6CA0E6C753476B075F41EF80902CCA825F018C8DD223EE05BACC3E84D565A90E3120E38AEA66DC0574A08A056B6E970C7DEB2BCC28CD980', 'Rafistaa', NULL),
(27, 0, 'rafa', '$2a$10$29btZeJkXvesM.tky5eJyOHBBwu7xe0F91Ui1Yoone4hLE9u4eb7u', '', 500, '2020-12-10 21:44:09', 0, 0, 0, 0, 0, 0, 'D8903A045B0CF9D8A6CA0E6C753476B075F41EF80902CCA825F018C8DD223EE05BACC3E84D565A90E3120E38AEA66DC0574A08A056B6E970C7DEB2BCC28CD980', 'Rafistaa', NULL),
(28, 0, 'qwe', '$2a$10$EaLo6VEYRreqXvwkwuQLHeg3rR.fWpNExyci6ixw9/rEK0s0I6Znq', '', 500, '2020-12-10 21:47:58', -416.306, 1145.83, 325.857, 139.046, 0, 0, 'D8903A045B0CF9D8A6CA0E6C753476B075F41EF80902CCA825F018C8DD223EE05BACC3E84D565A90E3120E38AEA66DC0574A08A056B6E970C7DEB2BCC28CD980', 'Rafistaa', 1607728061869),
(29, 0, 'qwew', '$2a$10$5/Bmovp3WmOjj3B8ZUuzjuWNuESpvWggFTpG97o5W/clKoIUysEq2', '', 500, '2020-12-10 21:51:55', 0, 0, 0, 0, 0, 0, 'D8903A045B0CF9D8A6CA0E6C753476B075F41EF80902CCA825F018C8DD223EE05BACC3E84D565A90E3120E38AEA66DC0574A08A056B6E970C7DEB2BCC28CD980', 'Rafistaa', NULL),
(30, 0, '1234', '$2a$10$IshfDwS6D9rqU7VwSEKli.7S0n1bj8KnTR6XUatqZQuoz88m7Qg5C', '', 500, '2020-12-10 21:55:24', 0, 0, 0, 0, 0, 0, 'D8903A045B0CF9D8A6CA0E6C753476B075F41EF80902CCA825F018C8DD223EE05BACC3E84D565A90E3120E38AEA66DC0574A08A056B6E970C7DEB2BCC28CD980', 'Rafistaa', NULL),
(31, 0, '2222', '$2a$10$0AEHelqq0buekZmafVL3LuZI8u7dFlEKmq4a8G2Jz48.gBg6ggMxe', '', 500, '2020-12-10 21:57:53', 0, 0, 0, 0, 0, 0, 'D8903A045B0CF9D8A6CA0E6C753476B075F41EF80902CCA825F018C8DD223EE05BACC3E84D565A90E3120E38AEA66DC0574A08A056B6E970C7DEB2BCC28CD980', 'Rafistaa', NULL),
(32, 0, '22221', '$2a$10$OxMALndwAKmHXkOEgJcwcuab3VVcXdd5xDfafo7q0/Jkh1UVnlIOW', '', 500, '2020-12-10 21:58:00', 0, 0, 0, 0, 0, 0, 'D8903A045B0CF9D8A6CA0E6C753476B075F41EF80902CCA825F018C8DD223EE05BACC3E84D565A90E3120E38AEA66DC0574A08A056B6E970C7DEB2BCC28CD980', 'Rafistaa', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users_sessions`
--

CREATE TABLE `users_sessions` (
  `session_id` int(11) NOT NULL,
  `session_user_id` int(11) NOT NULL,
  `session_time_total` bigint(11) NOT NULL,
  `session_time_afk` int(11) NOT NULL,
  `session_money_earned` int(11) NOT NULL,
  `session_money_spend` int(11) NOT NULL,
  `session_time_end` bigint(20) NOT NULL,
  `session_vehicle_driver` float NOT NULL,
  `session_vehicle_passenger` float NOT NULL,
  `session_walking` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users_sessions`
--

INSERT INTO `users_sessions` (`session_id`, `session_user_id`, `session_time_total`, `session_time_afk`, `session_money_earned`, `session_money_spend`, `session_time_end`, `session_vehicle_driver`, `session_vehicle_passenger`, `session_walking`) VALUES
(6, 20, 20, 8, 0, 0, 1607456148087, 0, 0, 0.00478677),
(7, 20, 2, 0, 0, 0, 1607456185985, 0, 0, 0),
(8, 20, 24, 0, 0, 0, 1607457179430, 0, 0, 0.00710798),
(9, 20, 158, 0, 0, 0, 1607459177323, 0, 0, 2.11119),
(10, 1, 64, 0, 0, 0, 1607460449357, 0.00000102115, 0, 0.145746),
(11, 20, 57, 0, 0, 0, 1607461277729, 0, 0, 0.0517203),
(12, 20, 6, 0, 0, 0, 1607461289984, 0, 0, 0.00844267),
(13, 20, 6, 0, 0, 0, 1607466752816, 0, 0, 0.00383084),
(14, 20, 163, 0, 0, 0, 1607477169021, 0, 0, 0.178664),
(15, 20, 1155, 869, 0, 0, 1607479892708, 0, 0, 0.892554),
(16, 20, 247, 74, 0, 0, 1607522143653, 0, 0, 0.00890438),
(17, 20, 46, 0, 0, 0, 1607524152374, 0, 0, 0.0602141),
(18, 20, 156, 0, 0, 0, 1607524754425, 0.372129, 0, 0.192245),
(19, 20, 13, 0, 0, 0, 1607529254357, 0, 0, 0.00372628),
(20, 20, 136, 0, 0, 0, 1607529397835, 0, 0, 0.0094999),
(21, 1, 19, 0, 0, 0, 1607529424857, 0, 0, 0.0143333),
(22, 1, 40, 0, 0, 0, 1607529471094, 0, 0, 0.044546),
(23, 1, 2, 0, 0, 0, 1607529478842, 0, 0, 0),
(24, 20, 67, 1, 0, 0, 1607530105553, 0, 0, 0),
(25, 22, 19, 0, 0, 0, 1607530136301, 0, 0, 0.0863915),
(26, 22, 301, 175, 0, 0, 1607533857462, 0, 0, 0.722336),
(27, 20, 537, 330, 0, 0, 1607533859079, 0.0348624, 0, 2.72611),
(28, 20, 41, 9, 0, 0, 1607533973020, 0, 0, 0.0223758),
(29, 20, 204, 63, 0, 0, 1607534344989, 1.99824, 0, 1.67998),
(30, 20, 160, 46, 0, 0, 1607535329412, 0, 0, 0.0650982),
(31, 20, 10, 0, 0, 0, 1607538412623, 0, 0, 0.0120131),
(32, 20, 42, 28, 0, 0, 1607538468761, 0, 0, 0.000123174),
(33, 20, 5, 0, 0, 0, 1607538532084, 0, 0, 0.00585717),
(34, 20, 6, 0, 0, 0, 1607539230424, 0, 0, 0),
(35, 20, 0, 0, 0, 0, 1607539273980, 0, 0, 0),
(36, 20, 1, 0, 0, 0, 1607539363224, 0, 0, 0),
(37, 20, 4, 0, 0, 0, 1607539375085, 0, 0, 0),
(38, 20, 161, 152, 0, 0, 1607539546692, 0, 0, 0),
(39, 20, 5, 0, 0, 0, 1607548252663, 0, 0, 0.00302528),
(40, 20, 22, 13, 0, 0, 1607548339010, 0, 0, 0.00116889),
(41, 20, 33, 23, 0, 0, 1607556029235, 0, 0, 0.00789507),
(42, 20, 22, 6, 0, 0, 1607556057868, 0, 0, 0.0260652),
(43, 20, 73, 14, 0, 0, 1607556202946, 0.0848967, 0, 0.0436274),
(44, 20, 19, 6, 0, 0, 1607556579409, 0, 0, 0.00559623),
(45, 20, 10, 0, 0, 0, 1607556656825, 0, 0, 0.0287458),
(46, 20, 9, 0, 0, 0, 1607556722042, 0, 0, 0.0279575),
(47, 20, 30, 0, 0, 0, 1607556760802, 0, 0, 0.0768386),
(48, 22, 19, 0, 0, 0, 1607556892430, 0, 0, 0.0372623),
(49, 22, 13, 0, 0, 0, 1607556922775, 0, 0, 0.0433577),
(50, 20, 565, 95, 0, 0, 1607557429771, 1.14039, 0, 4.35105),
(51, 20, 6, 0, 0, 0, 1607559082829, 0, 0, 0.0158108),
(52, 20, 49, 16, 0, 0, 1607614248009, 0, 0, 0.0543373),
(53, 20, 257, 68, 0, 0, 1607616617165, 1.26995, 1.14076, 0.0884747),
(54, 20, 347, 123, 0, 0, 1607616971697, 0, 0, 0.64172),
(55, 22, 842, 23, 0, 0, 1607619545395, 5.5209, 0, 4.03656),
(56, 20, 57, 0, 0, 0, 1607620524716, 0.106956, 0, 0.140251),
(57, 20, 79, 35, 0, 0, 1607622553932, 0.229503, 0, 0.00792483),
(58, 20, 1, 0, 0, 0, 1607633267647, 0, 0, 0),
(59, 20, 14, 0, 0, 0, 1607634195166, 0, 0, 0.0225544),
(60, 1, 38, 0, 0, 0, 1607634281342, 0, 0, 0.126129),
(61, 20, 15, 0, 0, 0, 1607634306599, 0, 0, 0.0111827),
(62, 20, 15, 0, 0, 0, 1607647573152, 0, 0, 0.00380397),
(63, 20, 516, 136, 0, 0, 1607651427227, 0, 0, 2.30479),
(64, 20, 156, 51, 0, 0, 1607652082697, 0, 0, 2.30479),
(65, 20, 2, 0, 0, 0, 1607690724989, 0, 0, 0),
(66, 1, 15, 0, 0, 0, 1607694175672, 0, 0, 0.00680958),
(67, 20, 57, 21, 0, 0, 1607694764608, 0, 0, 0.00268475),
(68, 20, 6, 0, 0, 0, 1607696025041, 0, 0, 0),
(69, 20, 7, 0, 0, 0, 1607696038539, 0, 0, 0.00903966),
(70, 20, 24, 0, 0, 0, 1607727677241, 0, 0, 0.0654373),
(71, 28, 110, 45, 0, 0, 1607728061869, 0, 0, 0.0409695),
(72, 20, 1498, 714, 0, 0, 1607734937809, 0, 0, 2.30479),
(73, 1, 7, 0, 0, 0, 1607738430782, 0, 0, 0.00167964),
(74, 1, 5, 0, 0, 0, 1607739632304, 0, 0, 0.0107898),
(75, 1, 9, 0, 0, 0, 1607739652505, 0, 0, 0.0143395),
(76, 20, 23, 0, 0, 0, 1607739765228, 0, 0, 0.00913076),
(77, 1, 8, 0, 0, 0, 1607740929769, 0, 0, 0),
(78, 1, 210, 116, 0, 0, 1607741147183, 0, 0, 0.0651289),
(79, 20, 124, 26, 0, 0, 1607819654639, 0.432781, 0, 0.0933592),
(80, 1, 4, 0, 0, 0, 1607819665827, 0, 0, 0.00105699),
(81, 20, 15, 0, 0, 0, 1607884787348, 0, 0, 0.000997404),
(82, 20, 1279, 1167, 0, 0, 1607910246422, 0.647094, 0.000851746, 0.0677201),
(83, 20, 118, 0, 0, 0, 1607957285363, 0.529829, 0, 0.0274399),
(84, 20, 56, 0, 0, 0, 1607960525578, 0.0201784, 0, 0.112891),
(85, 20, 153, 16, 0, 0, 1607960686157, 0.828237, 0, 0.0737982),
(86, 20, 416, 121, 0, 0, 1607961149842, 4.41663, 0, 4.78903),
(87, 20, 183, 106, 0, 0, 1607961740024, 0, 0, 0.760432),
(88, 20, 426, 49, 0, 0, 1607962505560, 0.21892, 0.0040251, 30.915),
(89, 20, 598, 48, 0, 0, 1607983382649, 4.99267, 0.000538548, 2.25336),
(90, 20, 86, 0, 0, 0, 1607983475004, 1.69047, 0, 4.62181),
(91, 22, 7, 0, 0, 0, 1607983524754, 0, 0, 0.00887017),
(92, 20, 75, 0, 0, 0, 1607984279918, 0, 0, 0.231587),
(93, 20, 2, 0, 0, 0, 1608124620903, 0, 0, 0),
(94, 20, 4, 0, 0, 0, 1608125792032, 0, 0, 0.00680608),
(95, 20, 12, 0, 0, 0, 1608126882466, 0, 0, 0.0240495),
(96, 20, 19, 0, 0, 0, 1608127279145, 0, 0, 0.0991536),
(97, 20, 19, 0, 0, 0, 1608127304578, 0, 0, 0.117464),
(98, 20, 869, 832, 0, 0, 1608128306701, 0, 0, 0.862682),
(99, 20, 22, 0, 0, 0, 1608130464391, 0, 0, 0.00937602),
(100, 24, 3, 0, 0, 0, 1608130479036, 0, 0, 0.00074039),
(101, 20, 10, 0, 0, 0, 1608132062549, 0, 0, 0.016213),
(102, 24, 2, 0, 0, 0, 1608132071089, 0, 0, 0),
(103, 20, 656, 570, 0, 0, 1608134027032, 0, 0, 0.219251),
(104, 24, 22, 2, 0, 0, 1608134056062, 0, 0, 0.00402617),
(105, 20, 17, 8, 0, 0, 1608134864344, 0, 0, 0),
(106, 1, 61, 42, 0, 0, 1608135278393, 0, 0, 0),
(107, 20, 624, 76, 0, 0, 1608136492228, 7.52412, 0, 2.88433),
(108, 1, 823, 48, 0, 0, 1608136689588, 11.2081, 0, 8.1043),
(109, 22, 1055, 135, 0, 0, 1608136925417, 20.6667, 0, 9.61173),
(110, 20, 193, 51, 0, 0, 1608157667633, 0.0712954, 0.00149474, 0.136579),
(111, 20, 146, 0, 0, 0, 1608213148899, 2.81932, 0, 0.0289674),
(112, 20, 93, 0, 0, 0, 1608220928457, 0.329593, 0, 0.0811391),
(113, 20, 56, 0, 0, 0, 1608222353043, 0.0563291, 0, 0.0177203),
(114, 20, 3, 0, 0, 0, 1608223602437, 0, 0, 0),
(115, 20, 2, 0, 0, 0, 1608223661491, 0, 0, 0),
(116, 20, 52, 0, 0, 0, 1608223830105, 0.00481948, 0, 0.0525657),
(117, 20, 84, 0, 0, 0, 1608224415975, 0.373102, 0, 0.0855696),
(118, 24, 16, 0, 0, 0, 1608224439278, 0, 0, 0.0486007),
(119, 20, 64, 0, 0, 0, 1608224612095, 0.152863, 0, 0.0370989),
(120, 20, 42, 0, 0, 0, 1608224884214, 0.0439518, 0, 0.0215087),
(121, 20, 38, 0, 0, 0, 1608225192524, 0.0618741, 0, 0.0476904),
(122, 20, 93, 0, 0, 0, 1608225663207, 0.926133, 0, 0.0212475),
(123, 20, 61, 0, 0, 0, 1608227754321, 0.0538318, 0, 0.0732829),
(124, 20, 38, 0, 0, 0, 1608229052316, 0.230991, 0, 0.0176067),
(125, 20, 274, 0, 0, 0, 1608236352824, 2.52889, 0, 0.496017),
(126, 20, 105, 0, 0, 0, 1608282165585, 0.163257, 0, 0.0490385);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_inventory`
--

CREATE TABLE `user_inventory` (
  `inventory_user_id` int(11) NOT NULL,
  `inventory_water` mediumtext COLLATE utf8_polish_ci NOT NULL DEFAULT '1x1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `user_inventory`
--

INSERT INTO `user_inventory` (`inventory_user_id`, `inventory_water`) VALUES
(20, '1x1'),
(22, '1x1');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `vehicles`
--

CREATE TABLE `vehicles` (
  `vehicle_id` int(11) NOT NULL,
  `vehicle_owner` int(11) NOT NULL,
  `vehicle_model` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `vehicle_engine_version` int(11) NOT NULL,
  `vehicle_mileage` float NOT NULL DEFAULT 0,
  `vehicle_fuel` float NOT NULL DEFAULT 10,
  `vehicle_posX` float NOT NULL,
  `vehicle_posY` float NOT NULL,
  `vehicle_posZ` float NOT NULL,
  `vehicle_rotX` float NOT NULL,
  `vehicle_rotY` float NOT NULL,
  `vehicle_rotZ` float NOT NULL,
  `vehicle_heading` float NOT NULL,
  `vehicle_spawned` tinyint(1) NOT NULL DEFAULT 0,
  `vehicle_dirt` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `vehicles`
--

INSERT INTO `vehicles` (`vehicle_id`, `vehicle_owner`, `vehicle_model`, `vehicle_engine_version`, `vehicle_mileage`, `vehicle_fuel`, `vehicle_posX`, `vehicle_posY`, `vehicle_posZ`, `vehicle_rotX`, `vehicle_rotY`, `vehicle_rotZ`, `vehicle_heading`, `vehicle_spawned`, `vehicle_dirt`) VALUES
(1, 20, 'vigero', 0, 104.136, 0, 238.374, -370.968, 43.9316, 0.931662, 2.28627, -104.621, 255.333, 1, 0),
(2, 20, 'rocoto', 0, 1.1789, 9.52844, 249.545, -374.764, 44.2556, 0.905531, 1.43929, -109.107, 250.87, 1, 0),
(3, 20, 'nightshade', 0, 0.0200352, 9.99199, 256.103, -377.18, 43.9351, 1.22622, 1.58563, -110.802, 249.183, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `vehicles_damage`
--

CREATE TABLE `vehicles_damage` (
  `vehicle_damage_id` int(11) NOT NULL,
  `vehicle_door0` int(1) NOT NULL DEFAULT 0,
  `vehicle_door1` int(1) NOT NULL DEFAULT 0,
  `vehicle_door2` int(1) NOT NULL DEFAULT 0,
  `vehicle_door3` int(1) NOT NULL DEFAULT 0,
  `vehicle_door4` int(1) NOT NULL DEFAULT 0,
  `vehicle_door5` int(1) NOT NULL DEFAULT 0,
  `vehicle_window0` int(11) NOT NULL DEFAULT 0,
  `vehicle_window1` int(11) NOT NULL DEFAULT 0,
  `vehicle_window2` int(11) NOT NULL DEFAULT 0,
  `vehicle_window3` int(11) NOT NULL DEFAULT 0,
  `vehicle_window4` int(11) NOT NULL DEFAULT 0,
  `vehicle_window5` int(11) NOT NULL DEFAULT 0,
  `vehicle_window6` int(11) NOT NULL DEFAULT 0,
  `vehicle_window7` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `vehicles_damage`
--

INSERT INTO `vehicles_damage` (`vehicle_damage_id`, `vehicle_door0`, `vehicle_door1`, `vehicle_door2`, `vehicle_door3`, `vehicle_door4`, `vehicle_door5`, `vehicle_window0`, `vehicle_window1`, `vehicle_window2`, `vehicle_window3`, `vehicle_window4`, `vehicle_window5`, `vehicle_window6`, `vehicle_window7`) VALUES
(1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `vehicles_tunning`
--

CREATE TABLE `vehicles_tunning` (
  `vehicle_tunning_id` int(11) NOT NULL,
  `vehicle_turbo` int(11) NOT NULL DEFAULT -1,
  `vehicle_xenon` int(11) NOT NULL DEFAULT -1,
  `vehicle_window` int(11) NOT NULL DEFAULT 0,
  `vehicle_horn` int(11) NOT NULL DEFAULT -1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `vehicles_tunning`
--

INSERT INTO `vehicles_tunning` (`vehicle_tunning_id`, `vehicle_turbo`, `vehicle_xenon`, `vehicle_window`, `vehicle_horn`) VALUES
(1, 1, 1, 2, -1),
(2, -1, -1, 0, -1),
(3, -1, -1, 0, -1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `action_history`
--
ALTER TABLE `action_history`
  ADD PRIMARY KEY (`action_id`);

--
-- Indeksy dla tabeli `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`ann_id`);

--
-- Indeksy dla tabeli `appearances`
--
ALTER TABLE `appearances`
  ADD UNIQUE KEY `appearance_user_id` (`appearance_user_id`);

--
-- Indeksy dla tabeli `bans`
--
ALTER TABLE `bans`
  ADD PRIMARY KEY (`ban_id`);

--
-- Indeksy dla tabeli `mutes`
--
ALTER TABLE `mutes`
  ADD UNIQUE KEY `user_id` (`mute_user_id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indeksy dla tabeli `users_sessions`
--
ALTER TABLE `users_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indeksy dla tabeli `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD UNIQUE KEY `inventory_user_id` (`inventory_user_id`);

--
-- Indeksy dla tabeli `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`vehicle_id`);

--
-- Indeksy dla tabeli `vehicles_damage`
--
ALTER TABLE `vehicles_damage`
  ADD UNIQUE KEY `vehicle_damage_id` (`vehicle_damage_id`);

--
-- Indeksy dla tabeli `vehicles_tunning`
--
ALTER TABLE `vehicles_tunning`
  ADD UNIQUE KEY `vehicle_tunning_id` (`vehicle_tunning_id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `action_history`
--
ALTER TABLE `action_history`
  MODIFY `action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT dla tabeli `announcements`
--
ALTER TABLE `announcements`
  MODIFY `ann_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `bans`
--
ALTER TABLE `bans`
  MODIFY `ban_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25035;

--
-- AUTO_INCREMENT dla tabeli `users_sessions`
--
ALTER TABLE `users_sessions`
  MODIFY `session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT dla tabeli `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `vehicle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
