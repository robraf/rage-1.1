var async = require('async');
const KICK_TIME = 20;
const UTC_TIME = 60*60*1000;

var SERVER_READY = false;

mp.events.add('packagesLoaded', () =>
{
  logoutAllPlayers();
  mp.events.call("call::spawnVehicles");
});

async function logoutAllPlayers() {
    var start = Date.now();
    var respond = await GlobalFunctions.mysql.update("users", "user_is_loggon = ?", "user_is_loggon = 1", [0]);
  
    if(respond!=null && respond!=undefined){
        var end = Date.now();
        console.log("Wylogowano "+respond.affectedRows+ "użytkowników. Czas: "+parseInt((end-start)/1000)+"s.");
    }else{
        console.log("***NIE UDAŁO SIĘ WYLOGOWAĆ UŻYTKOWNIKÓW***")
    }
}

//timer
setTimeout(function(){
    SERVER_READY=true;
    console.log("SERWER GOTOWY")
    mp.players.forEach((player, id) => {
        if(player.getVariable("playerData") == null){
            player.call("callClient::serverIsReady", [SERVER_READY]);
        }
    })
}, 5*1000)


/*
SERWER READY
tutaj odbierac w przyszlosci gotowosc od roznych komponentów
1) spawn vehicle
*/


/*sprawdzanie czy przy dołączaniu gracz ma bana, jeśli nie dołącza do serwera
*/
mp.events.add("playerReady", (player) => {
    GlobalFunctions.mysql.handle.query('SELECT * FROM `bans` WHERE ban_serial = ? OR ban_socialclub = ?', [player.serial, player.socialClub], function (error, result, row) {
        if (error) console.log(error);
        if (result.length > 0) {
            expirationDate = new Date(result[0]["ban_expiration_date"]);
            var banId = result[0]["ban_id"];
            dateNow = Date.now();
            var diffrenceTime = (dateNow - expirationDate) / 1000; //seconds
            var date = new Date((result[0]["ban_expiration_date"]+UTC_TIME)).toISOString().split('T')[0] + " " + (new Date((result[0]["ban_expiration_date"]+UTC_TIME)).toISOString().split('T')[1]).split('.')[0];

            if (diffrenceTime >= 0) { //jesli ban się przedawnił       
                GlobalFunctions.mysql.delete("bans", "ban_id = ?", [banId]); //usuwanie go z bazy danych
                GlobalFunctions.mysql.update("users", "user_banned = ?", "user_id = "+result[0]["user_id"],[false]); 
                showLoginPanel(player);
            } else { //wyswietlanie infromacji graczowi
                player.call("callClient::playerHasActiveBan", [date, result[0]["ban_reason"], KICK_TIME]);
                setTimeout(function () {
                    player.kick("Masz aktywnego bana do "+date+" za: "+result[0]["ban_reason"]);
                }, KICK_TIME * 1000)
            }

        } else { //gracz nie ma bana
            showLoginPanel(player);
        }
    });
});

function showLoginPanel(player){
    player.call("callClient::createLoginPanelBrowser", [SERVER_READY]);
}


//SERVER SHUTDOWN
mp.events.add("serverShutdown", async () =>
{
  mp.events.delayShutdown = true;
  await shutdownServer();
  mp.events.delayShutdown = false;
});

async function shutdownServer(){
    var respond = await GlobalFunctions.mysql.update("users", "user_is_loggon = ?", "user_is_loggon = 1", [0]);
}
