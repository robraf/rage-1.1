module.exports =
{
    chatToAll: function (playerMessage, textMessage) {
        var playerData = playerMessage.getVariable("playerData");
        if (playerData != null) {
            var messageChat = {
                type: "chat",
                chatType: "Globalny",
                nick: playerData.nick,
                playerType: playerData.admin,
                playerId: playerData.id,
                text: textMessage
            };
            GlobalFunctions.log.add("chat", "(globalny) " + playerData.nick + ": " + textMessage);
            mp.players.forEach(
                (player, id) => {
                    if (player.getVariable("playerData")) {
                        if (player.getVariable("playerData").spawned) {
                            player.call("callClient::sendMessageToChat", [JSON.stringify(messageChat)]);
                        }
                    }
                }
            );
        }
    },

    chatToLocal: function (playerMessage, textMessage) {
        var playerData = playerMessage.getVariable("playerData");
        if (playerData != null) {
            var messageChat = {
                type: "chat",
                chatType: "",
                nick: playerData.nick,
                playerType: playerData.admin,
                playerId: playerData.id,
                text: textMessage
            };
            GlobalFunctions.log.add("chat", "(lokalny) " + playerData.nick + ": " + textMessage);

            mp.players.forEach((player, id) => {
                if (player.getVariable("playerData") !== null && player.getVariable("playerData") !== undefined) {
                    if (player.getVariable("playerData").spawned) {
                        if (playerMessage.dist(player.position) <= 80) {
                            player.call("callClient::sendMessageToChat", [JSON.stringify(messageChat)]);
                        }
                    }
                }
            });
        }
    },

    chatToAdmin: function (playerMessage, textMessage) {
        var playerData = playerMessage.getVariable("playerData");
        if (playerData != null) {
            var messageChat = {
                type: "chat",
                chatType: "Admin",
                nick: playerData.nick,
                playerType: playerData.admin,
                playerId: playerData.id,
                text: textMessage
            };
            GlobalFunctions.log.add("chat", "(admin) " + playerData.nick + ": " + textMessage);

            mp.players.forEach((player, id) => {
                if (player.getVariable("playerData") !== null && player.getVariable("playerData") !== undefined) {
                    if (player.getVariable("playerData").spawned) {
                        if (player.getVariable("playerData").admin != "") {
                            player.call("callClient::sendMessageToChat", [JSON.stringify(messageChat)]);
                        }
                    }
                }
            });
        }
    },

    announcementToAll: function (messageAnnouncement) {
        mp.players.forEach(
            (player, id) => {
                if (player.getVariable("playerData") !== null && player.getVariable("playerData") !== undefined) {
                    if (player.getVariable("playerData").spawned) {
                        player.call("callClient::sendMessageToChat", [JSON.stringify(messageAnnouncement)]);
                    }
                }
            }
        );
    },

    toPlayer: function (player, message, colorMessage) {
        var playerData = player.getVariable("playerData");
        if (playerData != null) {
            var messageChat = {
                type: "player",
                text: message,
                color: colorMessage
            };
            player.call("callClient::sendMessageToChat", [JSON.stringify(messageChat)]);
        }
    }
}

// function chatMessage(playerMessage, textMessage) {
//     if (!isPlayerMuted(playerMessage)) {
//         GlobalFunctions.chat.chatToLocal(playerMessage, textMessage);
//     }
// };
// mp.events.add("playerChat", chatMessage);

mp.events.add("callServer::playerSendMessage", (player, chat, message) => {
    if (chat === "global") { //czat globalny
        if (!isPlayerMuted(player) && message!="") {
            GlobalFunctions.chat.chatToAll(player, message);
        }
    } else if (chat === "admin" && message!="") { //czat adminowski
        if (!isPlayerMuted(player)) {
            var playerData = player.getVariable("playerData");
            if (playerData != null) {
                if (playerData.admin != "") {
                    GlobalFunctions.chat.chatToAdmin(player, message);
                }
            }
        }
    } else if (chat === "local" && message!="") { //czat lokalny
        if (!isPlayerMuted(player)) {
            GlobalFunctions.chat.chatToLocal(player, message);
        }
    }
});


function messageToPlayer(player, message, color) {
    GlobalFunctions.chat.toPlayer(player, message, color);
};
mp.events.add("callServer::sendMessageToPlayer", messageToPlayer);
mp.events.add("call::sendMessageToPlayer", messageToPlayer);

function playerUsingChat(player, value) {
    player.setVariable("playerChatting", value);
}
mp.events.add("callServer::playerUseChat", playerUsingChat);

function isPlayerMuted(playerMessage) {
    var playerMuted = playerMessage.getVariable("playerMuted");
    var timeNow = Date.now();
    if (!playerMuted) {
        return false;
    } else {
        playerMuted = parseInt(playerMuted);
        if ((timeNow - playerMuted) >= 0) {
            playerMessage.setVariable("playerMuted", null);
            GlobalFunctions.mysql.delete("mutes", "mute_user_id = ?", [playerMessage.getVariable("playerData").id]);
            return false;
        } else {
            playerMessage.call("callClient::addNotify", ["Czat", "Masz nałożoną blokadę czatu!", "red", null]);
            return true;
        }
    }
}