var async = require('async');

let fuelStationList = require('..//json/fuel_station.json').FuelStation;

var fuelStationTimeout = null;

var fuelStationsData = [];
var colshapesList = [];
var labelList = [];

setFuelStationData();
async function setFuelStationData() {
    resetElements();
    fuelStationsData = [];
    //przeleciec bazę MYSQL 
    var getFuelStations = await GlobalFunctions.mysql.select("fuel_stations", "", [])
    if (getFuelStations !== null && getFuelStations !== undefined) {
        if (getFuelStations.length > 0) {
            for (var i = 0; i < getFuelStations.length; i++) {
                var fuelStationId = getFuelStations[i]["fuel_station_id"];
                // console.log("ID: "+fuelStationId)
                var findFuelInJson = findFuelStationById(fuelStationId);
                if (findFuelInJson !== null && findFuelInJson !== undefined && findFuelInJson.length !== 0) {
                    //stacja istnieje

                    findFuelInJson = findFuelInJson[0];
                    let fuelStationObject = {
                        id: findFuelInJson.id,
                        name: findFuelInJson.name,
                        position: new mp.Vector3(findFuelInJson.blip_posX, findFuelInJson.blip_posY, findFuelInJson.blip_posZ),
                        pricePB: (getRandomFloat(findFuelInJson.price_PB[0], findFuelInJson.price_PB[1])).toFixed(2),
                        priceON: (getRandomFloat(findFuelInJson.price_ON[0], findFuelInJson.price_ON[1])).toFixed(2),
                        priceLPG: (getRandomFloat(findFuelInJson.price_LPG[0], findFuelInJson.price_LPG[1])).toFixed(2),
                        amountPB: getFuelStations[i]["fuel_station_pb_amount"],
                        amountON: getFuelStations[i]["fuel_station_on_amount"],
                        amountLPG: getFuelStations[i]["fuel_station_lpg_amount"],
                        distributors: findFuelInJson.distributors,
                    }
                    
                    fuelStationsData.push(fuelStationObject);
                    //console.log(fuelStationObject)
                    mp.blips.new(361, fuelStationObject.position,
                        {
                            name: 'Stacja benzynowa',
                            color: 73,
                            shortRange: true,
                            scale: 0.5,
                            dimension: 0,
                            alpha: 220
                        });
                    for (var j = 0; j < (findFuelInJson.distributors).length; j++) {
                        var distributor = findFuelInJson.distributors[j];
                        let sphere = mp.colshapes.newSphere(distributor.posX, distributor.posY, distributor.posZ, 2, 0);
                        let distributorObj = {
                            stationId: findFuelInJson.id,
                            distributorId: j,
                            types: distributor.fuel_types,
                            amountON: getFuelStations[i]["fuel_station_on_amount"],
                            amountPB: getFuelStations[i]["fuel_station_pb_amount"],
                            amountLPG: getFuelStations[i]["fuel_station_lpg_amount"],
                            priceON: fuelStationObject.priceON,
                            pricePB: fuelStationObject.pricePB,
                            priceLPG: fuelStationObject.priceLPG
                        }
                        sphere.setVariable("distributorData", distributorObj)
                        colshapesList.push(sphere);
                        var fuelTypes = (distributor.fuel_types).toString();
                        let label = mp.labels.new("Dystrybutor "+(j+1)+"\n"+fuelTypes, new mp.Vector3(distributor.posX, distributor.posY, distributor.posZ),
                            {
                                los: true,
                                font: 4,
                                drawDistance: 40,
                                color: [255, 255, 255, 255],
                                dimension: 0
                            });
                        labelList.push(label);
                    }

                }
            }
        }
    }

    mp.players.forEach((player, id) => {
        player.call("callClient::updatePlayerFuelStationList", [JSON.stringify(fuelStationsData)]);
    });

    fuelStationTimeout = clearTimeout();
    let nextUpdateTime = getRandomFloat(30, 45);
    fuelStationTimeout = setTimeout(setFuelStationData, nextUpdateTime * 1000)
}

//graczowi wysylac liste stacji benzynowych przy kazdym zalogowaniu 
mp.events.add('playerJoin', (player) => {
    player.call("callClient::updatePlayerFuelStationList", [JSON.stringify(fuelStationsData)]);
});

//graczowi wysylac liste stacji benzynowych po kazdym zatankowaniu innego gracza i pryz zmienieniu ceny paliw

function resetElements(){
    for(var i=0;i<colshapesList.length;i++){
        colshapesList[i].destroy();
    }
    colshapesList=[];

    for(var i=0;i<labelList.length;i++){
        labelList[i].destroy();
    }
    labelList=[];
}

function findFuelStationById(id) {
    return fuelStationList.filter(
        function (fuelStationList) { return fuelStationList.id == id }
    );
}

function getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}