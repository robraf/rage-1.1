var async = require('async');
var lastPlayerDisconncted = [];
const UTC_TIME = 0;

async function onAdminPanelAction(source, event, actionPlayerId, actionReason, actionTime) {
    switch (event) {
        case "mute_player":
            var playerId = actionPlayerId;
            var reason = actionReason;
            var time = (parseInt(actionTime) * 60 * 1000);
            var isUserOnline = false;

            GlobalFunctions.mysql.handle.query('SELECT * FROM `mutes` WHERE mute_user_id = ?', [playerId], async function (error, result, row) {
                if (error) {
                    console.log(error);
                    GlobalFunctions.log.add("error", "Mutes SQL: " + error);
                }
                var isMuteActive = false;
                if (result.length != 0) { //gracz ma aktywny mute
                    if ((Date.now() - result[0]["mute_expires"]) >= 0) {
                        GlobalFunctions.mysql.delete("mutes", "mute_user_id = ?", [playerId]);
                        isMuteActive = false;
                    } else {
                        isMuteActive = true;
                    }

                }
                if (!isMuteActive) {
                    mp.players.forEach(async function (player, id) {
                        var playerData = player.getVariable("playerData");
                        if (playerData!== null && playerData!==undefined) {
                            if (playerData.id == playerId) {
                                var timeNow = Date.now();
                                var isMuteAddedToDb = await GlobalFunctions.mysql.insert("mutes", "(mute_user_id, mute_reason, mute_expires, mute_admin) VALUES(?,?,?,?)", [parseInt(playerId), reason, (timeNow + time + UTC_TIME), source.getVariable("playerData").id]);
                                if (isMuteAddedToDb != null && isMuteAddedToDb.affectedRows == 1) {
                                    var isHistoryAddedToDb = await GlobalFunctions.mysql.insert("action_history", "(action_type, action_user_id, action_reason, action_who, action_length) VALUES(?,?,?,?,?)", ["mute", playerData.id, reason, source.getVariable("playerData").id, parseInt(time / 60000)]);
                                    if (isHistoryAddedToDb != null && isHistoryAddedToDb.affectedRows == 1) {
                                        var messageMute = {
                                            type: "announcement",
                                            header: "MUTE",
                                            text: "Gracz " + playerData.nick + " został wyciszony na czacie za<b>" + reason + "</b> przez " + source.getVariable("playerData").nick + " (" + time / 60000 + "min)",
                                            color: "red"
                                        };
                                        isUserOnline = true;

                                        var timeMute = timeNow + time + UTC_TIME;
                                        player.setVariable("playerMuted", "" + timeMute);
                                        player.call("callClient::addedMute", [timeNow]);
                                        GlobalFunctions.chat.announcementToAll(messageMute);
                                        return;
                                    } else {
                                        mp.events.call("call::sendMessageToPlayer", source, "Wystąpił błąd połączenia z bazą danych i gracz nie otrzymał mute.", "red");
                                    }
                                } else {
                                    mp.events.call("call::sendMessageToPlayer", source, "Wystąpił błąd połączenia z bazą danych i gracz nie otrzymał mute.", "red");
                                }
                            }
                        }
                    });
                    if (!isUserOnline) { //gracza nie ma już online
                        var lastPlayer = null;
                        for (var i = 0; i < lastPlayerDisconncted.length; i++) {
                            if (lastPlayerDisconncted[i].id == playerId) {
                                lastPlayer = lastPlayerDisconncted[i];
                                break;
                            }
                        }
                        if (lastPlayer != null) {
                            var isMuteAddedToDb = await GlobalFunctions.mysql.insert("mutes", "(mute_user_id, mute_reason, mute_expires, mute_admin) VALUES(?,?,?,?)", [lastPlayer.id, reason, (Date.now() + time + UTC_TIME), source.getVariable("playerData").id]);
                            if (isMuteAddedToDb != null && isMuteAddedToDb.affectedRows == 1) {
                                var isHistoryAddedToDb = await GlobalFunctions.mysql.insert("action_history", "(action_type, action_user_id, action_reason, action_who, action_length) VALUES(?,?,?,?,?)", ["mute", playerId, reason, source.getVariable("playerData").id, parseInt(time / 60000)]);
                                if (isHistoryAddedToDb != null && isHistoryAddedToDb.affectedRows == 1) {
                                    mp.events.call("call::sendMessageToPlayer", source, "Gracz otrzymał mute.", "green");
                                    var messageMute = {
                                        type: "announcement",
                                        header: "MUTE",
                                        text: "Gracz " + lastPlayer.nick + " został wyciszony na czacie za<b>" + reason + "</b> przez " + source.getVariable("playerData").nick + " (" + time / 60000 + "min)",
                                        color: "red"
                                    };
                                    GlobalFunctions.chat.announcementToAll(messageMute);
                                } else {
                                    mp.events.call("call::sendMessageToPlayer", source, "Wystąpił błąd połączenia z bazą danych i gracz nie otrzymał mute.", "red");
                                }
                            } else {
                                mp.events.call("call::sendMessageToPlayer", source, "Wystąpił błąd połączenia z bazą danych i gracz nie otrzymał mute.", "red");
                            }
                        } else {
                            var isMuteAddedToDb = await GlobalFunctions.mysql.insert("mutes", "(mute_user_id, mute_reason, mute_expires, mute_admin) VALUES(?,?,?,?)", [playerId, reason, (Date.now() + time + UTC_TIME), source.getVariable("playerData").id]);
                            if (isMuteAddedToDb != null && isMuteAddedToDb.affectedRows == 1) {
                                var isHistoryAddedToDb = await GlobalFunctions.mysql.insert("action_history", "(action_type, action_user_id, action_reason, action_who, action_length) VALUES(?,?,?,?,?)", ["mute", playerId, reason, source.getVariable("playerData").id, parseInt(time / 60000)]);
                                if (isHistoryAddedToDb != null && isHistoryAddedToDb.affectedRows == 1) {
                                    mp.events.call("call::sendMessageToPlayer", source, "Gracza nie ma już na serwerze, natomiast mute został dodany", "orange");
                                } else {
                                    mp.events.call("call::sendMessageToPlayer", source, "Wystąpił błąd połączenia z bazą danych i gracz nie otrzymał mute.", "red");
                                }
                            }
                        }
                    }
                } else { //gracz ma aktywny mute
                    mp.events.call("call::sendMessageToPlayer", source, "Nie dodano MUTE, ponieważ ten gracz ma już aktywną blokadę.", "red");
                }
            });
            break;
        case "kick_player":
            var playerId = actionPlayerId;
            var reason = actionReason;
            mp.players.forEach(async function (player, id) {
                var playerData = player.getVariable("playerData");
                if (playerData) {
                    if (playerData.id == playerId) {
                        var messageKick = {
                            type: "announcement",
                            header: "KICK",
                            text: "Gracz " + playerData.nick + " został wyrzucony z serwera za '<b>" + reason + "</b>' przez " + source.getVariable("playerData").nick,
                            color: "red"
                        };
                        GlobalFunctions.mysql.insert("action_history", "(action_type, action_user_id, action_reason, action_who) VALUES(?,?,?,?)", ["kick", playerData.id, reason, source.getVariable("playerData").id]);
                        GlobalFunctions.chat.announcementToAll(messageKick);
                        setTimeout(function () {
                            player.kickSilent();
                        }, 2000)
                        return;
                    }
                }
            });
            break;
        case "ban_player":
            var playerId = actionPlayerId;
            var reason = actionReason;
            var time = (parseInt(actionTime) * 60 * 1000);
            var isUserOnline = false;

            GlobalFunctions.mysql.handle.query('SELECT * FROM `users` WHERE user_id = ? AND user_banned = ?', [playerId, true], function (error, result, row) {
                if (error) {
                    console.log(error);
                    GlobalFunctions.log.add("error", "Mutes SQL: " + error);
                }
                if (result.length != 0) { //gracz ma aktywny ban
                    mp.events.call("call::sendMessageToPlayer", source, "Konto o ID: " + playerId + " jest już zbanowane!", "red");
                } else {
                    mp.players.forEach(async function (player, id) {
                        var playerData = player.getVariable("playerData");
                        if (playerData) {
                            if (playerData.id == playerId) {
                                var messageBan = {
                                    type: "announcement",
                                    header: "BAN",
                                    text: "Gracz " + playerData.nick + " został zbanowany na serwerze za '<b>" + reason + "</b>' przez " + source.getVariable("playerData").nick + " (" + time / 60000 + "min)",
                                    color: "red"
                                };
                                isUserOnline = true;
                                GlobalFunctions.mysql.insert("action_history", "(action_type, action_user_id, action_reason, action_who, action_length) VALUES(?,?,?,?,?)", ["ban", playerData.id, reason, source.getVariable("playerData").id, parseInt(time / 60000)]);
                                var isBanAddedToDb = await GlobalFunctions.mysql.insert("bans", "(ban_serial, ban_socialclub, ban_reason, ban_admin, ban_expiration_date, user_id) VALUES(?,?,?,?,?,?)", [player.serial, player.socialClub, reason, source.getVariable("playerData").id, (Date.now() + time + UTC_TIME), playerId]);
                                if (isBanAddedToDb != null && isBanAddedToDb.affectedRows == 1) {
                                    var isBanUserAddedToDb = await GlobalFunctions.mysql.update("users", "user_banned = ?", "user_id = " + playerId, [true]);
                                    if (isBanUserAddedToDb != null && isBanUserAddedToDb == 1) {
                                        GlobalFunctions.chat.announcementToAll(messageBan);
                                        setTimeout(function () {
                                            player.kickSilent();
                                        }, 2000)
                                        return;
                                    } else {
                                        player.kickSilent();
                                    }
                                } else {                     
                                    player.kickSilent();
                                }
                            }
                        }
                    });
                    if (!isUserOnline) { //gracza nie ma już online
                        var lastPlayer = null;
                        (async function(){
                        for await(var itemPlayer of lastPlayerDisconncted) {
                            if (itemPlayer.id == playerId) {
                                lastPlayer = itemPlayer;
                                break;
                            }
                        }
                        if (lastPlayer != null) { //gracza jest w 10 ostatnich wyjsciach
                            var isBanAddedToDb = await GlobalFunctions.mysql.insert("bans", "(ban_serial, ban_socialclub, ban_reason, ban_admin, ban_expiration_date, user_id) VALUES(?,?,?,?,?,?)", [lastPlayer.serial, lastPlayer.socialClub, reason, source.getVariable("playerData").id, (Date.now() + time + UTC_TIME), playerId]);
                            if (isBanAddedToDb != null && isBanAddedToDb.affectedRows == 1) {
                                mp.events.call("call::sendMessageToPlayer", source, "Gracz otrzymał bana na serial i socialclub.", "green");
                                var messageBan = {
                                    type: "announcement",
                                    header: "BAN",
                                    text: "Gracz " + lastPlayer.nick + " został zbanowany na serwerze za '<b>" + reason + "</b>' przez " + source.getVariable("playerData").nick + " (" + time / 60000 + "min)",
                                    color: "red"
                                };
                                GlobalFunctions.chat.announcementToAll(messageBan);
                            } 
                        } else {
                            mp.events.call("call::sendMessageToPlayer", source, "Gracza nie ma na serwerze, konto zablokowane.", "orange");
                        }
                        var isBanUserAddedToDb = await GlobalFunctions.mysql.update("users", "user_banned = ?", "user_id = " + playerId, [true]);
                        if (isBanUserAddedToDb != null && isBanUserAddedToDb == 1) {
                            mp.events.call("call::sendMessageToPlayer", source, "Gracz otrzymał bana na konto", "green");
                        } 
                        GlobalFunctions.mysql.insert("action_history", "(action_type, action_user_id, action_reason, action_who, action_length) VALUES(?,?,?,?,?)", ["ban", playerId, reason, source.getVariable("playerData").id, parseInt(time / 60000)]);
                        })();
                        
                    }
                }
            });
            break;
        case "get_last_players":
            source.call("callClient::onAdminPanelAction", ["update_last_players", JSON.stringify(lastPlayerDisconncted)]);
            break;
        case "get_user_by_id":
            var playerId = actionPlayerId;
            var isPlayerOnline = false;
            mp.players.forEach(async function (player, id) {
                var playerData = player.getVariable("playerData");
                if (playerData !== null && playerData !== undefined) {
                    if (playerData.id == playerId) {
                        isPlayerOnline = true;
                        var actionHistoryList = [];
                        GlobalFunctions.mysql.handle.query('SELECT * FROM `action_history` WHERE action_user_id = ?', [playerId], function (error, result, row) {
                            if (error) console.log(error);
                            if (result.length > 0) {
                                for (var i = 0; i < result.length; i++) {
                                    var historyObj = {
                                        id: result[i]["action_id"],
                                        type: result[i]["action_type"],
                                        reason: result[i]["action_reasone"],
                                        time: result[i]["action_time"],
                                        who: "rafista" //DO ZROBIENIA
                                    }
                                    actionHistoryList.push(historyObj);
                                }
                            }
                            var playerObj = {
                                online: true,
                                serial: player.serial,
                                socialClub: player.socialClub,
                                history: actionHistoryList
                            }
                            source.call("callClient::onAdminPanelAction", ["get_user_by_id", JSON.stringify(playerObj)]); //serial, ip, historia kar
                        });
                        return;
                    }
                }
            });
            if (!isPlayerOnline) {
                GlobalFunctions.mysql.handle.query('SELECT * FROM `action_history` WHERE action_user_id = ?', [playerId], function (error, result, row) {
                    if (error) console.log(error);
                    if (result.length > 0) {
                        for (var i = 0; i < result.length; i++) {
                            var historyObj = {
                                id: result[i]["action_id"],
                                type: result[i]["action_type"],
                                reason: result[i]["action_reasone"],
                                time: result[i]["action_time"],
                                who: "rafista" //DO ZROBIENIA
                            }
                            actionHistoryList.push(historyObj);
                        }
                    }
                    var playerObj = {
                        online: true,
                        serial: player.serial,
                        socialClub: player.socialClub,
                        history: actionHistoryList
                    }
                    source.call("callClient::onAdminPanelAction", ["get_user_by_id", JSON.stringify(playerObj)]); //serial, ip, historia kar
                });
            }
            break;
        default:
            break;
    }
}
mp.events.add("callServer::onAdminPanelAction", onAdminPanelAction);

//last 10 players quit
function playerQuitHandler(player, exitType, reason) {
    var playerData = player.getVariable("playerData");
    if (playerData) {
        var time = Date.now();

        var playerExistInList = -1;
        for (var i = 0; i < lastPlayerDisconncted.length; i++) {
            if (lastPlayerDisconncted[i].id == playerData.id) {
                playerExistInList = i;
                break;
            }
        }

        if (lastPlayerDisconncted.length == 10) {
            lastPlayerDisconncted.splice(0, 1);
        }
        var playerObj = {
            nick: playerData.nick,
            id: playerData.id,
            timeQuit: time,
            quitType: exitType,
            serial: player.serial,
            socialClub: player.socialClub,
            ip: player.ip
        }
        if (playerExistInList == -1) {
            lastPlayerDisconncted.push(playerObj); //nick, id, czas wyjscia, powód wyjscia(disconnect/timeout/kicked)
        } else {
            lastPlayerDisconncted.splice(playerExistInList, 1);
            lastPlayerDisconncted.push(playerObj);
        }
    }
}
mp.events.add("playerQuit", playerQuitHandler);


//remove mute player
function removeMutePlayer(player) {
    player.setVariable("playerMuted", null);
    GlobalFunctions.mysql.delete("mutes", "mute_user_id = ?", [player.getVariable("playerData").id]);
}
mp.events.add("callServer::removeMute", removeMutePlayer);

mp.events.add("playerCommand", (player, command) => {
    const args = command.split(/[ ]+/);
    const commandName = args.splice(0, 1)[0];
    //mp.events.callRemote("callServer::sendMessageToPlayer", "Ustawiam handling "+args+" "+args.length, "blue");
    // args.shift();
    var playerData = player.getVariable("playerData");
    if (playerData) {
        if (playerData.admin == "ADMIN") {
            var reason = ""
            for (var i = 2; i < args.length; i++) {
                reason = reason + " " + args[i];
            }

            if (commandName === "blip_visible") {
                mp.players.forEach((source, id) => {
                    source.call("callClient::changeBlipVisible", [args[0]]);
                });

            } else if (commandName === "blip_time") {
                mp.players.forEach((source, id) => {
                    source.call("callClient::changeBlipUpdateTime", [args[0]]);
                });
            }
        }
    }
});