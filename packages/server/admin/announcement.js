var announcementList = [];
//załadowanie ogłoszeń z bazy danych do listy

mp.events.add('packagesLoaded', () => {
    GlobalFunctions.mysql.handle.query('SELECT * FROM `announcements`', [], function (errorLogin, resultLogin, row) {
        if (errorLogin) console.log(errorLogin);
        if (resultLogin.length > 0) {
            announcementList = [];
            for (var i = 0; i < resultLogin.length; i++) {
                var announcementObj = {
                    header: resultLogin[i]["ann_header"],
                    text: resultLogin[i]["ann_text"],
                    color: resultLogin[i]["ann_color"],
                    expires: resultLogin[i]["ann_expires"],
                    start: resultLogin[i]["ann_start"],
                    interval: resultLogin[i]["ann_interval"],
                    lastTime: null,
                    id: resultLogin[i]["ann_id"]
                };
                announcementList.push(announcementObj);
                //console.log(announcementObj);
            }
        }
    });
});


//dodowanie ogłoszeń tutaj (na razie komendą potem panem)

//usuwanie ogłoszeń jeśli przedawnione

//wyświetlanie ogłoszeń do ludzi z czatu (sprawdzać kiedy ostatni raz wysłano poprzedni i jesli interval pozwala to wysłać)
var setIntervalAnnouncement = setInterval(sendAnnouncement, 45 * 1000);
function sendAnnouncement() {
    for (var i = 0; i < announcementList.length; i++) {
        var announcement = announcementList[i];
        if (announcement.expires - Date.now() <= 0) {  //nie wyświetla tych, które straciły ważność
            GlobalFunctions.mysql.delete("announcements", "ann_id = ?", [announcement.id]);
        } else {
            var isAnnouncementStarted = true;
            if (announcement.start != null) {//
                if (announcement.start - Date.now() > 0) {  //nie wyświetla tych, które nie wystartowaly
                    isAnnouncementStarted = false;
                }
            }
            if (isAnnouncementStarted) {
                if (announcement.lastTime == null) {
                    //console.log(announcement.text);
                    printAnnouncement(announcement)
                    announcement.lastTime = Date.now();
                } else {
                    var currentTime = Date.now();
                    var lastAnnouncementTime = announcement.lastTime;
                    var diffrenceTime = (currentTime - lastAnnouncementTime) / 1000; //seconds
                    if (diffrenceTime >= announcement.interval) {
                        announcement.lastTime = currentTime;
                        //console.log(announcement.text)
                        printAnnouncement(announcement)
                    }
                }
            }
        }
    }
}

function printAnnouncement(announcement) {
    var messageAnnouncement = {
        type: "announcement",
        header: announcement.header,
        text: announcement.text,
        color: announcement.color
    };
    GlobalFunctions.chat.announcementToAll(messageAnnouncement);
}