var bestPlayersInterval = setInterval(findBestPlayers, 5 * 60 * 1000);

mp.events.add("playerQuit", (player) => {
    savePlayerData(player);
});

function savePlayerData(player) {
    var currentDate = Date.now();
    var playerData = player.getVariable("playerData");
    if (playerData !== null && playerData !== undefined) {
        if (playerData.spawned) { //jesli zespawnowano to zaktualizowac pozycje 
            var playerSessionStats = player.getVariable("playerSessionStats");
            GlobalFunctions.mysql.update("users", "user_is_loggon = ?, user_money = ?, user_posX = ?, user_posY = ?, user_posZ = ?, user_heading = ?, user_logout_time = ?", "user_id = " + playerData.id, [false, playerData.money, player.position.x, player.position.y, player.position.z, player.heading, currentDate]);
            if (playerSessionStats) {
                GlobalFunctions.mysql.insert("users_sessions", "(session_user_id, session_time_total, session_time_afk, session_money_earned, session_money_spend, session_time_end, session_vehicle_driver, session_vehicle_passenger, session_walking) VALUES(?,?,?,?,?,?,?,?,?)",
                    [playerData.id, playerSessionStats.total, playerSessionStats.afk, 0, 0, currentDate, playerSessionStats.drive, playerSessionStats.passenger, playerSessionStats.walking]);
            }
        } else {
            GlobalFunctions.mysql.update("users", "user_is_loggon = ?", "user_id = " + playerData.id, [false]);
        }
    }
}

function updatePlayerOnlineTime(player, object, isAfk) {
    player.setVariable("playerAFK", isAfk);
    var receivedSessionData = JSON.parse(object);
    player.setVariable("playerSessionStats", receivedSessionData);
}
mp.events.add("callServer::updatePlayerTime", updatePlayerOnlineTime);

function playerSetVarbiale(player, variable, value) {
    player.setVariable(variable, value);
}
mp.events.add("callServer::playerSetVariable", playerSetVarbiale);


/*
CO 10 minut wypisywac gracza ktory jest aktualnie najdluzej na serwerze, najwiecej przejechal ITD
*/

function findBestPlayers() {
    try {
        var logonPlayerList = [];
        var bestDriversPlayerList = [];
        var walkingPlayerList = [];
        mp.players.forEach((player, id) => {
            var playerData = player.getVariable("playerData");
            if (playerData !== null && playerData !== undefined) {
                var playerSessionStats = player.getVariable("playerSessionStats");
                if (playerSessionStats !== null && playerSessionStats !== undefined) {
                    //session time
                    var objectTotalTime = {
                        nick: playerData.nick,
                        value: playerSessionStats.total
                    }
                    if (objectTotalTime.value > 0)
                        logonPlayerList.push(objectTotalTime);

                    //driver
                    var objectVehicleDriver = {
                        nick: playerData.nick,
                        value: (playerSessionStats.drive).toFixed(3)
                    }
                    if (objectVehicleDriver.value > 0.0)
                        bestDriversPlayerList.push(objectVehicleDriver);
                    //walking
                    var objectWalking = {
                        nick: playerData.nick,
                        value: (playerSessionStats.walking).toFixed(3)
                    }
                    if (objectWalking.value > 0.0)
                        walkingPlayerList.push(objectWalking);

                }
            }
        }
        );
        logonPlayerList.sort(compare);
        logonPlayerList.reverse();
        bestDriversPlayerList.sort(compare);
        bestDriversPlayerList.reverse();
        walkingPlayerList.sort(compare);
        walkingPlayerList.reverse();


        var winners = "";
        var color = "white";

        if (logonPlayerList.length > 0)
            winners = "<b>Gracze najdłużej online:</b><br>";
        for (var i = 0; i < logonPlayerList.length; i++) {
            if (i == 0) {
                color = "yellow";
            } else if (i == 1) {
                color = "silver";
            }
            else if (i == 2) {
                color = "#cd7f32";
            } else {
                color = "white";
            }

            var onlineTime = parseInt(logonPlayerList[i].value / 3600) + "h " + parseInt(logonPlayerList[i].value / 60) + "m " + (logonPlayerList[i].value % 60) + "s";
            winners += "<font color='" + color + "'>&#9812;</font><font color='white'> <b>" + logonPlayerList[i].nick + "</b> (" + onlineTime + ")</font>&#9;&#9;&#9;<br>";
        }

        if (bestDriversPlayerList.length > 0)
            winners += "<br><b>Gracze z najwięcej przejechanymi kilometrami:</b><br>"
        for (var i = 0; i < bestDriversPlayerList.length; i++) {
            if (i == 0) {
                color = "yellow";
            } else if (i == 1) {
                color = "silver";
            }
            else if (i == 2) {
                color = "#cd7f32";
            } else {
                color = "white";
            }

            winners += "<font color='" + color + "'>&#9812;</font><font color='white'> <b>" + bestDriversPlayerList[i].nick + "</b> (" + bestDriversPlayerList[i].value + " km)</font>&#9;&#9;&#9;<br>";
        }

        if (walkingPlayerList.length > 0)
            winners += "<br><b>Gracze, którzy najwięcej przeszli pieszo:</b><br>"
        for (var i = 0; i < walkingPlayerList.length; i++) {
            if (i == 0) {
                color = "yellow";
            } else if (i == 1) {
                color = "silver";
            }
            else if (i == 2) {
                color = "#cd7f32";
            } else {
                color = "white";
            }

            winners += "<font color='" + color + "'>&#9812;</font><font color='white'> <b>" + walkingPlayerList[i].nick + "</b> (" + walkingPlayerList[i].value + " km)</font>&#9;&#9;&#9;<br>";
        }

        var messageWinner = {
            type: "announcement",
            header: "RANKING SESJI",
            text: winners,
            color: "blue"
        };

        if (!(walkingPlayerList.length == 0 && bestDriversPlayerList.length == 0 && logonPlayerList == 0))
            GlobalFunctions.chat.announcementToAll(messageWinner);
    } catch (error) {
        console.log("[ERROR] Session ranking: "+error);
        GlobalFunctions.log.add("error", "[ERROR] Session ranking: "+error);
    }

}

function compare(a, b) {
    if (a.value < b.value) {
        return -1;
    }
    if (a.value > b.value) {
        return 1;
    }
    return 0;
}