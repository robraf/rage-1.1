var async = require('async');

mp.events.add("callServer::onPlayerInCreator", async function (player, event, appearanceObj) {
    switch (event) {
        case "player_in_creator":
            var playerData = player.getVariable("playerData");
            if (playerData != null && playerData != undefined) {
                playerData.inCreator = true;
                player.setVariable("playerData", playerData);
            }
            break;
        case "player_save_character":
            //array object
            var playerData = player.getVariable("playerData");
            if (playerData != null && playerData != undefined) {
                playerData.inCreator = false;
                player.setVariable("playerData", playerData);
                var appearance = JSON.parse(appearanceObj);
                player.setVariable("playerAppearance", appearance);
                //update
                /*
                    SPRAWDZIĆ CZY ZAPISANO JESLI ZAPISANO DOPIERO ZWROCIC DO KLIENTA POMYSLNIE, W PRZECIWNYM WYPADKU NIC
                */
                GlobalFunctions.mysql.update("appearances", "appearance_chosen = ?, appearance_sex = ?, appearance_eye = ?, appearance_hair = ?, appearance_hair_color1 = ?, appearance_hair_color2 = ?, appearance_mother = ?, appearance_father = ?, appearance_shape = ?, appearance_skin = ?, appearance_face = ?, appearance_blemishes = ?, appearance_facialhair = ?, appearance_facialhair_color = ?, appearance_eyebrows = ?, appearance_eyebrows_color = ?, appearance_ageing = ?, appearance_makeup = ?, appearance_makeup_color1 = ?, appearance_makeup_color2 = ?, appearance_blush = ?, appearance_blush_color = ?, appearance_complexion = ?, appearance_sundamage = ?, appearance_lipstick = ?, appearance_lipstick_color = ?, appearance_moles = ?", "appearance_user_id = " + playerData.id,
                    [1, appearance.sex, appearance.eye, appearance.hair, appearance.hairColor1, appearance.hairColor2, appearance.mother, appearance.father, appearance.shape, appearance.skin, appearance.face,
                        appearance.blemishes, appearance.facialhair, appearance.facialhairColor, appearance.eyebrows, appearance.eyebrowsColor, appearance.ageing,
                        appearance.makeup, appearance.makeupColor1, appearance.makeupColor2, appearance.blush, appearance.blushColor, appearance.complexion, appearance.sundamage,
                        appearance.lipstick, appearance.lipstickColor, appearance.moles]);
            }
            break;
        case "get_character":
            var playerData = player.getVariable("playerData");
            if (playerData != null && playerData != undefined) {
                var getCharacter = await GlobalFunctions.mysql.select("appearances", "WHERE appearance_user_id = ?", [playerData.id])
                if(getCharacter!== null && getCharacter!==undefined){
                    if(getCharacter.length>0){
                        var playerAppearance = {
                            created: getCharacter[0]["appearance_chosen"], //is character created
                            sex: getCharacter[0]["appearance_sex"], // M/F
                            eye: parseInt(getCharacter[0]["appearance_eye"]), // 0 do 31
                            face: getCharacter[0]["appearance_face"],
                            hair: parseInt(getCharacter[0]["appearance_hair"]), //0 do 78 lub 74 oprocz cos tam
                            hairColor1: parseInt(getCharacter[0]["appearance_hair_color1"]), //0, 63
                            hairColor2: parseInt(getCharacter[0]["appearance_hair_color2"]), //0, 63
                            mother: parseInt(getCharacter[0]["appearance_mother"]),
                            father: parseInt(getCharacter[0]["appearance_father"]),
                            shape: parseFloat(getCharacter[0]["appearance_shape"]), //podopienstwo ksztaltow 
                            skin: parseFloat(getCharacter[0]["appearance_skin"]), //podobienstwo skory
                            blemishes: parseFloat(getCharacter[0]["appearance_blemishes"]),
                            facialhair: parseFloat(getCharacter[0]["appearance_facialhair"]),
                            facialhairColor: parseFloat(getCharacter[0]["appearance_facialhair_color"]),
                            eyebrows: parseFloat(getCharacter[0]["appearance_eyebrows"]),
                            eyebrowsColor: parseFloat(getCharacter[0]["appearance_eyebrows_color"]),
                            ageing: parseFloat(getCharacter[0]["appearance_ageing"]),
                            makeup: parseFloat(getCharacter[0]["appearance_makeup"]),
                            makeupColor1: parseFloat(getCharacter[0]["appearance_makeup_color1"]),
                            makeupColor2: parseFloat(getCharacter[0]["appearance_makeup_color2"]),
                            blush: parseFloat(getCharacter[0]["appearance_blush"]),
                            blushColor: parseFloat(getCharacter[0]["appearance_blush_color"]),
                            complexion: parseFloat(getCharacter[0]["appearance_complexion"]),
                            sundamage: parseFloat(getCharacter[0]["appearance_sundamage"]),
                            lipstick: parseFloat(getCharacter[0]["appearance_lipstick"]),
                            lipstickColor: parseFloat(getCharacter[0]["appearance_lipstick_color"]),
                            moles: parseFloat(getCharacter[0]["appearance_moles"])
                        }
                        //wyslij do clienta
                        player.call("call::playerCreatorBrowser", ["set_player_in_creator", JSON.stringify(playerAppearance)]);

                    }
                }
            }
            break;
        default:
            break;
    }
})