const INTERVAL_TIME = 250;

var interval = setInterval(function () {
    var playerListArray = [];
    mp.players.forEach((playerObj, id) => {
        var playerData = playerObj.getVariable("playerData");
        if (playerData!=null && playerData!=undefined) {
            if(playerObj.getVariable("playerSessionID")>=0){
                if(!playerData.inCreator && !playerObj.getVariable("playerNoclip")){
                    var object = {
                        position: playerObj.position,
                        spawned: playerData.spawned,
                        noclip: false,
                        nick: playerData.nick,
                        id: playerData.id,
                        sessionId: playerObj.getVariable("playerSessionID"),
                        totalTime: playerObj.getVariable("playerSessionStats").total
                    }
                    playerListArray.push(object);
                } 
            }   
        }
    });

    mp.players.forEach((playerObj, id) => {
        if (playerObj.getVariable("playerData")) {
            playerObj.call("callClient::sendPlayerList", [JSON.stringify(playerListArray)]);
        }
    });
    
}, INTERVAL_TIME)


mp.events.add("playerCommand", (player, command) => {
    const args = command.split(/[ ]+/);
    const commandName = args.splice(0, 1)[0];
    //args.shift();
    if (commandName === "incognito") {
        mp.players.forEach((playerObj, id) => {
            if (playerObj.getVariable("playerData")) {
                playerObj.call("callClient::blipPlayerManager", ["incognito_blip", parseInt(args[0]), parseInt(args[1])]);
            }
        });
    }
});

function playerQuitHandler(player, exitType, reason) {
    removeBlip(player);
}
mp.events.add("playerQuit", playerQuitHandler);


function removeBlip(player){
    var sessionId = player.getVariable("playerSessionID");
    if (sessionId >= 0 && sessionId!== null) {
        console.log("usun blip o id "+sessionId)
        mp.players.forEach((playerObj, id) => {
            if (playerObj.getVariable("playerData")) {
                playerObj.call("callClient::blipPlayerManager", ["remove_blip", parseInt(sessionId)]);
            }
        });
    }
    
}
mp.events.add("callServer::blipPlayerRemove", playerQuitHandler);