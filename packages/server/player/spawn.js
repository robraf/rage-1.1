let spawnPoints = require('..//json/spawn.json').Spawn;

const LAST_POSITION_INTERVAL = 2 * 60 //1h (chwilowo 2 min)

//dodać sprawdzenie czy gracz ma dom
mp.events.add("callServer::onSpawnAction", (source, event, playerPos) => {
    switch (event) {
        case "get_list":
            var spawnListToPush = [];
            spawnPoints.forEach(function(item, index){
                spawnListToPush.push(item);
            })

            var lastPosition = source.getVariable("playerData").lastPosition;
            var currentTime = Date.now();
            var diffrentTime = (currentTime - lastPosition[0]) / 1000; //seconds
            if (diffrentTime <= LAST_POSITION_INTERVAL) {
                spawnListToPush.push({ 
                    "id": "last_pos", 
                    "x": lastPosition[1], 
                    "y": lastPosition[2], 
                    "z": lastPosition[3], 
                    "rotation": lastPosition[4], 
                    "text": "Ostatnia pozycja", 
                    "details": "", 
                    "camX": lastPosition[1], 
                    "camY": lastPosition[2], 
                    "camZ": lastPosition[3] + 150, 
                    "img": "last.png" });
            }
            source.call("callClient::returnSpawnList", [spawnListToPush]);
            break;
        case "player_spawned":
            source.spawn(playerPos);
            var playerData = source.getVariable("playerData");
            playerData.spawned = true;
            source.setVariable("playerData", playerData);
            break;
        case "player_died":
            source.spawn(playerPos);
            break;
        default:
            break;
    }

});