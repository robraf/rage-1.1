fs = require("fs");
var async = require('async');

module.exports =
{
    add: async function (type, log) {
        var date = new Date();
        var month = date.getMonth() + 1;
        var day = date.getUTCDate();
        var year = date.getUTCFullYear();

        var fileName = "" + day + "-" + month + "-" + year;
        var date = new Date();
        var timeNow = new Date();
        var hours = timeNow.getHours();
        var minutes = timeNow.getMinutes();
        var seconds = timeNow.getSeconds();
        var time = "" + ((hours < 10) ? "0" : "") + hours;
        time += ((minutes < 10) ? ":0" : ":") + minutes;
        time += ((seconds < 10) ? ":0" : ":") + seconds;
        switch (type) {
            case "chat":
                var message = "" + time + "| " + log+"\n";
                var path = "logs/chat/" + fileName + ".txt";
                fs.appendFile(path, message, function (err) {
                    if (err) return console.log(err);
                });
                break;
            case "info":
                var message = "" + time + "| " + log+"\n";
                var path = "logs/info/" + fileName + ".txt";
                fs.appendFile(path, message, function (err) {
                    if (err) return console.log(err);
                });
                break;
            case "error":
                var message = "" + time + "| " + log+"\n";
                var path = "logs/error/" + fileName + ".txt";
                fs.appendFile(path, message, function (err) {
                    if (err) return console.log(err);
                });
                break;
            default:
                break;
        }
    }
}

mp.events.add("callServer::addDataToLogs", (type, log) => {
    GlobalFunctions.log.add(type, log);
});

mp.events.add("callServer::addConsoleLog", (player, message) => {
    console.log(message);
});