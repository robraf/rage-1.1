global.GlobalFunctions = {};
GlobalFunctions.mysql = require('./mysql.js');
GlobalFunctions.log = require('./log/save.js');
GlobalFunctions.chat = require('./chat/sender.js')

GlobalFunctions.mysql.connect(function() { });

require('./login_register/login.js');
require('./login_register/register.js');

require('./character_creator/controller.js');
require('./admin/announcement.js');
require('./admin/actions.js')
require('./admin/polls.js')

require('./player/spawn.js');
require('./player/blip.js');
require('./player/character.js');
require('./player/session.js');
require('./server.js');

require('./fuel_station/data.js');

require('./vehicle/create.js');
require('./vehicle/data.js');

require('./vehicletest.js');