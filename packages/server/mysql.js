var mysql = require('mysql');
var async = require('async');
module.exports =
{
    handle: null,

    connect: function (call) {
        this.handle = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'rage'
        });

        this.handle.connect(function (err) {
            if (err) {
                switch (err.code) {
                    case "ECONNREFUSED":
                        console.log("\x1b[93m[MySQL] \x1b[97mError: Check your connection details (packages/mysql/mysql.js) or make sure your MySQL server is running. \x1b[39m");
                        break;
                    case "ER_BAD_DB_ERROR":
                        console.log("\x1b[91m[MySQL] \x1b[97mError: The database name you've entered does not exist. \x1b[39m");
                        break;
                    case "ER_ACCESS_DENIED_ERROR":
                        console.log("\x1b[91m[MySQL] \x1b[97mError: Check your MySQL username and password and make sure they're correct. \x1b[39m");
                        break;
                    case "ENOENT":
                        console.log("\x1b[91m[MySQL] \x1b[97mError: There is no internet connection. Check your connection and try again. \x1b[39m");
                        break;
                    default:
                        console.log("\x1b[91m[MySQL] \x1b[97mError: " + err.code + " \x1b[39m");
                        break;
                }
            } else {
                console.log("\x1b[92m[MySQL] \x1b[97mConnected Successfully \x1b[39m");
            }
        });
    },

    select: function (table, conditions, values) {
        return new Promise(resolve => {
            this.handle.query('SELECT * FROM `' + table + '` ' + conditions, values, function (err, res, row) {
                if(err){
                    console.log(err)
                    GlobalFunctions.log.add("error", "MYSQL select "+error);
                    resolve(error);
                }else{
                    resolve(res);
                }
            });
        });
    },


    update: function (table, params, conditions, values) {
        return new Promise(resolve => {
            this.handle.query('UPDATE `' + table + '` SET ' + params + ' WHERE ' + conditions, values, function (err, res, row) {
                if(err){
                    console.log(err)
                    GlobalFunctions.log.add("error", "MYSQL update "+error);
                    resolve(error);
                }else{
                    resolve(res);
                }
            });
        });
    },

    delete: function (table, params, values) {
        return new Promise(resolve => {
            this.handle.query('DELETE FROM `' + table + '` WHERE ' + params, values, function (err, res, row) {
                if(err){
                    console.log(err)
                    GlobalFunctions.log.add("error", "MYSQL delete "+error);
                    resolve(error);
                }else{
                    resolve(res);
                }
            });
        });
    },

    insert: function (table, params, values) {
        return new Promise(resolve => {
            this.handle.query('INSERT INTO `' + table + '` ' + params, values, function (err, res, row) {
                if(err){
                    console.log(err)
                    GlobalFunctions.log.add("error", "MYSQL insert "+error);
                    resolve(error);
                }else{
                    resolve(res);
                }
            });
        });
    }
};