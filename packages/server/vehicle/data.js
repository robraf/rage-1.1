////update mileage, fuel itd
var async = require('async');
mp.events.add("callServer::updateVehicleData", async function (player, event, value, vehicle) {
    if (event === "veh_data") {
        if (vehicle !== null && vehicle !== undefined) {
            let vehicleValue = JSON.parse(value); //vehicle, mileage, w przyszlosci fuel
            vehicle.setVariable("vehicleMileage", vehicleValue.mileage);
            vehicle.setVariable("vehicleFuel", vehicleValue.fuel);
        }
    } else if (event === "save_data") {
        //zapisanie do bazy pozycji auta
        /*
            dodać zniszczenia silnika, karoserii, drzwi, okien
        */
        try {
            let vehicleValue = JSON.parse(value);
            vehicle.setVariable("vehicleMileage", vehicleValue.mileage);

            let vehicleData = vehicle.getVariable("vehicleData");
            if (vehicleData !== null && vehicleData !== undefined) {
                if (vehicleData.type === "private") {


                    let rotation = vehicle.rotation;
                    var updateVeh = await GlobalFunctions.mysql.update("vehicles", "vehicle_mileage = ?, vehicle_fuel = ?, vehicle_posX = ?, vehicle_posY = ?, vehicle_posZ = ?, vehicle_heading = ?, vehicle_rotX = ?, vehicle_rotY = ?, vehicle_rotZ = ?", "vehicle_id = " + vehicleData.id, [parseFloat(vehicleValue.mileage), parseFloat(vehicleValue.fuel), vehicle.position.x, vehicle.position.y, vehicle.position.z, vehicleValue.heading, rotation.x, rotation.y, rotation.z]);
                    if (!(updateVeh != null && updateVeh.affectedRows == 1)) {
                        GlobalFunctions.log.add("error", "MYSQL vehicle update, veh_id: " + vehicleData.id);
                    }


                    let vehicleDoors = vehicle.getVariable("vehicleDoors");
                    let vehicleWindows = vehicle.getVariable("vehicleWindows");

                    if (vehicleDoors !== null && vehicleDoors !== undefined
                        && vehicleWindows !== null && vehicleWindows !== undefined) {
                        var updateVeh = await GlobalFunctions.mysql.update("vehicles_damage", "vehicle_door0 = ?, vehicle_door1 = ?, vehicle_door2 = ?, vehicle_door3 = ?, vehicle_door4 = ?, vehicle_door5 = ?, vehicle_window0 = ?, vehicle_window1 = ?, vehicle_window2 = ?, vehicle_window3 = ?, vehicle_window4 = ?, vehicle_window5 = ?, vehicle_window6 = ?, vehicle_window7 = ?", "vehicle_damage_id = " + vehicleData.id,
                            [vehicleDoors.door0, vehicleDoors.door1, vehicleDoors.door2, vehicleDoors.door3, vehicleDoors.door4, vehicleDoors.door5, vehicleWindows.window0, vehicleWindows.window1, vehicleWindows.window2, vehicleWindows.window3, vehicleWindows.window4, vehicleWindows.window5, vehicleWindows.window6, vehicleWindows.window7]);
                        if (!(updateVeh != null && updateVeh.affectedRows == 1)) {
                            GlobalFunctions.log.add("error", "MYSQL vehicle update, veh_id: " + vehicleData.id);
                        }
                    }
                }
            }
        } catch (error) {
            GlobalFunctions.log.add("error", "MYSQL vehicle update " + error);
        }
    } else if (event === "save_tunning") {
        //zapisanie do bazy tuning auta
    }
    else if (event === "damage") { //zapisuje zniszczenia
        if (vehicle !== null && vehicle !== undefined) {
            let vehicleValue = JSON.parse(value);
            let doors = vehicleValue.doorsStatus;
            let windows = vehicleValue.windowsStatus;

            var doorsData = vehicle.getVariable("vehicleDoors");
            if (doors[0] == 1) {
                doorsData.door0 = 1
            }
            if (doors[1] == 1) {
                doorsData.door1 = 1
            }
            if (doors[2] == 1) {
                doorsData.door2 = 1
            }
            if (doors[3] == 1) {
                doorsData.door3 = 1
            }
            if (doors[4] == 1) {
                doorsData.door4 = 1
            }
            if (doors[5] == 1) {
                doorsData.door5 = 1
            }
            vehicle.setVariable("vehicleDoors", doorsData);

            var windowsData = vehicle.getVariable("vehicleWindows");
            if (windows[0] == 1) windowsData.window0 = 1;
            if (windows[1] == 1) windowsData.window1 = 1;
            if (windows[2] == 1) windowsData.window2 = 1;
            if (windows[3] == 1) windowsData.window3 = 1;
            if (windows[4] == 1) windowsData.window4 = 1;
            if (windows[5] == 1) windowsData.window5 = 1;
            if (windows[6] == 1) windowsData.window6 = 1;
            if (windows[7] == 1) windowsData.window7 = 1;
            vehicle.setVariable("vehicleWindows", windowsData);

        }
    }
    else if (event === "freeze") { //odfreezowuje pojazd
        vehicle.setVariable("vehicleFreeze", false);
    }
});