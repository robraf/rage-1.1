//ON SERVER START CREATE VEHICLES
//SET DATA FROM DB
var async = require('async');
let vehicleList = require('..//json/vehicle.json').Vehicle;

async function spawnVehicles() {
    var respond = await GlobalFunctions.mysql.select("vehicles", "LEFT JOIN vehicles_tunning ON vehicles.vehicle_id=vehicles_tunning.vehicle_tunning_id LEFT JOIN vehicles_damage ON vehicles.vehicle_id = vehicles_damage.vehicle_damage_id WHERE vehicle_spawned = ?", [true]);
    if (respond !== null && respond !== undefined && respond.length > 0) {
        for (var i = 0; i < respond.length; i++) {
            var vehicleObject = respond[i];
            var vehicle = mp.vehicles.new(mp.joaat(vehicleObject["vehicle_model"]), new mp.Vector3(parseFloat(vehicleObject["vehicle_posX"]), parseFloat(vehicleObject["vehicle_posY"]), parseFloat(vehicleObject["vehicle_posZ"])),
                {
                    numberPlate: "RL" + vehicleObject["vehicle_id"],
                    heading: vehicleObject["vehicle_heading"]
                });
            vehicle.rotation = new mp.Vector3(parseFloat(vehicleObject["vehicle_rotX"]), parseFloat(vehicleObject["vehicle_rotY"]), parseFloat(vehicleObject["vehicle_rotZ"]));
            let vehicleStats = findVehicleByName(vehicleObject["vehicle_model"])[0];

            let engineStats = findVehicleByEngineVersion(vehicleStats.versions, parseInt(vehicleObject["vehicle_engine_version"]))[0];

            let vehicleData = {
                id: parseInt(vehicleObject["vehicle_id"]),
                type: "private",
                owner: parseInt(vehicleObject["vehicle_owner"]),
                engineType: getEngineType(engineStats.engine_type),
                enginePower: engineStats.engine_power,
                maxSpeed: engineStats.max_speed,
                fuelUsage: engineStats.fuel_usage,
                fuelTank: engineStats.fuel_tank,
                trunkCapacity: engineStats.trunk_capacity,
                handlingEngine: engineStats.handling_engine
            }
            vehicle.setVariable("vehicleData", vehicleData);

            let vehicleDoors = {
                door0: vehicleObject["vehicle_door0"],
                door1: vehicleObject["vehicle_door1"],
                door2: vehicleObject["vehicle_door2"],
                door3: vehicleObject["vehicle_door3"],
                door4: vehicleObject["vehicle_door4"],
                door5: vehicleObject["vehicle_door5"],
            }
            vehicle.setVariable("vehicleDoors", vehicleDoors);

            let vehicleWindows = {
                window0: vehicleObject["vehicle_window0"],
                window1: vehicleObject["vehicle_window1"],
                window2: vehicleObject["vehicle_window2"],
                window3: vehicleObject["vehicle_window3"],
                window4: vehicleObject["vehicle_window4"],
                window5: vehicleObject["vehicle_window5"],
                window6: vehicleObject["vehicle_window6"],
                window7: vehicleObject["vehicle_window7"]
            }
            vehicle.setVariable("vehicleWindows", vehicleWindows);

            let vehicleTunning = {
                turbo: parseInt(vehicleObject["vehicle_turbo"]),
                xenon: parseInt(vehicleObject["vehicle_xenon"]),
                window: parseInt(vehicleObject["vehicle_window"])
            }

            vehicle.setMod(18, vehicleTunning.turbo); //turbo
            vehicle.setMod(55, vehicleTunning.window); //window
            vehicle.setMod(22, vehicleTunning.xenon); //xenon
            vehicle.setVariable("vehicleTunning", vehicleTunning);
            vehicle.setVariable("vehicleFreeze", true);

            vehicle.setVariable("vehicleMileage", parseFloat(vehicleObject["vehicle_mileage"]));
            vehicle.setVariable("vehicleFuel", parseFloat(vehicleObject["vehicle_fuel"]));
        }

    }
}
mp.events.add("call::spawnVehicles", spawnVehicles);

function findVehicleByName(name) {
    return vehicleList.filter(
        function (vehicleList) { return vehicleList.name == name }
    );
}

function findVehicleByEngineVersion(engineList, id) {
    return engineList.filter(
        function (engineList) { return engineList.id == id }
    );
}

function getEngineType(type) {
    var engineType = "";
    switch (type) {
        case "pb":
            engineType = "benzyna";
            break;
        case "on":
            engineType = "diesel";
            break;
        case "lpg":
            engineType = "gaz";
            break;
        case "e":
            engineType = "elektryczny";
            break;
        default:
            break;
    }
    return engineType;
}

/*
NA CZAS TWORZENIA SERWERA DODAWANIE AUT DO BAZY DANYCH

*/
mp.events.addCommand('auto_db', async function (player, _, vehicleName, engineVersion) {
    if (vehicleName !== null && vehicleName !== undefined && engineVersion !== null && engineVersion !== undefined) {
        engineVersion = parseInt(engineVersion);
        if (!isNaN(engineVersion)) {
            var isAdded = await insertVehicleToDatabase(player, vehicleName, engineVersion);
            if (isAdded) {
                player.call("callClient::addNotify", ["Tworzenie pojazdu", "Dodaje...", "green", null]);
            } else {
                player.call("callClient::addNotify", ["Tworzenie pojazdu", "Nie udało się stworzyć pojazdu", "red", null]);
            }
        } else {
            player.call("callClient::addNotify", ["Tworzenie pojazdu", "Wersja silnikowa musi być liczbą całkowitą", "red", null]);
        }
    } else {
        player.call("callClient::addNotify", ["Tworzenie pojazdu", "Składnia: /auto_db [nazwa] [wersja_silnika]", "red", null]);
    }

});

async function insertVehicleToDatabase(player, vehicleName, engineVersion) {
    let vehicleStats = findVehicleByName(vehicleName)[0];
    if (vehicleStats !== null && vehicleStats !== undefined) {
        let engineStats = findVehicleByEngineVersion(vehicleStats.versions, parseInt(engineVersion))[0];
        if (engineStats !== null && engineStats !== undefined) {
            let vehicleId = null;
            let playerData = player.getVariable("playerData");
            let playerId = 0;
            if(playerData!==null && playerData!==undefined){
                playerId=playerData.id;
            }
            var isAddedToVehicles = await GlobalFunctions.mysql.insert("vehicles", "(vehicle_owner, vehicle_model, vehicle_engine_version, vehicle_posX, vehicle_posY, vehicle_posZ, vehicle_rotX, vehicle_rotY, vehicle_rotZ, vehicle_heading, vehicle_spawned) VALUES(?,?,?,?,?,?,?,?,?,?,?)",
                [playerId, vehicleName, engineStats.engine_type, player.position.x, player.position.y, player.position.z, 0, 0, 0, player.heading, 1]);
            if (isAddedToVehicles != null && isAddedToVehicles.affectedRows == 1) {
                vehicleId = isAddedToVehicles.insertId;
                var isAddedToVehiclesDamage = await GlobalFunctions.mysql.insert("vehicles_damage", "(vehicle_damage_id) VALUES(?)", [vehicleId]);
                if (isAddedToVehiclesDamage != null && isAddedToVehiclesDamage.affectedRows == 1) {
                    var isAddedToVehiclesTunning = await GlobalFunctions.mysql.insert("vehicles_tunning", "(vehicle_tunning_id) VALUES(?)", [vehicleId]);
                    if (isAddedToVehiclesTunning != null && isAddedToVehiclesTunning.affectedRows == 1) {
                        //SUCCESS
                        var vehicle = mp.vehicles.new(mp.joaat(vehicleName), new mp.Vector3(player.position.x, player.position.y, player.position.z+0.5),
                            {
                                numberPlate: "RL" + vehicleId,
                                heading: player.heading
                            });
                        vehicle.rotation = new mp.Vector3(0, 0, 0);
                     
                        let vehicleData = {
                            id: vehicleId,
                            type: "private",
                            owner: playerId,
                            engineType: getEngineType(engineStats.engine_type),
                            enginePower: engineStats.engine_power,
                            maxSpeed: engineStats.max_speed,
                            fuelUsage: engineStats.fuel_usage,
                            fuelTank: engineStats.fuel_tank,
                            trunkCapacity: engineStats.trunk_capacity,
                            handlingEngine: engineStats.handling_engine
                        }
                        vehicle.setVariable("vehicleData", vehicleData);

                        let vehicleDoors = {
                            door0: 0,
                            door1: 0,
                            door2: 0,
                            door3: 0,
                            door4: 0,
                            door5: 0,
                        }
                        vehicle.setVariable("vehicleDoors", vehicleDoors);

                        let vehicleWindows = {
                            window0: 0,
                            window1: 0,
                            window2: 0,
                            window3: 0,
                            window4: 0,
                            window5: 0,
                            window6: 0,
                            window7: 0
                        }
                        vehicle.setVariable("vehicleWindows", vehicleWindows);

                        vehicle.setVariable("vehicleTunning", null);
                        vehicle.setVariable("vehicleFreeze", true);

                        vehicle.setVariable("vehicleMileage", 0.0);
                        vehicle.setVariable("vehicleFuel", 10.0);
                        return true;
                    } else return false;
                } else return false;
            } else return false;
        }
        else {
            player.call("callClient::addNotify", ["Tworzenie pojazdu", "Ten pojazd nie posiada takiej wersji silnika", "red", null]);
            return false;
        }
    } else {
        player.call("callClient::addNotify", ["Tworzenie pojazdu", "Tego pojazdu nie ma w rotacji", "red", null]);
        return false;
    }

}