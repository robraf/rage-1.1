var async = require('async');
var bcrypt = require('bcrypt-nodejs');
var sessionIdArray = [];

mp.events.add("callServer::onLoginPanelBrowserAction", (source, event, login, password) => {
    switch (event) {
        case "loginPlayer":
            GlobalFunctions.mysql.handle.query('SELECT * FROM `users` LEFT JOIN mutes ON users.user_id=mutes.mute_user_id LEFT JOIN appearances ON users.user_id=appearances.appearance_user_id WHERE users.user_login = ?', [login], function (errorLogin, resultLogin, row) {
                if (errorLogin) console.log(errorLogin);
                if (resultLogin.length > 0) {
                    bcrypt.compare(password, resultLogin[0]["user_password"], function (err, res) {
                        if (res === true) {  //Password is correct
                            if (resultLogin[0]["user_banned"]) { //to konto jest zbanowane
                                source.call("callClient::onLoginPanelBrowserAction", ["banned"]);
                            } else {
                                if (!resultLogin[0]["user_is_loggon"]) { //jeśli nikt nie jest zalogowany na koncie

                                    GlobalFunctions.log.add("info", "Zalogowano gracza [" + login + ", serial: " + source.serial + ", socialclub: " + source.socialClub + ", ip: " + source.ip + "]");
                                    /*
                                    LOGOWAĆ I PRZYPISAĆ WSZELKIEGO RODZAJU PARAMETRY, id, admin, kasa, pozycja końcowa jesli px,py,pz !=0 
                                    */
                                    var playerData = {
                                        inInterrior: false,
                                        spawned: false,
                                        nick: resultLogin[0]["user_login"],
                                        id: resultLogin[0]["user_id"],
                                        money: resultLogin[0]["user_money"],
                                        lastPosition: [resultLogin[0]["user_logout_time"], resultLogin[0]["user_posX"], resultLogin[0]["user_posY"], resultLogin[0]["user_posZ"], resultLogin[0]["user_heading"]],
                                        admin: "",
                                        inCreator: false
                                    }

                                    if (resultLogin[0]["user_admin"] != "") {
                                        playerData.admin = resultLogin[0]["user_admin"];
                                    }

                                    source.setVariable("playerData", playerData);

                                    //przydzielenie ID sesji
                                    var playerSessionID = -1;
                                    if (sessionIdArray.length > 0) {
                                        var addNewIdToTable = true;
                                        for (var i = 0; i < sessionIdArray.length; i++) {
                                            if (sessionIdArray[i] === null) {
                                                playerSessionID = i;
                                                sessionIdArray[i] = playerData.nick;
                                                addNewIdToTable = false;
                                                break;
                                            }
                                        }
                                        if (addNewIdToTable) {
                                            var newId = sessionIdArray.length;
                                            playerSessionID = newId;
                                            sessionIdArray.push(playerData.nick);
                                        }
                                    } else {
                                        sessionIdArray.push(playerData.nick);
                                        playerSessionID = 0;
                                    }

                                    source.setVariable("playerSessionID", playerSessionID);
                                    console.log("ID sesji gracza: " + playerSessionID);
                                    console.log("SESJE ID " + JSON.stringify(sessionIdArray));
                                    var objectSession = {
                                        total: 0,
                                        afk: 0,
                                        isAfk: false,
                                        drive: 0.0,
                                        passenger: 0.0,
                                        walking: 0.0
                                    }
                                    source.setVariable("playerSessionStats", objectSession);
                                    source.setVariable("playerChatting", false);
                                    source.setVariable("playerAFK", false);
                                    source.setVariable("playerNoclip", false);
                                    var muteExpires = resultLogin[0]["mute_expires"];
                                    if (muteExpires != null) {
                                        var timeNow = Date.now();
                                        if ((timeNow - muteExpires) >= 0) { //jesli blokada minela
                                            source.setVariable("playerMuted", null);
                                            GlobalFunctions.mysql.delete("mutes", "mute_user_id = ?", [playerData.id]);
                                        } else {
                                            source.setVariable("playerMuted", "" + muteExpires);
                                            source.call("callClient::addedMute", [Date.now()]);
                                            var date = new Date(muteExpires);
                                            var year = date.getFullYear();
                                            var month = ("0" + (date.getMonth() + 1)).slice(-2);
                                            var day = ("0" + date.getDate()).slice(-2);

                                            var hours = ((date.getHours() < 10) ? "0" : "") + date.getHours();
                                            var minutes = ((date.getMinutes() < 10) ? "0" : "") + date.getMinutes();
                                            var seconds = ((date.getSeconds() < 10) ? "0" : "") + date.getSeconds();
                                            var dateString = day + "." + month + "." + year + " " + hours + ":" + minutes + ":" + seconds;
                                            source.call("callClient::addNotify", ["Czat", "Masz nałożoną blokadę na czat do " + dateString, "red", null]);
                                        }
                                    } else {
                                        source.setVariable("playerMuted", null);
                                    }

                                    var playerAppearance = {
                                        created: resultLogin[0]["appearance_chosen"], //is character created
                                        sex: resultLogin[0]["appearance_sex"], // M/F
                                        eye: parseInt(resultLogin[0]["appearance_eye"]), // 0 do 31
                                        face: resultLogin[0]["appearance_face"],
                                        hair: parseInt(resultLogin[0]["appearance_hair"]), //0 do 78 lub 74 oprocz cos tam
                                        hairColor1: parseInt(resultLogin[0]["appearance_hair_color1"]), //0, 63
                                        hairColor2: parseInt(resultLogin[0]["appearance_hair_color2"]), //0, 63
                                        mother: parseInt(resultLogin[0]["appearance_mother"]),
                                        father: parseInt(resultLogin[0]["appearance_father"]),
                                        shape: parseFloat(resultLogin[0]["appearance_shape"]), //podopienstwo ksztaltow 
                                        skin: parseFloat(resultLogin[0]["appearance_skin"]), //podobienstwo skory
                                        blemishes: parseFloat(resultLogin[0]["appearance_blemishes"]),
                                        facialhair: parseFloat(resultLogin[0]["appearance_facialhair"]),
                                        facialhairColor: parseFloat(resultLogin[0]["appearance_facialhair_color"]),
                                        eyebrows: parseFloat(resultLogin[0]["appearance_eyebrows"]),
                                        eyebrowsColor: parseFloat(resultLogin[0]["appearance_eyebrows_color"]),
                                        ageing: parseFloat(resultLogin[0]["appearance_ageing"]),
                                        makeup: parseFloat(resultLogin[0]["appearance_makeup"]),
                                        makeupColor1: parseFloat(resultLogin[0]["appearance_makeup_color1"]),
                                        makeupColor2: parseFloat(resultLogin[0]["appearance_makeup_color2"]),
                                        blush: parseFloat(resultLogin[0]["appearance_blush"]),
                                        blushColor: parseFloat(resultLogin[0]["appearance_blush_color"]),
                                        complexion: parseFloat(resultLogin[0]["appearance_complexion"]),
                                        sundamage: parseFloat(resultLogin[0]["appearance_sundamage"]),
                                        lipstick: parseFloat(resultLogin[0]["appearance_lipstick"]),
                                        lipstickColor: parseFloat(resultLogin[0]["appearance_lipstick_color"]),
                                        moles: parseFloat(resultLogin[0]["appearance_moles"])

                                    }
                                    source.setVariable("playerAppearance", playerAppearance); // TO DO!!!
                                    GlobalFunctions.mysql.update("users", "user_is_loggon = ?" , "user_id = "+playerData.id,[true]);
                                    source.call("callClient::onLoginPanelBrowserAction", ["success", null, playerAppearance.created]);
                                } else {
                                    source.call("callClient::onLoginPanelBrowserAction", ["logged_in"]);
                                }
                            }
                        } else {    //Password is incorrect
                            source.call("callClient::onLoginPanelBrowserAction", ["bad_password"]);
                        }
                    });
                } else {
                    source.call("callClient::onLoginPanelBrowserAction", ["incorrect_login"]);
                }
            });
            break;
        default:
            break;
    }
});

function randomFloat(min, max) {
    var value = (Math.random() * (min - max) + max).toFixed(3);
    return parseFloat(value);
}

function randomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return parseInt(Math.floor(Math.random() * (max - min + 1)) + min);
}

//player quit, reset to null ID
function playerQuitHandler(player, exitType, reason) {
    var playerData = player.getVariable("playerData");
    if (playerData) {
        var sessionId = player.getVariable("playerSessionID");
        if (sessionId >= 0) {
            var playerNick = playerData.nick;
            var indexToNull = sessionIdArray.indexOf("" + playerNick, 0);
            if (indexToNull != -1) {
                sessionIdArray[indexToNull] = null;
                console.log("Usuwam gracza " + playerNick + ", zwolnione ID: " + indexToNull);
                console.log(JSON.stringify(sessionIdArray))
            }
        }
    }
}
mp.events.add("playerQuit", playerQuitHandler);