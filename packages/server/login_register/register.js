var bcrypt = require('bcrypt-nodejs');

mp.events.add("callServer::onLoginPanelBrowserAction", (source, event, login, password) => {
    switch (event) {
        case "registerPlayer":
            GlobalFunctions.mysql.handle.query('SELECT * FROM `users` WHERE user_login = ?', [login], function (errorCheck, resCheck) {
                if (errorCheck) {
                    console.log(errorCheck);
                    GlobalFunctions.log.add("error", "MYSQL register error: " + errorCheck);
                }
                if (resCheck.length == 0) {//można rejestrować bo konto nie istnieje              
                    bcrypt.hash(password, null, null, function (err, hash) {
                        if (!err) {
                            var serial = "";
                            var socialclub = "";
                            if (source.serial != null) serial = source.serial;
                            if (source.socialClub != null) socialclub = source.socialClub;
                            GlobalFunctions.mysql.handle.query('INSERT INTO users (user_login, user_password, user_serial, user_socialclub) VALUES(?,?,?,?)', [login, hash, serial, socialclub], function (errorRegister, resRegister) {
                                if (errorRegister) {
                                    console.log(errorRegister);
                                    GlobalFunctions.log.add("error", "MYSQL register error: " + errorRegister);
                                }
                                if (resRegister != null) {
                                    GlobalFunctions.log.add("info", "Zarejestrowano konto [" + login + "(id: " + resRegister.insertId + "), seria: " + serial + ", socialclub: " + socialclub + ", ip: " + source.ip + "]");
                                    GlobalFunctions.mysql.insert("appearances", "(appearance_user_id) VALUES(?)", [resRegister.insertId]);

                                    source.call("callClient::onLoginPanelBrowserAction", ["registration_success", resRegister.insertId]);
                                }
                            });
                        } else {
                            console.log("\x1b[31m[BCrypt]: " + err)
                            GlobalFunctions.log.add("error", "Bcrypt error: " + err);
                        }
                    });
                } else {
                    source.call("callClient::onLoginPanelBrowserAction", ["login_taken"]);
                }
            });
            break;
        default:
            break;
    }
});
