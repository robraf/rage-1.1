var listaGraczePomiar = [];
var isPomiar = false;
var vehicle1 = null;
var vehicle2 = null;
var handbrakeTimeout = null;
let vehicleList = require('./json/vehicle.json').Vehicle;

mp.events.add("playerCommand", (player, command) => {
    const args = command.split(/[ ]+/);
    const commandName = args.splice(0, 1)[0];
    //args.shift();
    if (commandName === "auto") {
        if (args[0] !== null && args[0] !== undefined) {
            if (args[1] !== null && args[1] !== undefined) {
                createVehiclePomiar(player, args[0], parseInt(args[1]), new mp.Vector3(player.position.x + 2, player.position.y, player.position.z + 0.5), player.heading);
            } else {
                var vehicle = mp.vehicles.new(mp.joaat(args[0]), new mp.Vector3(player.position.x + 2, player.position.y, player.position.z + 0.5),
                    {
                        heading: player.heading,
                        numberPlate: "RL",
                        locked: false
                    });
                mp.events.call("call::sendMessageToPlayer", player, "Zespawnowano pojazd " + args[0], "orange");
                let vehicleData = {
                    id: null,
                    type: "public",
                    owner: null,
                    engineType: "-",
                    enginePower: "-",
                    maxSpeed: 350,
                    fuelUsage: 0.1,
                    fuelTank: 40.00,
                    trunkCapacity: 50.0,
                    handlingEngine: 0
                }
                vehicle.setVariable("vehicleData", vehicleData);

                let vehicleDoors = {
                    door0: 0,
                    door1: 0,
                    door2: 0,
                    door3: 0,
                    door4: 0,
                    door5: 0,
                }
                vehicle.setVariable("vehicleDoors", vehicleDoors);

                let vehicleWindows = {
                    window0: 0,
                    window1: 0,
                    window2: 0,
                    window3: 0,
                    window4: 0,
                    window5: 0,
                    window6: 0,
                    window7: 0
                }
                vehicle.setVariable("vehicleWindows", vehicleWindows);
                vehicle.setVariable("vehicleMileage", 0);
                vehicle.setVariable("vehicleFuel", 40.00);
            }
            //player.putIntoVehicle(vehicle, 0);
        } else {
            player.call("callClient::addNotify", ["Nieprawidłowa składnia", "/auto nazwa [wersja_silnika]", "red", null]);
        }

    } else if (commandName === "autopomiar") {
        if (args[0] !== null && args[0] !== undefined && args[1] !== null && args[1] !== undefined) {
            // var vehicle = mp.vehicles.new(mp.joaat(args[0]), new mp.Vector3(572.7, -673.9, 13.3),
            //     {
            //         heading: 175.659,
            //         numberPlate: "RL",
            //         locked: false
            //     });
            createVehiclePomiar(player, args[0], parseInt(args[1]), new mp.Vector3(572.7, -673.9, 13.3), 175.659);
            player.position = new mp.Vector3(570.7, -673.9, 13.3);
            // player.putIntoVehicle(vehicle, 0);
        } else {
            player.call("callClient::addNotify", ["Nieprawidłowa składnia", "/auto nazwa wersja_silnika", "red", null]);
        }
    }
    else if (commandName === "pomiar_czas") {
        if (isPomiar) {
            if (handbrakeTimeout != null) {
                clearTimeout(handbrakeTimeout);
                handbrakeTimeout = setTimeout(realeseHandBrake, parseInt(args[0]) * 1000);
                mp.events.call("call::sendMessageToPlayer", player, "Zmniejszono czas do" + args[0], "green");
                listaGraczePomiar[0].call("callClient::changeTime", [parseInt(args[0])]);
                //listaGraczePomiar[1].call("callClient::setHandbrake", [parseInt(args[0])]);
            } else {
                mp.events.call("call::sendMessageToPlayer", player, "Wystąpił błąd", "red");
            }
        } else {
            mp.events.call("call::sendMessageToPlayer", player, "Pomiar nie jest rozpoczęty", "red");
        }
    }
    else if (commandName === "pomiar_resetuj") {
        try {
            if (listaGraczePomiar.length > 0) {
                listaGraczePomiar.forEach(function (item, index) {
                    item.call("callClient::endPomiar", []);
                })
            }
            if (vehicle1 != null) {
                vehicle1.destroy();
                vehicle1 = null;
            }
            if (vehicle2 != null) {
                vehicle2.destroy();
                vehicle2 = null;
            }
            handbrakeTimeout = null;
            listaGraczePomiar = [];
            isPomiar = false;
            mp.events.call("call::sendMessageToPlayer", player, "Zresetowno pomiar...", "green");
        } catch (error) {
            GlobalFunctions.log.add("error", "POMIAR " + error);
        }
    }
    else if (commandName === "pomiar_rozpocznij") { //arg - nazwa auta, czas do startu
        try {
            if (listaGraczePomiar.length == 1) {
                isPomiar = true;
                //wyslanie do graczy z listy informacji o rozpoczęciu i włożenie ich do AUT
                listaGraczePomiar[0].position = new mp.Vector3(1049.5, 3085.3, 41.5);
                // listaGraczePomiar[1].position = new mp.Vector3(1051.3, 3078.6, 41.5);

                setTimeout(function () {
                    vehicle1 = mp.vehicles.new(mp.joaat(args[0]), new mp.Vector3(1051.5, 3085.3, 41.5), //dodac pozycje
                        {
                            heading: 281.4,
                            numberPlate: "RL",
                            locked: false
                        });

                    // vehicle2 = mp.vehicles.new(mp.joaat(args[0]), new mp.Vector3(1053.3, 3078.6, 41.5), //dodac pozycje
                    //     {
                    //         heading: 281.4,
                    //         numberPlate: "RL",
                    //         locked: false
                    //     });
                    setTimeout(function () {
                        listaGraczePomiar[0].putIntoVehicle(vehicle1, 0);
                        // listaGraczePomiar[1].putIntoVehicle(vehicle2, 0);

                        //wyslac graczom informacje i zaciagnac reczny
                        var timeoutTime = parseInt(args[1]);

                        setTimeout(function () {
                            listaGraczePomiar[0].call("callClient::setHandbrake", [true, timeoutTime]);
                            //listaGraczePomiar[1].call("callClient::setHandbrake", [true, timeoutTime]);
                        }, 100)
                        if (timeoutTime) {
                            mp.events.call("call::sendMessageToPlayer", player, "Wystartowano pomiar, który rozpocznie się za " + timeoutTime + " sekund.", "green");
                            handbrakeTimeout = setTimeout(realeseHandBrake, timeoutTime * 1000)
                        }
                    }, 100)


                }, 500)

            } else {
                mp.events.call("call::sendMessageToPlayer", player, "Nie ma wystarczającej ilości graczy", "red");
            }
        } catch (error) {
            GlobalFunctions.log.add("error", "POMIAR " + error);
        }

    }
    else if (commandName === "pomiar_dodaj") { //arg - id gracza
        try {
            //czy istnieje w grze takie ID
            //czy juz jest dodany
            var idToAdd = parseInt(args[0]);

            if (listaGraczePomiar.length < 2) {
                if (idToAdd) {
                    var playerObj = null;
                    var playerList = mp.players;
                    for (var i = 0; i < playerList.length; i++) {
                        playerI = mp.players[i];
                        var playerData = playerI.getVariable("playerData");
                        if (playerData) {
                            if (idToAdd == playerData.id) {
                                playerObj = playerI;
                                break;
                            }
                        }
                    }

                    if (playerObj != null) {
                        var isPlayerExistInList = listaGraczePomiar.includes(playerObj, 0);
                        if (!isPlayerExistInList) {
                            listaGraczePomiar.push(playerObj);
                            //wyslij wiadomosc do gracza, że został dodany
                            mp.events.call("call::sendMessageToPlayer", player, "Pomyślnie dodano gracza", "green");
                            mp.events.call("call::sendMessageToPlayer", playerObj, "Zostałeś dodany do pomiaru, za chwilę będzie rozpoczęcie.", "green");
                        } else {
                            //ten gracz został już dodany
                            mp.events.call("call::sendMessageToPlayer", player, "Ten gracz już został dodany", "red");
                        }

                    } else {
                        //nie ma gracza o takim ID
                        mp.events.call("call::sendMessageToPlayer", player, "Nie ma gracza o takim ID", "red");
                    }

                } else {
                    //nie podano ID
                    mp.events.call("call::sendMessageToPlayer", player, "Nie podano żadnego ID", "red");
                }
            } else {
                mp.events.call("call::sendMessageToPlayer", player, "Już jest maksymalna liczba uczestników (2)", "red");
            }
        } catch (error) {
            GlobalFunctions.log.add("error", "POMIAR " + error);
        }
    }
    else if (commandName === "dotankuj") {
        if (player.vehicle) {

            if (args[0] !== undefined && args[0] !== null) {
                let vehicleFuel = player.vehicle.getVariable("vehicleFuel");
                if (vehicleFuel !== null && vehicleFuel !== undefined) {
                    let vehicleData = player.vehicle.getVariable("vehicleData");
                    if (vehicleData !== null && vehicleData !== undefined) {
                        let quantity = parseFloat(args[0]);
                        if (!isNaN(quantity)) {
                            let fuelTank = vehicleData.fuelTank;
                            var newQuantity = vehicleFuel + quantity;
                            if (newQuantity > fuelTank) {
                                quantity = quantity - (newQuantity - fuelTank);
                            }
                            if (newQuantity < 0) {
                                quantity = (newQuantity - quantity) * (-1);
                            }
                            player.call("callClient::addNotify", ["Paliwo", "Dotankowano " + quantity.toFixed(2) + " litrów paliwa", "green", null]);
                            player.vehicle.setVariable("vehicleFuel", vehicleFuel + quantity);
                            player.call("callClient::addFuelToVehicle", [quantity]);
                        } else {
                            player.call("callClient::addNotify", ["Nieprawidłowa składnia", "/dotankuj [ilość]", "red", null]);
                        }
                    }
                }
            } else {
                player.call("callClient::addNotify", ["Nieprawidłowa składnia", "/dotankuj [ilość]", "red", null]);
            }
        }
    } else if (commandName === "engine") {
        if (player.vehicle) {
            if (args[0] !== null && args[0] !== undefined) {
                let engineValue = args[0];
                if (!isNaN(engineValue)) {
                    let vehicleData = player.vehicle.getVariable("vehicleData");
                    if (vehicleData !== null && vehicleData !== undefined) {
                        vehicleData.handlingEngine = engineValue;
                        player.vehicle.setVariable("vehicleData", vehicleData);
                    }
                }
            }
        }
    } else if(commandName ==="fix"){
        if(player.vehicle){
            let vehicleDoors = {
                door0: 0,
                door1: 0,
                door2: 0,
                door3: 0,
                door4: 0,
                door5: 0,
            }
            player.vehicle.setVariable("vehicleDoors", vehicleDoors);

            let vehicleWindows = {
                window0: 0,
                window1: 0,
                window2: 0,
                window3: 0,
                window4: 0,
                window5: 0,
                window6: 0,
                window7: 0
            }
            player.vehicle.setVariable("vehicleWindows", vehicleWindows);

            //call player, ze naprawiono szyby i drzwi
            player.call("callClient::fixDoorsAndWindow", []);
        }
    }
});

mp.events.addCommand('mod', (player, _, modType, modIndex) => {
    if (player.vehicle) {
        player.vehicle.setMod(parseInt(modType), parseInt(modIndex));
        mp.events.call("call::sendMessageToPlayer", player, "Dodano modyfikacje", "orange");
    }
});

function realeseHandBrake() {
    listaGraczePomiar[0].call("callClient::setHandbrake", [false, 0]);
    //listaGraczePomiar[1].call("callClient::setHandbrake", [false, 0]);
}

function createVehiclePomiar(player, vehicleName, engineVersion, position, heading) {

    let vehicleStats = findVehicleByName(vehicleName)[0];
    if (vehicleStats !== null && vehicleStats !== undefined) {
        let engineStats = findVehicleByEngineVersion(vehicleStats.versions, parseInt(engineVersion))[0];
        if (engineStats !== null && engineStats !== undefined) {
            var vehicle = mp.vehicles.new(mp.joaat(vehicleName), position,
                {
                    heading: heading,
                    numberPlate: "RL",
                    locked: false
                });
            mp.events.call("call::sendMessageToPlayer", player, "Zespawnowano pojazd " + vehicleName, "orange");
            let vehicleData = {
                id: null,
                type: "public",
                owner: null,
                engineType: getEngineType(engineStats.engine_type),
                enginePower: engineStats.engine_power,
                maxSpeed: engineStats.max_speed,
                fuelUsage: 0.1,
                fuelTank: 40.00,
                trunkCapacity: 50.00,
                handlingEngine: engineStats.handling_engine
            }
            vehicle.setVariable("vehicleData", vehicleData);

            let vehicleDoors = {
                door0: 0,
                door1: 0,
                door2: 0,
                door3: 0,
                door4: 0,
                door5: 0,
            }
            vehicle.setVariable("vehicleDoors", vehicleDoors);

            let vehicleWindows = {
                window0: 0,
                window1: 0,
                window2: 0,
                window3: 0,
                window4: 0,
                window5: 0,
                window6: 0,
                window7: 0
            }
            vehicle.setVariable("vehicleWindows", vehicleWindows);

            let vehicleTunning = {
                // turbo: parseInt(vehicleObject["vehicle_turbo"]),
                // xenon: parseInt(vehicleObject["vehicle_xenon"]),
                // window: parseInt(vehicleObject["vehicle_window"])
            }

            // vehicle.setMod(18, vehicleTunning.turbo); //turbo
            // vehicle.setMod(55, vehicleTunning.window); //window
            // vehicle.setMod(22, vehicleTunning.xenon); //xenon
            vehicle.setVariable("vehicleTunning", vehicleTunning);
            // vehicle.setVariable("vehicleFreeze", true);

            vehicle.setVariable("vehicleMileage", 0);
            vehicle.setVariable("vehicleFuel", 40.00);
        }
        else {
            player.call("callClient::addNotify", ["Silnik", "Ten pojazd nie posiada takiej wersji silnika", "red", null]);
        }
    } else {
        player.call("callClient::addNotify", ["Pojazd", "Tego pojazdu nie ma w rotacji", "red", null]);
    }
}

function findVehicleByName(name) {
    return vehicleList.filter(
        function (vehicleList) { return vehicleList.name == name }
    );
}

function findVehicleByEngineVersion(engineList, id) {
    return engineList.filter(
        function (engineList) { return engineList.id == id }
    );
}

function getEngineType(type) {
    var engineType = "";
    switch (type) {
        case "pb":
            engineType = "benzyna";
            break;
        case "on":
            engineType = "diesel";
            break;
        case "lpg":
            engineType = "gaz";
            break;
        case "e":
            engineType = "elektryczny";
            break;
        default:
            break;
    }
    return engineType;
}